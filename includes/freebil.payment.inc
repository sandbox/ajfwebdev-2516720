<?php

/**
 * @file
 * Manages payments.
 *
 * Invoices have payments.
 * Payments are accessed from the invoice page.
 * The invoice page has a list of all payments, and a link to add
 * new payments.
 * Each payment row has links to update or delete the payment.
 */

/**
 * Page Callback: A form to create a payment.
 *
 * @return array
 *   A form array
 *
 * @see freebil_menu()
 * @see freebil_payment_create_form()
 *
 * @ingroup forms
 */
function freebil_payment_page_create($inv) {
  return drupal_get_form('freebil_payment_create_form', $inv);
}

/**
 * Form constructor for the payment create form.
 *
 * @param string $inv_id
 *   The invoice ID to create the payment for.
 *
 * @return array
 *   A form array
 *
 * @see freebil_payment_create_form_submit()
 * @see freebil_payment_create_form_validate()
 *
 * @ingroup forms
 */
function freebil_payment_create_form($form, &$form_state, $inv_id) {
  $inv_data = freebil_get_inv_data($inv_id);

  // Set the page breadcrumb.
  $crumbs = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('freebil'), 'admin/freebil'),
    l(t('Customers'), 'admin/freebil/customers'),
    l($inv_data['cust_name'], 'admin/freebil/customers/' . $inv_data['cust_id'] . '/view'),
    l($inv_data['inv_name'], 'admin/freebil/invoices/' . $inv_data['inv_id'] . '/view'),
    l($inv_data['inv_name'] . ' ' . t('Payments'), 'admin/freebil/invoices/' . $inv_data['inv_id'] . '/view/payments'),
  );
  drupal_set_breadcrumb($crumbs);

  $pay_data = array();
  return freebil_build_payment_form("create", $form_state, $pay_data, $inv_data);
}

/**
 * Form validation handler for freebil_payment_create_form().
 *
 * @see freebil_payment_create_form_submit()
 */
function freebil_payment_create_form_validate($form, &$form_state) {
  $payment_date = check_plain($form_state['values']['payment_date']);
  if (ctype_space($payment_date) || $payment_date == '') {
    form_set_error('payment_date', t('Select a Payment Date.'));
  }
  $currency = check_plain($form_state['values']['currency_code']);
  if (ctype_space($currency) || $currency == '') {
    form_set_error('currency_code', t('You must populate the Currency Code.'));
  }
}

/**
 * Form submission handler for freebil_payment_create_form().
 */
function freebil_payment_create_form_submit($form, &$form_state) {

  $row = array(
    'inv_id' => $form_state['values']['inv_id'],
    'currency_code' => $form_state['values']['currency_code'],
    'payment_amt' => $form_state['values']['payment_amt'],
    'payment_date' => $form_state['values']['payment_date'],
    'description' => $form_state['values']['description'],
    'notes' => $form_state['values']['notes'],
  );

  // Derive year and quarter from payment date for reporting purposes.
  $payment_date_parts = freebil_format_year_quarter($form_state['values']['payment_date']);
  $row['payment_year'] = $payment_date_parts['year'];
  $row['payment_quarter'] = $payment_date_parts['quarter'];

  freebil_payment_insert($row);
  freebil_recalc_invoice_payment_total_and_update($row['inv_id']);

  $form_state['redirect'] = 'admin/freebil/invoices/' . $row['inv_id'] . '/view/payments';
  drupal_set_message(t('Successfully saved Payment.'));
}

/**
 * Payment insert API.
 *
 * @param array $row
 *   An associative array containing:
 *   - The column names as keys and the column values as values for
 *     the payment row to be inserted.
 *
 * @return object
 *   - InsertQuery object
 */
function freebil_payment_insert(array $row) {
  $row['created_ts'] = REQUEST_TIME;
  $row['last_update_ts'] = REQUEST_TIME;

  $pay_id = db_insert('freebil_payment')
    ->fields($row)->execute();

  return $pay_id;
}

/**
 * Page Callback: A form to edit a payment.
 *
 * @param string $pay
 *   The payment ID for the payment to be edited.
 *
 * @return array
 *   A form array
 *
 * @see freebil_menu()
 */
function freebil_payment_page_edit($pay) {
  return drupal_get_form('freebil_payment_edit_form', $pay);
}

/**
 * Form constructor for the payment edit form.
 *
 * @param string $pay
 *   The payment ID for the payment to edit.
 *
 * @return array
 *   A form array
 *
 * @see freebil_payment_edit_form_submit()
 * @see freebil_payment_edit_form_validate()
 *
 * @ingroup forms
 */
function freebil_payment_edit_form($form, &$form_state, $pay) {
  $pay_data = array();
  $pay_data = freebil_get_pay_data($pay);
  $inv_data = array();
  $inv_data = freebil_get_inv_data($pay_data['inv_id']);

  // Set the page breadcrumb.
  $crumbs = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('freebil'), 'admin/freebil'),
    l(t('Customers'), 'admin/freebil/customers'),
    l($inv_data['cust_name'], 'admin/freebil/customers/' . $inv_data['cust_id'] . '/view'),
    l($inv_data['inv_name'], 'admin/freebil/invoices/' . $inv_data['inv_id'] . '/view'),
    l($inv_data['inv_name'] . ' ' . t('Payments'), 'admin/freebil/invoices/' . $inv_data['inv_id'] . '/view/payments'),
  );
  drupal_set_breadcrumb($crumbs);

  $form = freebil_build_payment_form("edit", $form_state, $pay_data);

  return $form;
}

/**
 * Form validation handler for freebil_payment_edit_form().
 *
 * @see freebil_payment_edit_form_submit()
 */
function freebil_payment_edit_form_validate($form, &$form_state) {
  $payment_date = check_plain($form_state['values']['payment_date']);
  if (ctype_space($payment_date) || $payment_date == '') {
    form_set_error('payment_date', t('Select a Payment Date.'));
  }
  $currency = check_plain($form_state['values']['currency_code']);
  if (ctype_space($currency) || $currency == '') {
    form_set_error('currency_code', t('You must populate the Currency Code.'));
  }
}

/**
 * Form submission handler for freebil_payment_edit_form().
 */
function freebil_payment_edit_form_submit($form, &$form_state) {

  $row = array(
    'currency_code' => $form_state['values']['currency_code'],
    'payment_amt' => $form_state['values']['payment_amt'],
    'payment_date' => $form_state['values']['payment_date'],
    'description' => $form_state['values']['description'],
    'notes' => $form_state['values']['notes'],
  );

  // Derive year and quarter from payment date for reporting purposes.
  $payment_date_parts = freebil_format_year_quarter($form_state['values']['payment_date']);
  $row['payment_year'] = $payment_date_parts['year'];
  $row['payment_quarter'] = $payment_date_parts['quarter'];

  // Update this customer row with data from the submitted form.
  freebil_payment_update($row, $form_state['values']['pay_id']);

  // Recalculate payment total and update the invoice row.
  freebil_recalc_invoice_payment_total_and_update($form_state['values']['inv_id']);

  $form_state['redirect'] = 'admin/freebil/invoices/' . $form_state['values']['inv_id'] . '/view/payments';
  drupal_set_message(t('Successfully updated Payment.'));
}

/**
 * Payment update API.
 *
 * @param array $row
 *   An associative array containing:
 *   - The column names as keys and the column values as values for
 *     the payment row to be updated.
 * @param string $pay_id
 *   The payment ID for the payment to be updated.
 *
 * @return object
 *   An UpdateQuery object
 */
function freebil_payment_update(array $row, $pay_id) {
  $row['last_update_ts'] = REQUEST_TIME;

  $update_query_obj = db_update('freebil_payment')
    ->fields($row)
    ->condition('pay_id', $pay_id, '=')
    ->execute();

  return $update_query_obj;
}

/**
 * Page Callback: A form to delete a payment.
 *
 * @param string $pay
 *   The payment ID for the payment to be deleted.
 *
 * @return array
 *   A form array
 *
 * @see freebil_menu()
 */
function freebil_payment_page_delete($pay) {
  return drupal_get_form('freebil_payment_delete_form', $pay);
}

/**
 * Form constructor for the payment delete form.
 *
 * @param string $pay
 *   The payment ID for the payment to delete.
 *
 * @return array
 *   A form array
 *
 * @see freebil_payment_delete_form_submit()
 *
 * @ingroup forms
 */
function freebil_payment_delete_form($form, &$form_state, $pay) {
  $pay_data = array();
  $pay_data = freebil_get_pay_data($pay);
  $inv_data = array();
  $inv_data = freebil_get_inv_data($pay_data['inv_id']);

  // Set the page breadcrumb.
  $crumbs = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('freebil'), 'admin/freebil'),
    l(t('Customers'), 'admin/freebil/customers'),
    l($inv_data['cust_name'], 'admin/freebil/customers/' . $inv_data['cust_id'] . '/view'),
    l($inv_data['inv_name'], 'admin/freebil/invoices/' . $inv_data['inv_id'] . '/view'),
    l($inv_data['inv_name'] . ' ' . t('Payments'), 'admin/freebil/invoices/' . $inv_data['inv_id'] . '/view/payments'),
  );
  drupal_set_breadcrumb($crumbs);

  $form = freebil_build_payment_delete_form($pay_data);

  return $form;
}

/**
 * Form submission handler for freebil_payment_delete_form().
 */
function freebil_payment_delete_form_submit($form, &$form_state) {
  freebil_payment_delete($form_state['values']['pay_id']);

  // Recalculate payment total and update the invoice.
  freebil_recalc_invoice_payment_total_and_update($form_state['values']['inv_id']);

  $form_state['redirect'] = 'admin/freebil/invoices/' . $form_state['values']['inv_id'] . '/view/payments';
  drupal_set_message(t('Successfully deleted Payment.'));
}

/**
 * Payment delete API.
 *
 * @param string $pay_id
 *   The payment ID for the payment to be deleted.
 *
 * @return object
 *   A DeleteQuery object
 */
function freebil_payment_delete($pay_id) {
  $delete_query_obj = db_delete('freebil_payment')
    ->condition('pay_id', $pay_id)
    ->execute();
  return $delete_query_obj;
}

/**
 * Builds forms for creating or editing payments.
 *
 * @param string $type
 *   Indicates the type of form to build: "create" or "edit".
 * @param array $pay_data
 *   An associative array containing:
 *   - All data for an existing payment with column names as
 *     keys and the column values as values.
 * @param array $inv_data
 *   An associative array containing:
 *   - All data for the current invoice with column names as
 *     keys and the column values as values.
 *
 * @return array
 *   A form array
 */
function freebil_build_payment_form($type, &$form_state, $pay_data = array(), $inv_data = array()) {
  // Current date to use as default value for date fields.
  $date = format_date(REQUEST_TIME, 'custom', 'Y-m-d H:i:s');

  if (array_key_exists("cust_name", $pay_data)) {
    $cust_name = $pay_data['cust_name'];
  }
  else {
    $cust_name = $inv_data['cust_name'];
  }

  $form['customer_name_display'] = array(
    '#markup' => t('<h5>Customer: @cust_name</h5>', array('@cust_name' => $cust_name)),
  );

  $cust_id = '';
  if (array_key_exists("cust_id", $pay_data)) {
    $cust_id = $pay_data['cust_id'];
  }
  elseif (array_key_exists("cust_id", $inv_data)) {
    $cust_id = $inv_data['cust_id'];
  }

  $form['cust_id'] = array(
    '#type' => 'hidden',
    '#value' => $cust_id,
  );

  if (array_key_exists("inv_id", $pay_data)) {
    $inv_id = $pay_data['inv_id'];
  }
  else {
    $inv_id = $inv_data['inv_id'];
  }

  $form['invoice_number_display'] = array(
    '#markup' => t('<h5>Invoice ID: @inv_id</h5>', array('@inv_id' => $inv_id)),
  );

  if ($type === "create") {
    $form['invoice_section']['inv_id'] = array(
      '#type' => 'hidden',
      '#value' => $inv_id,
    );
  }

  // The currency code used for the payment must be the same as the invoice.
  // The field is included as documentation, and is not editable.
  $currency_code = '';
  if (array_key_exists('currency_code', $inv_data)) {
    $currency_code = $inv_data['currency_code'];
  }
  elseif (array_key_exists('currency_code', $pay_data)) {
    $currency_code = $pay_data['currency_code'];
  }
  $form['currency_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Currency Code'),
    '#default_value' => $currency_code,
    '#size' => 4,
    '#maxlength' => 3,
    '#attributes' => array('readonly' => 'readonly'),
    '#description' => t('The currency code is specified on the invoice, and cannot be changed for the payment.'),
  );
  $form['payment_amt'] = array(
    '#type' => 'textfield',
    '#title' => 'Payment Amount',
    '#default_value' => '0.00',
    '#maxlength' => 12,
    '#element_validate' => array('element_validate_number'),
  );
  $form['payment_date'] = array(
    '#type' => 'date_popup',
    '#title' => t('Payment Date'),
    '#default_value' => $date,
    '#date_type' => DATE_DATETIME,
    '#date_timezone' => date_default_timezone(),
    '#date_format' => 'Y-m-d',
    '#date_increment' => 1,
    '#date_year_range' => '-3:+2',
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Payment Description'),
    '#maxlength' => 255,
    '#wysiwyg' => FALSE,
  );
  $form['notes'] = array(
    '#type' => 'textarea',
    '#title' => t('Notes'),
    '#maxlength' => 255,
    '#wysiwyg' => FALSE,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/freebil/invoices/' . $inv_id . '/view/payments'),
  );

  if ($type === "edit") {
    $form['pay_id'] = array(
      '#type' => 'hidden',
      '#value' => $pay_data['pay_id'],
    );

    $form['cust_id']['#default_value'] = $pay_data['cust_id'];
    $form['invoice_section']['inv_id']['#default_value'] = $pay_data['inv_id'];
    $form['currency_code']['#default_value'] = $pay_data['currency_code'];
    $form['payment_amt']['#default_value'] = $pay_data['payment_amt'];
    $form['payment_date']['#default_value'] = $pay_data['payment_date'];
    $form['description']['#default_value'] = $pay_data['description'];
    $form['notes']['#default_value'] = $pay_data['notes'];

    $form['inv_id'] = array(
      '#type' => 'hidden',
      '#value' => $pay_data['inv_id'],
    );
  }
  return $form;
}


/**
 * Builds a form for deleting payments.
 *
 * @param array $pay_data
 *   An associative array containing:
 *   - All data for an existing payment with column
 *     names as keys and the column values as values.
 *
 * @return array
 *   A form array
 */
function freebil_build_payment_delete_form(array $pay_data) {
  $header = array(
    'pay_id' => t('ID'),
    'cust_name' => t('Customer Name'),
    'inv_id' => t('Invoice Nbr'),
    'payment_amt' => t('Payment Amount'),
    'currency_code' => t('Currency'),
    'payment_date' => t('Payment Date'),
    'description' => t('Description'),
    'created_ts' => t('Created'),
    'last_update_ts' => t('Last Updated'),
  );

  $rows[0] = array(
    'pay_id' => $pay_data['pay_id'],
    'cust_name' => $pay_data['cust_name'],
    'inv_id' => $pay_data['inv_id'],
    'payment_amt' => $pay_data['payment_amt'],
    'currency_code' => $pay_data['currency_code'],
    'payment_date' => $pay_data['payment_date'],
    'description' => $pay_data['description'],
    'created_ts' => format_date($pay_data['created_ts'], 'short'),
    'last_update_ts' => format_date($pay_data['last_update_ts'], 'short'),
  );
  $form['identity'] = array(
    '#markup' => theme('table', array('header' => $header, 'rows' => $rows)),
  );
  $form['confirm'] = array(
    '#markup' => '<p>' . t('Are you sure you want to delete this payment?') . '</p>',
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/freebil/invoices/' . $pay_data['inv_id'] . '/view/payments'),
  );
  // Need the payment ID to delete the table row on form submit.
  $form['pay_id'] = array(
    '#type' => 'hidden',
    '#value' => $pay_data['pay_id'],
  );
  // Need the invoice ID to recalculate the payment total after delete.
  $form['inv_id'] = array(
    '#type' => 'hidden',
    '#value' => $pay_data['inv_id'],
  );

  return $form;
}

/**
 * Gets all payment data for the input payment ID.
 *
 * @param string $pay
 *   The payment ID to get data for.
 *
 * @return an associative array containing:
 *   - All data for the payment with column names as
 *     keys and the column values as values.
 */
function freebil_get_pay_data($pay) {
  $query = 'SELECT
   p.pay_id
  , p.inv_id
  , i.cust_id
  , c.cust_name
  , p.currency_code
  , p.payment_amt
  , p.payment_date
  , p.description
  , p.notes
  , p.created_ts
  , p.last_update_ts
   FROM
   {freebil_payment} p
   INNER JOIN {freebil_invoice} i
   ON p.inv_id = i.inv_id
   INNER JOIN {freebil_customer} c
   ON i.cust_id = c.cust_id
   WHERE
   pay_id = :payid';
  $result = db_query($query, array(':payid' => $pay));
  $record = $result->fetchObject();
  $output = array(
    'pay_id' => check_plain($record->pay_id),
    'inv_id' => check_plain($record->inv_id),
    'cust_id' => check_plain($record->cust_id),
    'cust_name' => freebil_check_text($record->cust_name),
    'currency_code' => check_plain($record->currency_code),
    'payment_amt' => check_plain($record->payment_amt),
    'payment_date' => check_plain($record->payment_date),
    'description' => freebil_check_text($record->description),
    'notes' => freebil_check_text($record->notes),
    'created_ts' => check_plain($record->created_ts),
    'last_update_ts' => check_plain($record->last_update_ts),
  );
  return $output;
}

/**
 * Get data for all payments related to the input invoice ID.
 *
 * @param string $inv_id
 *   The invoice ID to get related payments for.
 *
 * @return array
 *   An associative array containing:
 *   - An associative array containing:
 *     - The headings for each data column.
 *   - An associative array containing:
 *     - rows containing the data for each associated payment.
 */
function freebil_payments_for_invoice_query($inv_id) {
  $query = 'SELECT
   p.pay_id
  , p.inv_id
  , p.currency_code
  , p.payment_amt
  , p.payment_date
  , p.description
  , p.notes
  , p.created_ts
  , p.last_update_ts
   FROM
   {freebil_payment} p
   INNER JOIN {freebil_invoice} i
   ON p.inv_id = i.inv_id
   WHERE
   p.inv_id = :invid';
  $result = db_query($query, array(':invid' => $inv_id));

  return $result;
}

/**
 * Gets all payment IDs related to an invoice.
 *
 * @param string $inv_id
 *   The invoice ID.
 *
 * @return array containing:
 *   - All payment IDs related to the invoice ID
 */
function freebil_get_related_pay_ids($inv_id) {
  $query = 'SELECT
   pay_id
   FROM
   {freebil_payment}
   WHERE
   inv_id = :invid';
  $result = db_query($query, array(':invid' => $inv_id));

  $pay_id_array = array();
  foreach ($result as $row) {
    $pay_id_array[] = $row->pay_id;
  }
  return $pay_id_array;
}
