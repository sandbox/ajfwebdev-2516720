<?php

/**
 * @file
 * Renders the invoice page.
 *
 * The invoice page displays information for an individual invoice
 * and includes options for the user to manage invoice data.
 *
 * Invoice items, payments, and recipients all have sections on
 * this page.
 * Each section includes a link to add new, and a list of all
 * existing, along with links to edit or delete each individual item.
 *
 * This page also includes a link to generate a completed view of the invoice
 * including all data that will be sent to the customer.
 */

/**
 * Page Callback: Displays the invoice data with options to make changes.
 *
 * @param string $inv
 *   The invoice ID for the invoice to be displayed.
 *
 * @return string
 *   Markup for the page to display the invoice info.
 *
 * @see freebil_menu()
 */
function freebil_invoice_view_page($inv) {

  $inv_data = freebil_get_inv_data($inv);
  drupal_set_title($inv_data['cust_name'] . ', ' . $inv_data['inv_name']);

  // Set the page breadcrumb.
  $crumbs = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('freebil'), 'admin/freebil'),
    l(t('Customers'), 'admin/freebil/customers'),
    l($inv_data['cust_name'], 'admin/freebil/customers/' . $inv_data['cust_id'] . '/view/'),
    l($inv_data['cust_name'] . ' ' . t('Invoices'), 'admin/freebil/customers/' . $inv_data['cust_id'] . '/view/invoices'),
  );
  drupal_set_breadcrumb($crumbs);

  $inv_hdr_tbl = freebil_invoice_view_header_data($inv_data);
  $inv_item_hourly_tbl = freebil_invoice_item_hourly_data($inv);
  // Calling freebil_invoice_item_other_data() with TRUE makes it return items
  // with quantity and unit cost populated. FALSE excludes these items.
  $inv_item_fixed_rate_tbl = freebil_invoice_item_other_data($inv, 'Fixed Rate', FALSE);
  $inv_item_fixed_rate_qty_tbl = freebil_invoice_item_other_data($inv, 'Fixed Rate', TRUE);
  $inv_item_product_tbl = freebil_invoice_item_product_data($inv);
  $inv_item_expense_tbl = freebil_invoice_item_other_data($inv, 'Expense', FALSE);
  $inv_item_expense_qty_tbl = freebil_invoice_item_other_data($inv, 'Expense', TRUE);
  $inv_item_adjust_tbl = freebil_invoice_item_other_data($inv, 'Adjustment', FALSE);
  $inv_item_adjust_qty_tbl = freebil_invoice_item_other_data($inv, 'Adjustment', TRUE);
  $inv_item_tax_tbl = freebil_invoice_item_other_data($inv, 'Tax', FALSE);
  $inv_item_tax_qty_tbl = freebil_invoice_item_other_data($inv, 'Tax', TRUE);
  $inv_totals_tbl = freebil_invoice_view_totals_data($inv_data);
  $inv_balance_tbl = freebil_invoice_view_balance_data($inv_data);
  $inv_recipient_tbl = freebil_recipients_for_invoice_links($inv, TRUE);

  return theme('freebil_invoice_view_page', array(
    'inv_data' => $inv_data,
    'inv_header_tbl' => $inv_hdr_tbl,
    'item_hourly_tbl' => $inv_item_hourly_tbl,
    'item_fixed_tbl' => $inv_item_fixed_rate_tbl,
    'item_fixed_qty_tbl' => $inv_item_fixed_rate_qty_tbl,
    'item_product_tbl' => $inv_item_product_tbl,
    'item_expense_tbl' => $inv_item_expense_tbl,
    'item_expense_qty_tbl' => $inv_item_expense_qty_tbl,
    'item_adjust_tbl' => $inv_item_adjust_tbl,
    'item_adjust_qty_tbl' => $inv_item_adjust_qty_tbl,
    'item_tax_tbl' => $inv_item_tax_tbl,
    'item_tax_qty_tbl' => $inv_item_tax_qty_tbl,
    'inv_totals_tbl' => $inv_totals_tbl,
    'inv_balance_tbl' => $inv_balance_tbl,
    'inv_recipients_tbl' => $inv_recipient_tbl,
  ));
}

/**
 * Page Callback: Displays the list of payments for the current invoice.
 *
 * @param string $inv
 *   The invoice ID of the invoice to list payments for.
 *
 * @return string
 *   Markup for the page to display the payment list info.
 *
 * @see freebil_menu()
 */
function freebil_invoice_payments_page($inv) {

  $inv_data = freebil_get_inv_data($inv);
  drupal_set_title($inv_data['cust_name'] . ' ' . $inv_data['inv_name'] . ' ' . t('Payments'));

  // Set the page breadcrumb.
  $crumbs = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('freebil'), 'admin/freebil'),
    l(t('Customers'), 'admin/freebil/customers'),
    l($inv_data['cust_name'], 'admin/freebil/customers/' . $inv_data['cust_id'] . '/view'),
    l($inv_data['cust_name'] . ' ' . t('Invoices'), 'admin/freebil/customers/' . $inv_data['cust_id'] . '/view/invoices'),
    l($inv_data['inv_name'], 'admin/freebil/invoices/' . $inv_data['inv_id'] . '/view'),
  );
  drupal_set_breadcrumb($crumbs);

  $inv_payments_tbl = freebil_invoice_view_payments_list_data($inv);
  $inv_balance_tbl = freebil_invoice_view_balance_data($inv_data);

  return theme('freebil_invoice_payments_page', array(
    'inv_data' => $inv_data,
    'inv_payments_tbl' => $inv_payments_tbl,
    'inv_balance_tbl' => $inv_balance_tbl,
  ));
}

/**
 * Builds markup for rendering the invoice page.
 *
 * @param array $variables
 *   An associative array containing:
 *   - associative arrays for each data set needed to build
 *     the various sections of the invoice page.
 *
 * @return string
 *   The markup for the invoice page.
 *
 * @ingroup themeable
 */
function theme_freebil_invoice_view_page(array $variables) {
  $output = '';
  $inv_data = $variables['inv_data'];

  // Invoice header data.
  $output .= '<div id="freebil-invview-invdata" class="freebil-page-section">';
  $output .= '<h4>' . t('Invoice Info') . '</h4>';
  $output .= theme('table', array('header' => $variables['inv_header_tbl']['header'], 'rows' => $variables['inv_header_tbl']['rows']));

  // Invoice Operations Links.
  $inv_id = $inv_data['inv_id'];
  $output .= '<div class="freebil-operation">';
  $path = 'admin/freebil/invoices/' . $inv_id . '/edit';
  $output .= l(t('Edit Invoice Info'), $path, array('attributes' => array('class' => array('freebil-link'))));
  $output .= '</div>';
  $output .= '<div class="freebil-operation">';
  $path = 'admin/freebil/invoices/' . $inv_id . '/delete';
  $output .= l(t('Delete This Invoice'), $path, array('attributes' => array('class' => array('freebil-link'))));
  $output .= '</div>';

  $output .= '</div>';

  // Invoice Items Section.
  $output .= '<div id="freebil-invview-items" class="freebil-page-section">';
  $output .= '<h4>' . t('Invoice Items') . '</h4>';

  // Add New Item Link.
  $add_itm_links['add_itm'] = array(
    'href' => 'admin/freebil/invoices/' . $inv_data['inv_id'] . '/item/create',
    'title' => t('Add Item'),
  );
  $output .= theme('links', array(
     'links' => $add_itm_links,
     'attributes' => array('class' => array('action-links')),
   ));

  if (!empty($variables['item_hourly_tbl']['rows'])) {
    $output .= '<h4>' . t('Hourly Charges') . '</h4>';
    $output .= theme('table', array('header' => $variables['item_hourly_tbl']['header'], 'rows' => $variables['item_hourly_tbl']['rows']));
  }
  if (!empty($variables['item_fixed_tbl']['rows'])) {
    $output .= '<h4>' . t('Fixed Rate Charges') . '</h4>';
    $output .= theme('table', array('header' => $variables['item_fixed_tbl']['header'], 'rows' => $variables['item_fixed_tbl']['rows']));
  }
  if (!empty($variables['item_fixed_qty_tbl']['rows'])) {
    $output .= theme('table', array('header' => $variables['item_fixed_qty_tbl']['header'], 'rows' => $variables['item_fixed_qty_tbl']['rows']));
  }
  if (!empty($variables['item_product_tbl']['rows'])) {
    $output .= '<h4>' . t('Charges for Products') . '</h4>';
    $output .= theme('table', array('header' => $variables['item_product_tbl']['header'], 'rows' => $variables['item_product_tbl']['rows']));
  }
  if (!empty($variables['item_expense_tbl']['rows'])) {
    $output .= '<h4>' . t('Expenses') . '</h4>';
    $output .= theme('table', array('header' => $variables['item_expense_tbl']['header'], 'rows' => $variables['item_expense_tbl']['rows']));
  }
  if (!empty($variables['item_expense_qty_tbl']['rows'])) {
    $output .= theme('table', array('header' => $variables['item_expense_qty_tbl']['header'], 'rows' => $variables['item_expense_qty_tbl']['rows']));
  }
  if (!empty($variables['item_adjust_tbl']['rows'])) {
    $output .= '<h4>' . t('Adjustments') . '</h4>';
    $output .= theme('table', array('header' => $variables['item_adjust_tbl']['header'], 'rows' => $variables['item_adjust_tbl']['rows']));
  }
  if (!empty($variables['item_adjust_qty_tbl']['rows'])) {
    $output .= theme('table', array('header' => $variables['item_adjust_qty_tbl']['header'], 'rows' => $variables['item_adjust_qty_tbl']['rows']));
  }
  if (!empty($variables['item_tax_tbl']['rows'])) {
    $output .= '<h4>' . t('Taxes') . '</h4>';
    $output .= theme('table', array('header' => $variables['item_tax_tbl']['header'], 'rows' => $variables['item_tax_tbl']['rows']));
  }
  if (!empty($variables['item_tax_qty_tbl']['rows'])) {
    $output .= theme('table', array('header' => $variables['item_tax_qty_tbl']['header'], 'rows' => $variables['item_tax_qty_tbl']['rows']));
  }

  $output .= '</div>';

  // Invoice Totals Section.
  $output .= '<div id="freebil-invview-totals" class="freebil-page-section">';
  $output .= '<h4>' . t('Total Charges') . '</h4>';
  $output .= theme('table', array('header' => $variables['inv_totals_tbl']['header'], 'rows' => $variables['inv_totals_tbl']['rows']));
  $output .= '</div>';

  // Invoice Balance Section.
  $output .= '<div id="freebil-invview-balance" class="freebil-page-section">';
  $output .= '<h4>' . t('Invoice Balance') . '</h4><p>' . t('(As billed amount minus payment received)') . '</p>';
  $output .= theme('table', array('header' => $variables['inv_balance_tbl']['header'], 'rows' => $variables['inv_balance_tbl']['rows']));
  $output .= '</div>';

  // Invoice Email Recipients Section.
  $output .= '<div id="freebil-invview-recipients" class="freebil-page-section">';
  $output .= '<h4>' . t('Invoice Email Recipients') . '</h4><p>' . t('Lists email addresses that this invoice should be sent to.') . '</p>';

  // Add New Recipient Link.
  $add_recip_links['add_recip'] = array(
    'href' => 'admin/freebil/recipients/' . $inv_data['inv_id'] . '/create',
    'title' => t('Add Recipient'),
  );
  $output .= theme('links', array(
     'links' => $add_recip_links,
     'attributes' => array('class' => array('action-links')),
   ));

  if (!empty($variables['inv_recipients_tbl']['rows'])) {
    $output .= theme('table', array('header' => $variables['inv_recipients_tbl']['header'], 'rows' => $variables['inv_recipients_tbl']['rows']));
  }
  $output .= '</div>';

  // Generate Invoice Page Link.
  $output .= '<div id="freebil-invview-generate" class="freebil-page-section">';
  $output .= '<h4>' . t('Generate the Invoice for Delivery') . '</h4><p>' . t('This generates a completed view of the invoice including all data that will be sent to the customer.') . '</p>';
  $gen_inv_links['gen_inv'] = array(
    'href' => 'admin/freebil/invoices/' . $inv_data['inv_id'] . '/generate',
    'title' => t('Generate Invoice'),
  );
  $output .= theme('links', array(
     'links' => $gen_inv_links,
     'attributes' => array('class' => array('action-links')),
   ));
  $output .= '</div>';

  return $output;
}

/**
 * Builds markup for rendering the payment list page.
 *
 * @param array $variables
 *   An associative array containing:
 *   - associative arrays for each data set needed to
 *     build the various sections of the payment list page.
 *
 * @return string
 *   The markup for the payment list page.
 *
 * @ingroup themeable
 */
function theme_freebil_invoice_payments_page(array $variables) {
  $output = '';
  $inv_data = $variables['inv_data'];

  // Invoice Payments Section.
  $output .= '<div id="freebil-invview-payments" class="freebil-page-section">';
  $output .= '<h4>' . t('Payments') . '</h4>';

  // Add New Payment Link.
  $add_pay_links['add_pay'] = array(
    'href' => 'admin/freebil/payments/' . $inv_data['inv_id'] . '/create',
    'title' => t('Add Payment'),
  );
  $output .= theme('links', array(
     'links' => $add_pay_links,
     'attributes' => array('class' => array('action-links')),
   ));

  if (!empty($variables['inv_payments_tbl']['rows'])) {
    $output .= theme('table', array('header' => $variables['inv_payments_tbl']['header'], 'rows' => $variables['inv_payments_tbl']['rows']));
  }
  $output .= '</div>';

  // Invoice Balance Section.
  $output .= '<div id="freebil-invview-balance" class="freebil-page-section">';
  $output .= '<h4>' . t('Invoice Balance') . '</h4><p>' . t('(As billed amount minus payment received)') . '</p>';
  $output .= theme('table', array('header' => $variables['inv_balance_tbl']['header'], 'rows' => $variables['inv_balance_tbl']['rows']));
  $output .= '</div>';

  return $output;
}

/**
 * Calculates the balance due for the invoice.
 *
 * @param array $inv_data
 *   An associative array containing:
 *   - All data for the current invoice.
 *
 * @return array
 *   An associative array containing:
 *   - An associative array containing:
 *     - The headings for each data column.
 *   - An associative array containing:
 *     - one row containing the new total billed amount,
 *       total payments, and balance due.
 */
function freebil_invoice_view_balance_data(array $inv_data) {

  $rows[0] = array(
    'total_billed_amt' => $inv_data['total_billed_amt'],
    'total_payment_amt' => $inv_data['total_payment_amt'],
    'balance_due' => $inv_data['balance_due_amt'],
  );
  $row_header = array(
    'total_billed_amt' => t('Total Billed Amount'),
    'total_payment_amt' => t('Total Payment Received'),
    'balance_due' => t('Balance Due'),
  );

  return array('header' => $row_header, 'rows' => $rows);
}

/**
 * Gets all payments received for this invoice.
 *
 * Includes links to edit or delete each payment.
 *
 * @param string $inv_id
 *   The invoice ID for the invoice to get related payments for.
 *
 * @return array
 *   An associative array containing:
 *   - An associative array containing:
 *     - The headings for each data column.
 *   - An associative array containing:
 *     - Rows for each related payment including payment data
 *       and operations links.
 */
function freebil_invoice_view_payments_list_data($inv_id) {
  $rows = array();
  $count = 0;
  $operations = '';

  $result = freebil_payments_for_invoice_query($inv_id);
  foreach ($result as $row) {
    if (user_access('administer freebil')) {
      $op_links['edit'] = array(
        'href' => 'admin/freebil/payments/' . check_plain($row->pay_id) . '/edit',
        'title' => t('Edit'),
      );
      $op_links['delete'] = array(
        'href' => 'admin/freebil/payments/' . check_plain($row->pay_id) . '/delete',
        'title' => t('Delete'),
      );
      $operations = theme('links', array(
        'links' => $op_links,
        'attributes' => array('class' => array('links', 'inline')),
      ));
    }

    $rows[$count] = array(
      'pay_id' => check_plain($row->pay_id),
      'currency_code' => check_plain($row->currency_code),
      'payment_amt' => check_plain($row->payment_amt),
      'payment_date' => check_plain($row->payment_date),
      'description' => freebil_check_text($row->description),
      'notes' => freebil_check_text($row->notes),
      'created_ts' => check_plain(format_date($row->created_ts, 'short')),
      'last_update_ts' => check_plain(format_date($row->last_update_ts, 'short')),
      'operations' => $operations,
    );
    $count++;
  }

  $row_header = array(
    'pay_id' => t('Payment ID'),
    'currency_code' => t('Currency'),
    'payment_amt' => t('Payment Amount'),
    'payment_date' => t('Payment Date'),
    'description' => t('Description'),
    'notes' => t('Notes'),
    'created_ts' => t('Created'),
    'last_update_ts' => t('Last Updated'),
    'operations' => t('Operations'),
  );

  return array('header' => $row_header, 'rows' => $rows);
}

/**
 * Gets the high level (header) information for the current invoice.
 *
 * @param array $inv_data
 *   An associative array containing:
 *   - All data for the current invoice.
 *
 * @return array
 *   An associative array containing:
 *   - An associative array containing:
 *     - The headings for each data column.
 *   - An associative array containing:
 *     - One row with the header data.
 */
function freebil_invoice_view_header_data(array $inv_data) {
  $hdr_header = array(
    'inv_name' => t('Invoice Name'),
    'inv_id' => t('Invoice ID'),
    'cust_name' => t('Customer'),
    'description' => t('Description'),
    'status' => t('Status'),
    'invoice_sent' => t('Invoice Sent'),
    'currency_code' => t('Currency'),
    'opened_date' => t('Opened Date'),
    'sent_date' => t('Sent Date'),
    'closed_date' => t('Closed Date'),
  );

  if ($inv_data['open_closed_ind'] == 'O') {
    $status = "Open";
  }
  else {
    $status = "Closed";
  }
  if ($inv_data['invoice_sent'] == "N") {
    $sent_status = "No";
  }
  else {
    $sent_status = "Yes";
  }

  $hdr_values[0] = array(
    'inv_name' => $inv_data['inv_name'],
    'inv_id' => $inv_data['inv_id'],
    'cust_name' => $inv_data['cust_name'],
    'description' => $inv_data['description'],
    'status' => $status,
    'invoice_sent' => $sent_status,
    'currency_code' => $inv_data['currency_code'],
    'opened_date' => $inv_data['opened_date'],
    'sent_date' => $inv_data['sent_date'],
    'closed_date' => $inv_data['closed_date'],
  );

  return array('header' => $hdr_header, 'rows' => $hdr_values);
}

/**
 * Gets the total amounts for each invoice item type.
 *
 * Also gets total billed amount for the entire invoice.
 *
 * @param array $inv_data
 *   An associative array containing:
 *   - All data for the current invoice.
 *
 * @return array
 *   An associative array containing:
 *   - An associative array containing:
 *     - The headings for each data column.
 *   - An associative array containing:
 *     - One row with the total amounts.
 */
function freebil_invoice_view_totals_data(array $inv_data) {
  $header = array();
  $totals[0] = array();

  if ($inv_data['total_time_amt'] > 0.00) {
    $header['total_time_amt'] = t('Total Hourly');
    $totals[0]['total_time_amt'] = $inv_data['total_time_amt'];
  }
  if ($inv_data['total_fixedrate_amt'] > 0.00) {
    $header['total_fixedrate_amt'] = t('Total Fixed Rate');
    $totals[0]['total_fixedrate_amt'] = $inv_data['total_fixedrate_amt'];
  }
  if ($inv_data['total_product_amt'] > 0.00) {
    $header['total_product_amt'] = t('Total Products');
    $totals[0]['total_product_amt'] = $inv_data['total_product_amt'];
  }
  if ($inv_data['total_expense_amt'] > 0.00) {
    $header['total_expense_amt'] = t('Total Expenses');
    $totals[0]['total_expense_amt'] = $inv_data['total_expense_amt'];
  }
  if ($inv_data['total_adjust_amt'] != 0.00) {
    $header['total_adjust_amt'] = t('Total Adjustments');
    $totals[0]['total_adjust_amt'] = $inv_data['total_adjust_amt'];
  }
  if ($inv_data['total_tax_amt'] > 0.00) {
    $header['total_tax_amt'] = t('Total Taxes');
    $totals[0]['total_tax_amt'] = $inv_data['total_tax_amt'];
  }
  $header['total_billed_amt'] = t('Total Billed Amount');
  $totals[0]['total_billed_amt'] = $inv_data['total_billed_amt'];

  return array('header' => $header, 'rows' => $totals);
}

/**
 * Gets all hourly items related to this invoice.
 *
 * Includes links to edit or delete each item.
 *
 * @param string $inv_id
 *   The invoice ID for the invoice to get items for.
 *
 * @return array
 *   An associative array containing:
 *   - An associative array containing:
 *     - The headings for each data column.
 *   - An associative array containing:
 *     - Rows for each related hourly item.
 */
function freebil_invoice_item_hourly_data($inv_id) {
  $rows = array();
  $count = 0;
  $operations = '';

  $result = freebil_items_type_query($inv_id, 'Hourly');
  foreach ($result as $row) {
    if (user_access('administer freebil')) {
      $op_links['edit'] = array(
        'href' => 'admin/freebil/invoices/item/' . check_plain($row->itm_id) . '/edit',
        'title' => t('Edit'),
      );
      $op_links['delete'] = array(
        'href' => 'admin/freebil/invoices/item/' . check_plain($row->itm_id) . '/delete',
        'title' => t('Delete'),
      );
      $operations = theme('links', array(
        'links' => $op_links,
        'attributes' => array('class' => array('links', 'inline')),
      ));
    }

    $rows[$count] = array(
      'description' => freebil_check_text($row->description),
      'hours_worked' => check_plain($row->hours_worked),
      'hourly_rate' => check_plain($row->hourly_rate),
      'billed_amt' => check_plain($row->billed_amt),
      'created_ts' => check_plain(format_date($row->created_ts, 'short')),
      'last_update_ts' => check_plain(format_date($row->last_update_ts, 'short')),
      'operations' => $operations,
    );
    $count++;
  }

  $row_header = array(
    'description' => t('Description'),
    'hours_worked' => t('Hours Worked'),
    'hourly_rate' => t('Hourly Rate'),
    'billed_amt' => t('Billed Amount'),
    'created_ts' => t('Created'),
    'last_update_ts' => t('Last Updated'),
    'operations' => t('Operations'),
  );

  return array('header' => $row_header, 'rows' => $rows);
}

/**
 * Gets all product items related to this invoice.
 *
 * Includes links to edit or delete each item.
 *
 * @param string $inv_id
 *   The invoice ID for the invoice to get items for.
 *
 * @return array
 *   An associative array containing:
 *   - An associative array containing:
 *     - The headings for each data column.
 *   - An associative array containing:
 *     - Rows for each related product item.
 */
function freebil_invoice_item_product_data($inv_id) {
  $rows = array();
  $count = 0;
  $operations = '';

  $result = freebil_items_type_query($inv_id, 'Product');
  foreach ($result as $row) {
    if (user_access('administer freebil')) {
      $op_links['edit'] = array(
        'href' => 'admin/freebil/invoices/item/' . check_plain($row->itm_id) . '/edit',
        'title' => t('Edit'),
      );
      $op_links['delete'] = array(
        'href' => 'admin/freebil/invoices/item/' . check_plain($row->itm_id) . '/delete',
        'title' => t('Delete'),
      );
      $operations = theme('links', array(
        'links' => $op_links,
        'attributes' => array('class' => array('links', 'inline')),
      ));
    }

    $rows[$count] = array(
      'description' => freebil_check_text($row->description),
      'quantity' => check_plain($row->quantity),
      'unit_cost_amt' => check_plain($row->unit_cost_amt),
      'subtotal_amt' => check_plain($row->subtotal_amt),
      'tax_percent' => check_plain($row->tax_percent),
      'tax_amt' => check_plain($row->tax_amt),
      'billed_amt' => check_plain($row->billed_amt),
      'created_ts' => check_plain(format_date($row->created_ts, 'short')),
      'last_update_ts' => check_plain(format_date($row->last_update_ts, 'short')),
      'operations' => $operations,
    );
    $count++;
  }

  $row_header = array(
    'description' => t('Description'),
    'quantity' => t('Quantity'),
    'unit_cost_amt' => t('Unit Price'),
    'subtotal_amt' => t('Subtotal'),
    'tax_percent' => t('Tax Percent'),
    'tax_amt' => t('Tax Amount'),
    'billed_amt' => t('Billed Amount'),
    'created_ts' => t('Created'),
    'last_update_ts' => t('Last Updated'),
    'operations' => t('Operations'),
  );

  return array('header' => $row_header, 'rows' => $rows);
}

/**
 * Gets items of types that only require a description and a billed amount.
 *
 * Includes links to edit or delete each item.
 *
 * @param string $inv_id
 *   The invoice ID for the invoice to get items for.
 * @param string $type
 *   The item type to get (e.g. "Expense").
 * @param bool $quantity
 *   Whether to include quantity and unit cost in the results.
 *   Default is FALSE.
 *
 * @return array
 *   An associative array containing:
 *   - An associative array containing:
 *     - The headings for each data column.
 *   - An associative array containing:
 *     - Rows for each related item.
 */
function freebil_invoice_item_other_data($inv_id, $type, $quantity = FALSE) {
  $rows = array();
  $count = 0;
  $operations = '';

  $result = freebil_items_type_query($inv_id, $type, $quantity);
  foreach ($result as $row) {
    if (user_access('administer freebil')) {
      $op_links['edit'] = array(
        'href' => 'admin/freebil/invoices/item/' . check_plain($row->itm_id) . '/edit',
        'title' => t('Edit'),
      );
      $op_links['delete'] = array(
        'href' => 'admin/freebil/invoices/item/' . check_plain($row->itm_id) . '/delete',
        'title' => t('Delete'),
      );
      $operations = theme('links', array(
        'links' => $op_links,
        'attributes' => array('class' => array('links', 'inline')),
      ));
    }

    $rows[$count]['description'] = freebil_check_text($row->description);
    if ($quantity) {
      $rows[$count]['quantity'] = freebil_check_text($row->quantity);
      $rows[$count]['unit_cost_amt'] = freebil_check_text($row->unit_cost_amt);
    }
    $rows[$count]['billed_amt'] = freebil_check_text($row->billed_amt);
    $rows[$count]['created_ts'] = check_plain(format_date($row->created_ts, 'short'));
    $rows[$count]['last_update_ts'] = check_plain(format_date($row->last_update_ts, 'short'));
    $rows[$count]['operations'] = $operations;
    $count++;
  }

  $row_header['description'] = t('Description');
  if ($quantity) {
    $row_header['quantity'] = t('Quantity');
    $row_header['unit_cost_amt'] = t('Unit Cost');
  }
  $row_header['billed_amt'] = t('Billed Amount');
  $row_header['created_ts'] = t('Created');
  $row_header['last_update_ts'] = t('Last Updated');
  $row_header['operations'] = t('Operations');

  return array('header' => $row_header, 'rows' => $rows);
}
