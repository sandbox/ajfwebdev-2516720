<?php

/**
 * @file
 * Manages customer and contact email addresses.
 *
 * Customers and Contacts have email addresses.
 * The email addresses are accessed from Customer and Contact pages.
 */

/**
 * Page Callback: A form to create an email.
 *
 * @return array
 *   A form array
 *
 * @see freebil_menu()
 * @see freebil_email_create_form()
 *
 * @ingroup forms
 */
function freebil_email_page_create($cust, $sender) {
  return drupal_get_form('freebil_email_create_form', $cust, $sender);
}

/**
 * Form constructor for the email create form.
 *
 * @param string $cust_id
 *   The customer ID to create the email for.
 * @param string $sender
 *   Indicates which page this form was called from.
 *   Values are: 'contacts' or 'customers'.
 *
 * @return array
 *   A form array
 *
 * @see freebil_email_create_form_validate()
 * @see freebil_email_create_form_submit()
 *
 * @ingroup forms
 */
function freebil_email_create_form($form, &$form_state, $cust_id, $sender) {

  $cust_data = freebil_get_cust_data($cust_id);
  $parent_data = array();
  if ($sender == 'contacts') {
    $parent_data = freebil_get_cust_data($cust_data['parent_cust_id']);
  }
  $email_data = array();

  // Set the page breadcrumb.
  $crumbs = array();
  $crumbs[] = l(t('Home'), '<front>');
  $crumbs[] = l(t('Administration'), 'admin');
  $crumbs[] = l(t('freebil'), 'admin/freebil');
  $crumbs[] = l(t('Customers'), 'admin/freebil/customers');
  if ($sender == 'contacts') {
    $crumbs[] = l($parent_data['cust_name'], 'admin/freebil/customers/' . $parent_data['cust_id'] . '/view');
    $crumbs[] = l($cust_data['cust_name'], 'admin/freebil/contacts/' . $cust_data['cust_id'] . '/view');
  }
  else {
    $crumbs[] = l($cust_data['cust_name'], 'admin/freebil/customers/' . $cust_data['cust_id'] . '/view');
  }
  drupal_set_breadcrumb($crumbs);

  return freebil_build_email_form("create", $sender, $cust_data, $email_data);
}

/**
 * Form validation handler for freebil_email_create_form().
 *
 * @see freebil_email_create_form_submit()
 */
function freebil_email_create_form_validate($form, &$form_state) {
  $input_email = check_plain($form_state['values']['email_addr']);
  if (ctype_space($input_email) || $input_email == '') {
    form_set_error('email_addr', t('Please input an email address.'));
  }
}

/**
 * Form submission handler for freebil_email_create_form().
 *
 * @see freebil_email_create_form_validate()
 */
function freebil_email_create_form_submit($form, &$form_state) {

  $row = array(
    'cust_id' => $form_state['values']['cust_id'],
    'email_addr' => $form_state['values']['email_addr'],
    'email_type' => $form_state['values']['email_type'],
    'description' => $form_state['values']['description'],
  );
  freebil_email_insert($row);

  $form_state['redirect'] = 'admin/freebil/' . $form_state['values']['sender'] . '/' . $form_state['values']['cust_id'] . '/view';
  drupal_set_message(t('Successfully saved Email.'));
}

/**
 * Email insert API.
 *
 * @param array $row
 *   An associative array containing:
 *   - The column names as keys and the column values as values for
 *     the email row to be inserted.
 *
 * @return object
 *   - InsertQuery object
 */
function freebil_email_insert(array $row) {
  $row['created_ts'] = REQUEST_TIME;
  $row['last_update_ts'] = REQUEST_TIME;
  $obj = db_insert('freebil_email')
    ->fields($row)->execute();

  return $obj;
}

/**
 * Page Callback: A form to edit an Email.
 *
 * @param string $email
 *   The email ID for the email to be edited.
 * @param string $sender
 *   Indicates which page this form was called from.
 *   Values are: 'contacts' or 'customers'.
 *
 * @return array
 *   A form array
 *
 * @see freebil_menu()
 */
function freebil_email_page_edit($email, $sender) {
  return drupal_get_form('freebil_email_edit_form', $email, $sender);
}

/**
 * Form constructor for the email edit form.
 *
 * @param string $email
 *   The email ID for the email to edit.
 * @param string $sender
 *   Indicates which page this form was called from.
 *   Values are: 'contacts' or 'customers'.
 *
 * @return array
 *   A form array
 *
 * @see freebil_email_edit_form_validate()
 * @see freebil_email_edit_form_submit()
 *
 * @ingroup forms
 */
function freebil_email_edit_form($form, &$form_state, $email, $sender) {
  $email_data = freebil_get_email_data($email);
  $cust_data = freebil_get_cust_data($email_data['cust_id']);
  $parent_data = array();
  if ($sender == 'contacts') {
    $parent_data = freebil_get_cust_data($cust_data['parent_cust_id']);
  }

  // Set the page breadcrumb.
  $crumbs = array();
  $crumbs[] = l(t('Home'), '<front>');
  $crumbs[] = l(t('Administration'), 'admin');
  $crumbs[] = l(t('freebil'), 'admin/freebil');
  $crumbs[] = l(t('Customers'), 'admin/freebil/customers');
  if ($sender == 'contacts') {
    $crumbs[] = l($parent_data['cust_name'], 'admin/freebil/customers/' . $parent_data['cust_id'] . '/view');
    $crumbs[] = l($cust_data['cust_name'], 'admin/freebil/contacts/' . $cust_data['cust_id'] . '/view');
  }
  else {
    $crumbs[] = l($cust_data['cust_name'], 'admin/freebil/customers/' . $cust_data['cust_id'] . '/view');
  }
  drupal_set_breadcrumb($crumbs);

  $form = freebil_build_email_form("edit", $sender, $cust_data, $email_data);

  return $form;
}

/**
 * Form validation handler for freebil_email_page_edit().
 *
 * @see freebil_email_edit_form_submit()
 */
function freebil_email_edit_form_validate($form, &$form_state) {
  $input_email = check_plain($form_state['values']['email_addr']);
  if (ctype_space($input_email) || $input_email == '') {
    form_set_error('email_addr', t('Please input an email address.'));
  }
}

/**
 * Form submission handler for freebil_email_page_edit().
 *
 * @see freebil_email_edit_form_validate()
 */
function freebil_email_edit_form_submit($form, &$form_state) {

  $row = array(
    'cust_id' => $form_state['values']['cust_id'],
    'email_addr' => $form_state['values']['email_addr'],
    'email_type' => $form_state['values']['email_type'],
    'description' => $form_state['values']['description'],
  );

  // Update this email row with data from the submitted form.
  freebil_email_update($row, $form_state['values']['email_id']);

  $form_state['redirect'] = 'admin/freebil/' . $form_state['values']['sender'] . '/' . $form_state['values']['cust_id'] . '/view';
  drupal_set_message(t('Successfully updated Email.'));
}

/**
 * Email update API.
 *
 * @param array $row
 *   An associative array containing:
 *   - The column names as keys and the column values as values for
 *     the email row to be updated.
 * @param string $email_id
 *   The email ID for the email to be updated.
 *
 * @return object
 *   An UpdateQuery object
 */
function freebil_email_update(array $row, $email_id) {
  $row['last_update_ts'] = REQUEST_TIME;
  $update_query_obj = db_update('freebil_email')
    ->fields($row)
    ->condition('email_id', $email_id, '=')
    ->execute();

  return $update_query_obj;
}

/**
 * Page Callback: A form to delete an Email.
 *
 * @param string $email
 *   The email ID for the email to be deleted.
 * @param string $sender
 *   Indicates which page this form was called from.
 *   Values are: 'contacts' or 'customers'.
 *
 * @return array
 *   A form array
 *
 * @see freebil_menu()
 */
function freebil_email_page_delete($email, $sender) {
  return drupal_get_form('freebil_email_delete_form', $email, $sender);
}

/**
 * Form constructor for the email delete form.
 *
 * @param string $email
 *   The customer ID to delete.
 * @param string $sender
 *   Indicates which page this form was called from.
 *   Values are: 'contacts' or 'customers'.
 *
 * @return array
 *   A form array
 *
 * @see freebil_email_delete_form_submit()
 *
 * @ingroup forms
 */
function freebil_email_delete_form($form, &$form_state, $email, $sender) {
  $email_data = freebil_get_email_data($email);
  $cust_data = freebil_get_cust_data($email_data['cust_id']);
  $parent_data = array();
  if ($sender == 'contacts') {
    $parent_data = freebil_get_cust_data($cust_data['parent_cust_id']);
  }

  // Set the page breadcrumb.
  $crumbs = array();
  $crumbs[] = l(t('Home'), '<front>');
  $crumbs[] = l(t('Administration'), 'admin');
  $crumbs[] = l(t('freebil'), 'admin/freebil');
  $crumbs[] = l(t('Customers'), 'admin/freebil/customers');
  if ($sender == 'contacts') {
    $crumbs[] = l($parent_data['cust_name'], 'admin/freebil/customers/' . $parent_data['cust_id'] . '/view');
    $crumbs[] = l($cust_data['cust_name'], 'admin/freebil/contacts/' . $cust_data['cust_id'] . '/view');
  }
  else {
    $crumbs[] = l($cust_data['cust_name'], 'admin/freebil/customers/' . $cust_data['cust_id'] . '/view');
  }
  drupal_set_breadcrumb($crumbs);

  $form = freebil_build_email_delete_form($email_data, $sender);

  return $form;
}

/**
 * Form submission handler for freebil_email_delete_form().
 */
function freebil_email_delete_form_submit($form, &$form_state) {
  freebil_email_delete($form_state['values']['email_id']);

  $form_state['redirect'] = 'admin/freebil/' . $form_state['values']['sender'] . '/' . $form_state['values']['cust_id'] . '/view';
  drupal_set_message(t('Successfully deleted Email.'));
}

/**
 * Email delete API.
 *
 * @param string $email_id
 *   The email ID for the email to be deleted.
 *
 * @return object
 *   A DeleteQuery object
 */
function freebil_email_delete($email_id) {
  $delete_query_obj = db_delete('freebil_email')
    ->condition('email_id', $email_id)
    ->execute();
  return $delete_query_obj;
}

/**
 * Builds forms for creating or editing emails.
 *
 * @param string $type
 *   Indicates the type of form to build: "create" or "edit".
 * @param string $sender
 *   Indicates which page this form was called from.
 *   Values are: 'contacts' or 'customers'.
 * @param array $cust_data
 *   An associative array containing:
 *   - All data for the parent customer (or contact) with
 *     column names as keys and the column values as values.
 * @param array $email_data
 *   An associative array containing:
 *   - All data for an existing email with column names as keys
 *     and the column values as values.
 *
 * @return array
 *   A form array
 */
function freebil_build_email_form($type, $sender, array $cust_data, $email_data = array()) {

  $form['sender'] = array(
    '#type' => 'hidden',
    '#value' => $sender,
  );

  if ($type === "edit") {
    $form['email_id_display'] = array(
      '#markup' => t('<h5>Email ID: @email_id</h5>', array('@email_id' => $email_data['email_id'])),
    );
  }

  $cust_name = '';
  if (array_key_exists("cust_name", $email_data)) {
    $cust_name = $email_data['cust_name'];
  }
  elseif (array_key_exists("cust_name", $cust_data)) {
    $cust_name = $cust_data['cust_name'];
  }

  if ($sender == 'customers') {
    $sender_display = 'Customer';
  }
  else {
    $sender_display = 'Contact';
  }
  $form['customer_name_display'] = array(
    '#markup' => '<h5>' . $sender_display . ': ' . $cust_name . '</h5>',
  );

  $cust_id = '';
  if (array_key_exists("cust_id", $email_data)) {
    $cust_id = $email_data['cust_id'];
  }
  elseif (array_key_exists("cust_id", $cust_data)) {
    $cust_id = $cust_data['cust_id'];
  }
  $form['cust_id'] = array(
    '#type' => 'hidden',
    '#value' => $cust_id,
  );

  $form['email_addr'] = array(
    '#type' => 'textfield',
    '#title' => t('Email Address'),
    '#maxlength' => 25,
    '#wysiwyg' => FALSE,
    '#description' => t('Add the full email address here.'),
  );
  $form['email_type'] = array(
    '#type' => 'select',
    '#title' => t('Email Type'),
    '#options' => array('Work' => t('Work'), 'Personal' => t('Personal')),
    '#default_value' => 'Work',
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Email Description'),
    '#maxlength' => 255,
    '#wysiwyg' => FALSE,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  if ($type === "create") {
    $form['actions']['cancel'] = array(
      '#markup' => l(t('Cancel'), 'admin/freebil/' . $sender . '/' . $cust_id . '/view'),
    );
  }
  else {
    $form['actions']['cancel'] = array(
      '#markup' => l(t('Cancel'), 'admin/freebil/' . $sender . '/' . $cust_id . '/view'),
    );
  }

  // If this is the edit form, populate the values from the email table.
  if ($type === "edit") {
    // Need the email ID to update the table row on form submit.
    $form['email_id'] = array(
      '#type' => 'hidden',
      '#value' => $email_data['email_id'],
    );
    $form['email_addr']['#default_value'] = $email_data['email_addr'];
    $form['email_type']['#default_value'] = $email_data['email_type'];
    $form['description']['#default_value'] = $email_data['description'];
  }

  return $form;
}

/**
 * Builds a form for deleting an email address.
 *
 * @param array $email_data
 *   An associative array containing:
 *   - All data for an existing email with column names
 *     as keys and the column values as values.
 * @param string $sender
 *   Indicates which page this form was called from.
 *   Values are: 'contacts' or 'customers'.
 *
 * @return array
 *   A form array
 */
function freebil_build_email_delete_form(array $email_data, $sender) {
  $form['sender'] = array(
    '#type' => 'hidden',
    '#value' => $sender,
  );

  $header = array(
    t('ID'),
    t('Customer Name'),
    t('Email'),
    t('Type'),
    t('Description'),
    t('Created'),
    t('Last Updated'),
  );

  $rows[0] = array(
    'email_id' => $email_data['email_id'],
    'cust_name' => $email_data['cust_name'],
    'email_addr' => $email_data['email_addr'],
    'email_type' => $email_data['email_type'],
    'description' => $email_data['description'],
    'created_ts' => $email_data['created_ts'],
    'last_update_ts' => $email_data['last_update_ts'],
  );
  $form['identity'] = array(
    '#markup' => theme('table', array('header' => $header, 'rows' => $rows)),
  );
  $form['confirm'] = array(
    '#markup' => t('<p>Are you sure you want to delete this email address?</p>'),
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/freebil/' . $sender . '/' . $email_data['cust_id'] . '/view'),
  );
  // Need the email ID to delete the table row on form submit.
  $form['email_id'] = array(
    '#type' => 'hidden',
    '#value' => $email_data['email_id'],
  );
  // Need cust ID to redirect back to customer view page after delete.
  $form['cust_id'] = array(
    '#type' => 'hidden',
    '#value' => $email_data['cust_id'],
  );

  return $form;
}

/**
 * Gets all email rows for this customer and any associated contacts.
 *
 * @param string $cust_id
 *   The customer ID to get associated emails for.
 *
 * @return array
 *   An associative array containing:
 *   - An associative array containing:
 *     - The headings for each data column.
 *   - An associative array containing:
 *     - rows containing the email data for each column
 *       for each associated email.
 */
function freebil_cust_and_contact_emails($cust_id) {
  $rows = array();
  $result = freebil_cust_and_contact_emails_query($cust_id);

  foreach ($result as $row) {

    if (empty($row->parent_cust_id)) {
      $type = 'Customer';
    }
    else {
      $type = 'Contact';
    }

    $rows[$row->email_id] = array(
      'cust_name' => freebil_check_text($row->cust_name),
      'type' => $type,
      'email_addr' => check_plain($row->email_addr),
      'email_type' => check_plain($row->email_type),
      'description' => freebil_check_text($row->description),
      'created_ts' => check_plain(format_date($row->created_ts, 'short')),
      'last_update_ts' => check_plain(format_date($row->last_update_ts, 'short')),
    );
  }
  // Because this is used for a 'tableselect' form element,
  // the header array must also have indexes specified.
  // And the indexes must match the ones used above for the columns of each row!
  $row_header = array(
    'cust_name' => t('Name'),
    'type' => t('Type'),
    'email_addr' => t('Email'),
    'email_type' => t('Email Type'),
    'description' => t('Description'),
    'created_ts' => t('Created'),
    'last_update_ts' => t('Last Updated'),
  );

  return array('header' => $row_header, 'rows' => $rows);
}

/**
 * Get emails for the customer and related contacts.
 *
 * @param string $cust_id
 *   The customer ID to get emails for.
 *
 * @return object
 *   A DatabaseStatementInterface prepared statement
 *   object, already executed.
 */
function freebil_cust_and_contact_emails_query($cust_id) {
  $query = 'SELECT
   c.cust_id
  , c.parent_cust_id
  , c.cust_name
  , e.email_id
  , e.email_addr
  , e.email_type
  , e.description
  , e.created_ts
  , e.last_update_ts
   FROM
   {freebil_customer} c
   INNER JOIN
   {freebil_email} e
   ON e.cust_id = c.cust_Id
   WHERE
   c.cust_id = :custid
   OR c.parent_cust_id = :custid
   ORDER BY c.cust_name, e.created_ts';
  return db_query($query, array(':custid' => $cust_id));
}

/**
 * Gets all email rows for the input customer ID.
 *
 * The returned email address rows include links to edit or delete the email.
 *
 * @param string $cust_id
 *   The customer ID to get associated emails for.
 * @param string $sender
 *   Indicates which page this form was called from.
 *   Values are: 'contacts' or 'customers'.
 *
 * @return array
 *   An associative array containing:
 *   - An associative array containing:
 *     - The headings for each data column.
 *   - An associative array containing:
 *     - rows containing the email data for each associated email.
 */
function freebil_emails_for_cust_links($cust_id, $sender) {
  $rows = array();
  $count = 0;
  $operations = '';

  $result = freebil_emails_for_cust_query($cust_id);
  foreach ($result as $row) {
    if (user_access('administer freebil')) {
      $op_links['edit'] = array(
        'href' => 'admin/freebil/email/' . check_plain($row->email_id) . '/' . $sender . '/edit',
        'title' => t('Edit'),
      );
      $op_links['delete'] = array(
        'href' => 'admin/freebil/email/' . check_plain($row->email_id) . '/' . $sender . '/delete',
        'title' => t('Delete'),
      );
      $operations = theme('links', array(
        'links' => $op_links,
        'attributes' => array('class' => array('links', 'inline')),
      ));
    }

    $rows[$count] = array(
      'email_addr' => check_plain($row->email_addr),
      'email_type' => check_plain($row->email_type),
      'description' => freebil_check_text($row->description),
      'created_ts' => check_plain(format_date($row->created_ts, 'short')),
      'last_update_ts' => check_plain(format_date($row->last_update_ts, 'short')),
      'operations' => $operations,
    );
    $count++;
  }

  $row_header = array(
    'email_addr' => t('Email Address'),
    'email_type' => t('Type'),
    'description' => t('Description'),
    'created_ts' => t('Created'),
    'last_update_ts' => t('Last Updated'),
    'operations' => t('Operations'),
  );

  return array('header' => $row_header, 'rows' => $rows);
}

/**
 * Builds and executes a query to get emails for the input customer ID.
 *
 * @param string $cust_id
 *   The customer ID to get emails for.
 *
 * @return object
 *   A DatabaseStatementInterface prepared statement object, already executed.
 */
function freebil_emails_for_cust_query($cust_id) {
  $query = 'SELECT
   email_id
  , cust_id
  , email_addr
  , email_type
  , description
  , created_ts
  , last_update_ts
   FROM
   {freebil_email}
   WHERE
   cust_id = :custid';
  return db_query($query, array(':custid' => $cust_id));
}

/**
 * Gets all email data for the input email ID.
 *
 * @param string $email_id
 *   The email ID to get data for.
 *
 * @return array
 *   An associative array containing:
 *   -  The column names as keys and the column values
 *      as values for the email database row.
 */
function freebil_get_email_data($email_id) {
  $result = freebil_email_query($email_id);
  $record = $result->fetchObject();
  $row = array(
    'email_id' => check_plain($record->email_id),
    'cust_id' => check_plain($record->cust_id),
    'cust_name' => freebil_check_text($record->cust_name),
    'email_addr' => check_plain($record->email_addr),
    'email_type' => check_plain($record->email_type),
    'description' => freebil_check_text($record->description),
    'created_ts' => check_plain(format_date($record->created_ts, 'short')),
    'last_update_ts' => check_plain(format_date($record->last_update_ts, 'short')),
  );

  return $row;
}

/**
 * Builds and executes a query to get email data for the input email ID.
 *
 * @param string $email_id
 *   The email ID to get data for.
 *
 * @return object
 *   A DatabaseStatementInterface prepared statement object, already executed.
 */
function freebil_email_query($email_id) {
  $query = 'SELECT
   e.email_id
  , e.cust_id
  , c.cust_name
  , e.email_addr
  , e.email_type
  , e.description
  , e.created_ts
  , e.last_update_ts
   FROM
   {freebil_email} e
   INNER JOIN {freebil_customer} c
   ON c.cust_id = e.cust_id
   WHERE
   email_id = :emailid';
  return db_query($query, array(':emailid' => $email_id));
}

/**
 * Gets all email IDs related to a customer.
 *
 * @param string $cust_id
 *   The customer ID.
 *
 * @return array containing:
 *   - All email IDs related to the customer ID
 */
function freebil_get_related_email_ids($cust_id) {
  $query = 'SELECT
   email_id
   FROM
   {freebil_email}
   WHERE
   cust_id = :custid';
  $result = db_query($query, array(':custid' => $cust_id));

  $ids = array();
  foreach ($result as $row) {
    $ids[] = $row->email_id;
  }
  return $ids;
}
