<?php

/**
 * @file
 * Manages invoice recipients.
 *
 * Recipients are customers or contacts that the invoice will be sent to.
 *
 * Invoices have recipients, so the interface for selecting which
 * recipient(s) to send the invoice to is on the Invoice page.
 *
 * Recipient table rows map an invoice to the email address(es)
 * of the related customer and/or contacts. This deterimes
 * which email address(es) to use when sending the invoice.
 * Since the recipient rows have no data of their own,
 * they can be inserted and deleted, but not updated.
 */

/**
 * Page Callback: A form to create a recipient.
 *
 * @return array
 *   A form array
 *
 * @see freebil_menu()
 * @see freebil_recipient_create_form()
 *
 * @ingroup forms
 */
function freebil_recipient_page_create($inv) {
  return drupal_get_form('freebil_recipient_create_form', $inv);
}

/**
 * Form constructor for the recipient create form.
 *
 * @param string $inv_id
 *   The invoice ID to create the recipient for.
 *
 * @return array
 *   A form array
 *
 * @see freebil_recipient_create_form_submit()
 *
 * @ingroup forms
 */
function freebil_recipient_create_form($form, &$form_state, $inv_id) {

  $inv_data = freebil_get_inv_data($inv_id);

  $crumbs = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('freebil'), 'admin/freebil'),
    l(t('Customers'), 'admin/freebil/customers'),
    l($inv_data['cust_name'], 'admin/freebil/customers/' . $inv_data['cust_id'] . '/view'),
    l($inv_data['inv_name'], 'admin/freebil/invoices/' . $inv_data['inv_id'] . '/view'),
  );
  drupal_set_breadcrumb($crumbs);

  return freebil_build_recipient_create_form($inv_data);
}

/**
 * Form submission handler for freebil_invoice_create_form().
 *
 * Associates the chosen recipent(s) to this invoice
 * unless they are already associated.
 */
function freebil_recipient_create_form_submit($form, &$form_state) {
  $count = 0;
  foreach ($form_state['values']['recipient_table'] as $email_id) {
    if ($email_id > 0) {
      $email_data = freebil_get_email_data($email_id);
      $cust_data = freebil_get_cust_data($email_data['cust_id']);

      if (empty($cust_data['parent_cust_id'])) {
        $recip_type = 'Customer';
      }
      else {
        $recip_type = 'Contact';
      }

      $row = array(
        'inv_id' => $form_state['values']['inv_id'],
        'cust_id' => $email_data['cust_id'],
        'email_id' => $email_id,
        'recip_type' => $recip_type,
      );
      if (!freebil_recip_exists($form_state['values']['inv_id'], $email_id)) {
        $email_id = freebil_recipient_insert($row);
        $count++;
      }
    }
  }

  $form_state['redirect'] = 'admin/freebil/invoices/' . $form_state['values']['inv_id'] . '/view';

  if ($count > 1) {
    $msg = 'Successfully saved ' . $count . ' Recipients.';
  }
  elseif ($count == 0) {
    $msg = 'Recipient is already associated with this invoice, or no recipient chosen.';
  }
  else {
    $msg = 'Successfully saved ' . $count . ' Recipient.';
  }
  drupal_set_message($msg);
}

/**
 * Checks if the email id is already associated with invoice.
 *
 * @param string $inv_id
 *   The invoice ID of the current invoice.
 * @param string $email_id
 *   The email ID of the email selected as a recipient for this invoice.
 *
 * @return bool
 *   TRUE if already associated, FALSE if not.
 */
function freebil_recip_exists($inv_id, $email_id) {
  $query = 'SELECT
   inv_id
   FROM
   {freebil_recipient}
   WHERE
   inv_id = :invid
   AND email_id = :emailid';
  $result = db_query($query, array(':invid' => $inv_id, ':emailid' => $email_id));
  $row_count = $result->rowCount();
  if ($row_count > 0) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Recipient insert API.
 *
 * @param array $row
 *   An associative array containing:
 *   - The column names as keys and the column values as values for
 *     the recipient row to be inserted.
 *
 * @return object
 *   - InsertQuery object
 */
function freebil_recipient_insert(array $row) {

  $recip_id = db_insert('freebil_recipient')
    ->fields(array(
       'inv_id' => $row['inv_id'],
       'cust_id' => $row['cust_id'],
       'email_id' => $row['email_id'],
       'recip_type' => $row['recip_type'],
       'created_ts' => REQUEST_TIME,
    ))->execute();

  return $recip_id;
}

/**
 * Page Callback: A form to delete a recipient.
 *
 * @param string $recip
 *   The recipient ID for the recipient to be deleted.
 *
 * @return array
 *   A form array
 *
 * @see freebil_menu()
 */
function freebil_recipient_page_delete($recip) {
  return drupal_get_form('freebil_recipient_delete_form', $recip);
}

/**
 * Form constructor for the recipient delete form.
 *
 * @param string $recip
 *   The recipient ID for the recipient to delete.
 *
 * @return array
 *   A form array
 *
 * @see freebil_recipient_delete_form_submit()
 *
 * @ingroup forms
 */
function freebil_recipient_delete_form($form, &$form_state, $recip) {

  $recip_data = freebil_get_recipient_data($recip);
  $inv_data = freebil_get_inv_data($recip_data['inv_id']);

  $crumbs = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('freebil'), 'admin/freebil'),
    l(t('Customers'), 'admin/freebil/customers'),
    l($inv_data['cust_name'], 'admin/freebil/customers/' . $recip_data['cust_id'] . '/view'),
    l($inv_data['inv_name'], 'admin/freebil/invoices/' . $recip_data['inv_id'] . '/view'),
  );
  drupal_set_breadcrumb($crumbs);

  $form = freebil_build_recipient_delete_form($recip_data);

  return $form;
}

/**
 * Form submission handler for freebil_recipient_delete_form().
 */
function freebil_recipient_delete_form_submit($form, &$form_state) {
  freebil_recipient_delete($form_state['values']['recip_id']);

  $form_state['redirect'] = 'admin/freebil/invoices/' . $form_state['values']['inv_id'] . '/view';
  drupal_set_message(t('Successfully removed Recipient.'));
}

/**
 * Recipient delete API.
 *
 * @param string $recip_id
 *   The recipient ID for the recipient to be deleted.
 *
 * @return object
 *   A DeleteQuery object
 */
function freebil_recipient_delete($recip_id) {
  $delete_query_obj = db_delete('freebil_recipient')
    ->condition('recip_id', $recip_id)
    ->execute();
  return $delete_query_obj;
}

/**
 * Builds a form for creating a recipient.
 *
 * @param array $inv_data
 *   An associative array containing:
 *   - All data for the current invoice with column names as
 *     keys and the column values as values.
 *
 * @return array
 *   A form array
 */
function freebil_build_recipient_create_form(array $inv_data) {

  $form['cust_id'] = array(
    '#type' => 'hidden',
    '#value' => $inv_data['cust_id'],
  );
  $form['inv_id'] = array(
    '#type' => 'hidden',
    '#value' => $inv_data['inv_id'],
  );

  $form['form_heading'] = array(
    '#markup' => t('<h4>Recipients for Invoice Email<h4><h5>Invoice: @inv_name</h5><h5>Customer: @cust_name</h5>', array('@inv_name' => $inv_data['inv_name'], '@cust_name' => $inv_data['cust_name'])),
  );
  $form['form_instructions'] = array(
    '#markup' => t('If you don\'t see the correct email address here, go to the <a href="@cust">Customer Page</a> and add the correct email to the customer or a customer contact.',
                   array('@cust' => url('admin/freebil/customers/' . $inv_data['cust_id'] . '/view'))),
  );

  $email_for_cust_tbl = freebil_cust_and_contact_emails($inv_data['cust_id']);

  $form['recipient_table'] = array(
    '#type' => 'tableselect',
    '#header' => $email_for_cust_tbl['header'],
    '#options' => $email_for_cust_tbl['rows'],
    '#description' => t('Select recipient(s) for this invoice.'),
    '#empty' => t('No recipients found for this customer. Go to the <a href="@cust">Customer Page</a> and add an email address.',
                  array('@cust' => url('admin/freebil/customers/' . $inv_data['cust_id'] . '/view'))),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/freebil/invoices/' . $inv_data['inv_id'] . '/view'),
  );

  return $form;
}

/**
 * Builds a form for deleting a recipient.
 *
 * @param array $recip_data
 *   An associative array containing:
 *   - All data for the current recipient with column names
 *     as keys and the column values as values.
 *
 * @return array
 *   A form array
 */
function freebil_build_recipient_delete_form(array $recip_data) {

  $header = array(
    'recip_id' => t('ID'),
    'cust_name' => t('Name'),
    'recip_type' => t('Type'),
    'email_addr' => t('Email'),
    'email_type' => t('Email Type'),
    'created_ts' => t('Created'),
  );
  $rows[0] = array(
    'recip_id' => $recip_data['recip_id'],
    'cust_name' => $recip_data['cust_name'],
    'recip_type' => $recip_data['recip_type'],
    'email_addr' => $recip_data['email_addr'],
    'email_type' => $recip_data['email_type'],
    'created_ts' => $recip_data['created_ts'],
  );
  $form['identity'] = array(
    '#markup' => theme('table', array('header' => $header, 'rows' => $rows)),
  );
  $form['confirm'] = array(
    '#markup' => '<p>' . t('Are you sure you want to remove this recipient?') . '</p>',
  );

  $form['recip_id'] = array(
    '#type' => 'hidden',
    '#value' => $recip_data['recip_id'],
  );
  $form['inv_id'] = array(
    '#type' => 'hidden',
    '#value' => $recip_data['inv_id'],
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Remove'),
  );
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/freebil/invoices/' . $recip_data['inv_id'] . '/view'),
  );

  return $form;
}

/**
 * Get all recipient rows for this invoice.
 *
 * @param string $inv_id
 *   The invoice ID for the invoice to get associated recipients for.
 * @param bool $operations_yes
 *   TRUE to add a link to remove the recipient to each recipient row returned,
 *   FALSE to omit the link.
 *
 * @return array
 *   An associative array containing:
 *   - An associative array containing:
 *     - One row of Column Headings
 *   - An associative array containing:
 *     - Rows for each recipient associated with the
 *       input invoice ID, including a link to remove
 *       the recipient if $operations_yes is set to TRUE.
 */
function freebil_recipients_for_invoice_links($inv_id, $operations_yes = TRUE) {
  $rows = array();
  $count = 0;
  $operations = '';

  $result = freebil_recipients_for_invoice_query($inv_id);
  foreach ($result as $row) {
    if ($operations_yes) {
      if (user_access('administer freebil')) {
        $op_links['delete'] = array(
          'href' => 'admin/freebil/recipients/' . check_plain($row->recip_id) . '/remove',
          'title' => t('Remove'),
        );
        $operations = theme('links', array(
          'links' => $op_links,
          'attributes' => array('class' => array('links', 'inline')),
        ));
      }
    }

    $rows[$count] = array(
      'cust_name' => freebil_check_text($row->cust_name),
      'recip_type' => check_plain($row->recip_type),
      'email_addr' => check_plain($row->email_addr),
      'email_type' => check_plain($row->email_type),
      'created_ts' => check_plain(format_date($row->created_ts, 'short')),
    );
    if ($operations_yes) {
      $rows[$count]['operations'] = $operations;
    }
    $count++;
  }

  $header = array(
    'cust_name' => t('Name'),
    'recip_type' => t('Type'),
    'email_addr' => t('Email'),
    'email_type' => t('Email Type'),
    'created_ts' => t('Created'),
  );
  if ($operations_yes) {
    $header[] = t('Operations');
  }

  return array('header' => $header, 'rows' => $rows);
}

/**
 * Gets recipients for the invoice.
 *
 * @param string $inv_id
 *   The invoice ID for the invoice to get recipient data for.
 *
 * @return object
 *   A DatabaseStatementInterface prepared statement object,
 *   already executed.
 */
function freebil_recipients_for_invoice_query($inv_id) {
  $query = 'SELECT
   r.recip_id
  , r.inv_id
  , r.cust_id
  , c.cust_name
  , r.email_id
  , e.email_addr
  , e.email_type
  , r.recip_type
  , r.created_ts
   FROM
   {freebil_recipient} r
   INNER JOIN {freebil_customer} c
   ON c.cust_id = r.cust_id
   INNER JOIN {freebil_email} e
   ON e.email_id = r.email_id
   WHERE
   r.inv_id = :invid';
  return db_query($query, array(':invid' => $inv_id));
}

/**
 * Gets all recipient data for the input recipient ID.
 *
 * @param string $recip_id
 *   The recipient ID to get data for.
 *
 * @return array
 *   An associative array containing:
 *   - The column names as keys and the column
 *     values as values for the email database row.
 */
function freebil_get_recipient_data($recip_id) {
  $result = freebil_recipient_data_query($recip_id);
  $record = $result->fetchObject();
  $row = array(
    'recip_id' => check_plain($record->recip_id),
    'recip_type' => check_plain($record->recip_type),
    'inv_id' => check_plain($record->inv_id),
    'cust_id' => check_plain($record->cust_id),
    'cust_name' => freebil_check_text($record->cust_name),
    'email_id' => check_plain($record->email_id),
    'email_addr' => check_plain($record->email_addr),
    'email_type' => check_plain($record->email_type),
    'created_ts' => check_plain(format_date($record->created_ts, 'short')),
  );
  return $row;
}

/**
 * Builds and executes a query to get recipient data for the input recipient ID.
 *
 * @param string $recip_id
 *   The recipient ID to get data for.
 *
 * @return object
 *   A DatabaseStatementInterface prepared statement object, already executed.
 */
function freebil_recipient_data_query($recip_id) {
  $query = 'SELECT
   r.recip_id
  , r.inv_id
  , r.cust_id
  , c.cust_name
  , r.email_id
  , e.email_addr
  , e.email_type
  , r.recip_type
  , r.created_ts
   FROM
   {freebil_recipient} r
   INNER JOIN {freebil_customer} c
   ON c.cust_id = r.cust_id
   INNER JOIN {freebil_email} e
   ON e.email_id = r.email_id
   WHERE
   r.recip_id = :recipid';
  return db_query($query, array(':recipid' => $recip_id));
}

/**
 * Gets all recipient IDs related to an invoice.
 *
 * @param string $inv_id
 *   The invoice ID.
 *
 * @return array containing:
 *   - All recipient IDs related to the invoice ID
 */
function freebil_get_related_recip_ids($inv_id) {
  $query = 'SELECT
   recip_id
   FROM
   {freebil_recipient}
   WHERE
   inv_id = :invid';
  $result = db_query($query, array(':invid' => $inv_id));

  $recip_id_array = array();
  foreach ($result as $row) {
    $recip_id_array[] = $row->recip_id;
  }
  return $recip_id_array;
}
