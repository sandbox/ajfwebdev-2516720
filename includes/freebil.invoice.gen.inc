<?php
/**
 * @file
 * Generates a completed view of the invoice.
 *
 * The generated invoice view includes all the information that
 * will be delivered to the customer when the invoice is sent.
 * This allows the user to evaluate the completed invoice
 * before sending it to the customer.
 */

/**
 * Page Callback: Displays the completed invoice.
 *
 * @param string $inv
 *   The invoice ID for the invoice to be displayed.
 *
 * @return string
 *   Markup for the page to display the invoice info.
 *
 * @see freebil_menu()
 */
function freebil_invoice_generate($inv) {
  $inv_data = freebil_get_inv_data($inv);
  $cust_data = freebil_get_cust_data($inv_data['cust_id']);

  $crumbs = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('freebil'), 'admin/freebil'),
    l(t('Customers'), 'admin/freebil/customers'),
    l($inv_data['cust_name'], 'admin/freebil/customers/' . $inv_data['cust_id'] . '/view'),
    l($inv_data['inv_name'], 'admin/freebil/invoices/' . $inv_data['inv_id'] . '/view'),
  );
  drupal_set_breadcrumb($crumbs);

  $inv_hdr_tbl = freebil_invoice_view_header_data($inv_data);
  $invgen_item_hourly_tbl = freebil_invgen_item_hourly_data($inv);
  // Calling freebil_invoice_item_other_data() with TRUE makes it return items
  // with quantity and unit cost populated. FALSE excludes these items.
  $invgen_item_fixedrate_tbl = freebil_invgen_item_other_data($inv, 'Fixed Rate', FALSE);
  $invgen_item_fixedrate_qty_tbl = freebil_invgen_item_other_data($inv, 'Fixed Rate', TRUE);
  $invgen_item_product_tbl = freebil_invgen_item_product_data($inv);
  $invgen_item_expense_tbl = freebil_invgen_item_other_data($inv, 'Expense', FALSE);
  $invgen_item_expense_qty_tbl = freebil_invgen_item_other_data($inv, 'Expense', TRUE);
  $invgen_item_adjust_tbl = freebil_invgen_item_other_data($inv, 'Adjustment', FALSE);
  $invgen_item_adjust_qty_tbl = freebil_invgen_item_other_data($inv, 'Adjustment', TRUE);
  $invgen_item_tax_tbl = freebil_invgen_item_other_data($inv, 'Tax', FALSE);
  $invgen_item_tax_qty_tbl = freebil_invgen_item_other_data($inv, 'Tax', TRUE);
  $inv_totals_tbl = freebil_invoice_view_totals_data($inv_data);
  $invgen_payments_tbl = freebil_invgen_payments_list_data($inv);
  $inv_balance_tbl = freebil_invoice_view_balance_data($inv_data);
  $pay_due_date = freebil_calc_payment_due_date();
  $inv_recipient_tbl = freebil_recipients_for_invoice_links($inv, FALSE);

  return theme('freebil_invoice_generate',
    array(
      'inv_data' => $inv_data,
      'cust_data' => $cust_data,
      'pay_due_date' => $pay_due_date,
      'inv_header_tbl' => $inv_hdr_tbl,
      'item_hourly_tbl' => $invgen_item_hourly_tbl,
      'item_fixed_tbl' => $invgen_item_fixedrate_tbl,
      'item_fixed_qty_tbl' => $invgen_item_fixedrate_qty_tbl,
      'item_product_tbl' => $invgen_item_product_tbl,
      'item_expense_tbl' => $invgen_item_expense_tbl,
      'item_expense_qty_tbl' => $invgen_item_expense_qty_tbl,
      'item_adjust_tbl' => $invgen_item_adjust_tbl,
      'item_adjust_qty_tbl' => $invgen_item_adjust_qty_tbl,
      'item_tax_tbl' => $invgen_item_tax_tbl,
      'item_tax_qty_tbl' => $invgen_item_tax_qty_tbl,
      'inv_totals_tbl' => $inv_totals_tbl,
      'inv_payments_tbl' => $invgen_payments_tbl,
      'inv_balance_tbl' => $inv_balance_tbl,
      'inv_recipients_tbl' => $inv_recipient_tbl,
    )
  );
}

/**
 * Builds markup for rendering the completed invoice page.
 *
 * @param array $variables
 *   An associative array containing:
 *   - Associative arrays for each data set needed to
 *     build the various sections of the invoice page.
 *
 * @return string
 *   The markup for the invoice page.
 *
 * @ingroup themeable
 */
function theme_freebil_invoice_generate(array $variables) {
  $cust_name = $variables['inv_data']['cust_name'];
  $output = '';

  // Store the configured company address lines in a table.
  $co_addr_tbl = freebil_get_mycompany_addr_lines();

  // Store the customer address lines in a table.
  $cust_addr_tbl = freebil_get_customer_addr_lines($variables['cust_data']);

  // Header Section, My company name and address.
  $my_co_name = freebil_check_text(variable_get('freebil_company_name'));

  $output .= '<div id="freebil-invgen-coname-addr" class="freebil-invgen-heading">';
  if (empty($my_co_name)) {
    $output .= '<p>' . t('No company name found. Go to the Freebil configuration page to add the name of your company.') . '</p>';
  }
  else {
    $output .= '<h2>' . $my_co_name . '</h2>';
  }

  if (count($co_addr_tbl) < 1) {
    $output .= '<p>' . t('No company address found. Go to the Freebil configuration page and add your company address.') . '</p>';
  }
  else {
    $output .= '<p>';
    foreach ($co_addr_tbl as $addr_line) {
      if (!empty($addr_line)) {
        $output .= $addr_line . '<br>';
      }
    }
    $output .= '</p>';
  }

  $output .= '</div>';

  // Header Section, Invoice number, date, and pay by date.
  $output .= '<div id="freebil-invgen-invnum-dates" class="freebil-invgen-heading">';
  $output .= '<h2>' . t('Invoice #') . $variables['inv_data']['inv_id'] . '</h2>';
  $output .= '<p>' . '<strong>' . t('Invoice Date:') . ' </strong>' . $variables['pay_due_date']['today_date'] . '<br>';
  $output .= '<strong>' . t('Due Date:') . ' </strong>' . $variables['pay_due_date']['due_date'] . '<br>';
  $output .= '<strong>' . t('Invoice Currency:') . ' </strong>' . $variables['inv_data']['currency_code'] . '<br>';
  $output .= '</div>';

  // Header Section, Configured data info.
  $output .= '<div id="freebil-invgen-config" class="freebil-invgen-heading">';
  $output .= '<p>' . t("(The invoice date is today's date. The due date is based on the number of days to pay that is set in Freebil configuration. The name and address of your company are also set in Freebil configuration. Use the link below to set or change these values.)");
  // Freebil Configuration Link.
  $con_fre_links['config_fre'] = array(
    'href' => 'admin/config/system/freebil',
    'title' => t('Freebil Configuration'),
  );
  $output .= theme('links', array(
     'links' => $con_fre_links,
     'attributes' => array('class' => array('')),
  ));
  $output .= '</div>';

  // Header Section, Customer name and address.
  $output .= '<div id="freebil-invgen-invto" class="freebil-invgen-heading">';
  $output .= '<h4>' . t('Invoice To') . '</h4>';
  $output .= '<strong>' . $cust_name . '</strong><br>';
  foreach ($cust_addr_tbl as $addr_line) {
    if (!empty($addr_line)) {
      $output .= $addr_line . '<br>';
    }
  }
  $output .= '</p>';
  $output .= '</div>';

  // Invoice Items Section.
  $output .= '<div id="freebil-invgen-items" class="freebil-invgen-section">';
  if (!empty($variables['item_hourly_tbl']['rows'])) {
    $output .= '<h4>' . t('Hourly Charges') . '</h4>';
    $output .= theme('table', array('header' => $variables['item_hourly_tbl']['header'], 'rows' => $variables['item_hourly_tbl']['rows']));
  }
  if (!empty($variables['item_fixed_tbl']['rows'])) {
    $output .= '<h4>' . t('Fixed Rate Charges') . '</h4>';
    $output .= theme('table', array('header' => $variables['item_fixed_tbl']['header'], 'rows' => $variables['item_fixed_tbl']['rows']));
  }
  if (!empty($variables['item_fixed_qty_tbl']['rows'])) {
    $output .= theme('table', array('header' => $variables['item_fixed_qty_tbl']['header'], 'rows' => $variables['item_fixed_qty_tbl']['rows']));
  }
  if (!empty($variables['item_product_tbl']['rows'])) {
    $output .= '<h4>' . t('Charges for Products') . '</h4>';
    $output .= theme('table', array('header' => $variables['item_product_tbl']['header'], 'rows' => $variables['item_product_tbl']['rows']));
  }
  if (!empty($variables['item_expense_tbl']['rows'])) {
    $output .= '<h4>' . t('Expenses') . '</h4>';
    $output .= theme('table', array('header' => $variables['item_expense_tbl']['header'], 'rows' => $variables['item_expense_tbl']['rows']));
  }
  if (!empty($variables['item_expense_qty_tbl']['rows'])) {
    $output .= theme('table', array('header' => $variables['item_expense_qty_tbl']['header'], 'rows' => $variables['item_expense_qty_tbl']['rows']));
  }
  if (!empty($variables['item_adjust_tbl']['rows'])) {
    $output .= '<h4>' . t('Adjustments') . '</h4>';
    $output .= theme('table', array('header' => $variables['item_adjust_tbl']['header'], 'rows' => $variables['item_adjust_tbl']['rows']));
  }
  if (!empty($variables['item_adjust_qty_tbl']['rows'])) {
    $output .= theme('table', array('header' => $variables['item_adjust_qty_tbl']['header'], 'rows' => $variables['item_adjust_qty_tbl']['rows']));
  }
  if (!empty($variables['item_tax_tbl']['rows'])) {
    $output .= '<h4>' . t('Taxes') . '</h4>';
    $output .= theme('table', array('header' => $variables['item_tax_tbl']['header'], 'rows' => $variables['item_tax_tbl']['rows']));
  }
  if (!empty($variables['item_tax_qty_tbl']['rows'])) {
    $output .= theme('table', array('header' => $variables['item_tax_qty_tbl']['header'], 'rows' => $variables['item_tax_qty_tbl']['rows']));
  }

  $output .= '</div>';

  // Totals Section.
  $output .= '<div id="freebil-invgen-totals" class="freebil-invgen-section">';
  $output .= '<h3>' . t('Total Charges') . '</h3>';
  $output .= theme('table', array('header' => $variables['inv_totals_tbl']['header'], 'rows' => $variables['inv_totals_tbl']['rows']));
  $output .= '</div>';

  // Payments Section.
  if (!empty($variables['inv_payments_tbl']['rows'])) {
    $output .= '<div id="freebil-invgen-payments" class="freebil-invgen-section">';
    $output .= '<h3>' . t('Payments Received') . '</h3>';
    $output .= theme('table', array('header' => $variables['inv_payments_tbl']['header'], 'rows' => $variables['inv_payments_tbl']['rows']));
    $output .= '</div>';
  }

  // Balance Section.
  $output .= '<div id="freebil-invgen-balance" class="freebil-invgen-section">';
  $output .= '<h3>' . t('Total Due') . '</h3>';
  $output .= theme('table', array('header' => $variables['inv_balance_tbl']['header'], 'rows' => $variables['inv_balance_tbl']['rows']));
  $output .= '</div>';

  // Payment Options.
  $output .= '<div id="freebil-invgen-howpay" class="freebil-invgen-section">';
  $output .= '<h3>' . t('Payment Options') . '</h3>';
  $output .= '<p>' . freebil_check_text(variable_get('freebil_payment_instructions')) . '</p>';
  $output .= '</div>';

  // Payment Terms.
  $output .= '<div id="freebil-invgen-terms" class="freebil-invgen-section">';
  $output .= '<h3>' . t('Payment Terms') . '</h3>';
  $output .= '<p>' . freebil_check_text(variable_get('freebil_payment_terms')) . '</p>';
  $output .= '</div>';

  // Payment Options and Terms Section, Configured data.
  $output .= '<div id="freebil-invgen-config2" class="freebil-invgen-heading">';
  $output .= '<p>' . t("(The text for payment options and terms is set in Freebil configuration. Use the link below to set or change these values.)");
  // Freebil Configuration Link.
  $con_fre_links['config_fre'] = array(
    'href' => 'admin/config/system/freebil',
    'title' => t('Freebil Configuration'),
  );
  $output .= theme('links', array(
     'links' => $con_fre_links,
     'attributes' => array('class' => array('')),
  ));
  $output .= '</div>';

  // Invoice Email Recipients.
  $output .= '<div id="freebil-invgen-recipients" class="freebil-invgen-section">';
  $output .= '<h4>' . t('Invoice Email Recipients') . '</h4>';
  if (!empty($variables['inv_recipients_tbl']['rows'])) {
    $output .= theme('table', array('header' => $variables['inv_recipients_tbl']['header'], 'rows' => $variables['inv_recipients_tbl']['rows']));
  }
  else {
    $output .= '<p>' . t('No invoice email recipients chosen! Go back to the <a href="@actions">Invoice Actions page</a> to select email recipients.',
                  array('@actions' => url('admin/freebil/invoices/' . $variables['inv_data']['inv_id'] . '/view'))) . '</p>';
  }
  $output .= '</div>';

  // Back to invoice view link.
  $output .= '<div id="freebil-invgen-links" class="freebil-invgen-section">';
  $gen_links['email_inv_text'] = array(
    'href' => 'admin/freebil/invoices/' . $variables['inv_data']['inv_id'] . '/text/sendmail',
    'title' => t('Generate Plain Text Invoice Email'),
  );
  $gen_links['email_inv_html'] = array(
    'href' => 'admin/freebil/invoices/' . $variables['inv_data']['inv_id'] . '/html/sendmail',
    'title' => t('Generate HTML Invoice Email'),
  );
  $gen_links['back_inv'] = array(
    'href' => 'admin/freebil/invoices/' . $variables['inv_data']['inv_id'] . '/view',
    'title' => t('Back to Invoice Actions'),
  );
  $output .= theme('links', array(
     'links' => $gen_links,
     'attributes' => array('class' => array('')),
  ));
  $output .= '</div>';

  return $output;
}

/**
 * Gets the address lines for the user or the user's company.
 *
 * @return array
 *   a string for each address line.
 */
function freebil_get_mycompany_addr_lines() {
  $co_addr_tbl = array();
  for ($i = 1; $i < 5; $i++) {
    $line = freebil_check_text(variable_get('freebil_company_addr_line' . $i));
    if (!empty($line)) {
      $co_addr_tbl[] = $line;
    }
  }
  return $co_addr_tbl;
}

/**
 * Gets the address lines for the customer.
 *
 * @param array $cust_data
 *   An associative array containing:
 *   - All the data for the current customer.
 *
 * @return array
 *   a string for each address line.
 */
function freebil_get_customer_addr_lines(array $cust_data) {
  $cust_addr_tbl = array();
  for ($i = 1; $i < 5; $i++) {
    $line = $cust_data['addr_line' . $i];
    if (!empty($line)) {
      $cust_addr_tbl[] = $line;
    }
  }
  return $cust_addr_tbl;
}

/**
 * Calculates the payment due date for the current invoice.
 *
 * @return array
 *   An associative array containing:
 *   -  The payment due date and today's date.
 */
function freebil_calc_payment_due_date() {
  $current_time = REQUEST_TIME;
  $days_to_pay = check_plain(variable_get('freebil_days_due'));

  // Convert number of days to seconds.
  $seconds_to_pay = ($days_to_pay * 86400);
  $due_time = $current_time + $seconds_to_pay;

  $due_date = format_date($due_time, 'custom', 'Y-m-d');
  $today_date = format_date($current_time, 'custom', 'Y-m-d');

  return array('due_date' => $due_date, 'today_date' => $today_date);
}

/**
 * Gets all hourly items related to this invoice.
 *
 * @param string $inv_id
 *   The invoice ID for the invoice to get items for.
 *
 * @return array
 *   An associative array containing:
 *   - An associative array containing:
 *     - The headings for each data column.
 *   - An associative array containing:
 *     - Rows for each related hourly item.
 */
function freebil_invgen_item_hourly_data($inv_id) {
  $rows = array();
  $count = 0;

  $result = freebil_items_type_query($inv_id, 'Hourly');
  foreach ($result as $row) {

    $rows[$count] = array(
      'description' => freebil_check_text($row->description),
      'hours_worked' => check_plain($row->hours_worked),
      'hourly_rate' => check_plain($row->hourly_rate),
      'billed_amt' => check_plain($row->billed_amt),
    );
    $count++;
  }

  $row_header = array(
    'description' => t('Description'),
    'hours_worked' => t('Hours Worked'),
    'hourly_rate' => t('Hourly Rate'),
    'billed_amt' => t('Billed Amount'),
  );

  return array('header' => $row_header, 'rows' => $rows);

}

/**
 * Gets all product items related to this invoice.
 *
 * @param string $inv_id
 *   The invoice ID for the invoice to get items for.
 *
 * @return array
 *   An associative array containing:
 *   - An associative array containing:
 *     - The headings for each data column.
 *   - An associative array containing:
 *     - Rows for each related product item.
 */
function freebil_invgen_item_product_data($inv_id) {
  $rows = array();
  $count = 0;

  $result = freebil_items_type_query($inv_id, 'Product');
  foreach ($result as $row) {

    $rows[$count] = array(
      'description' => freebil_check_text($row->description),
      'quantity' => check_plain($row->quantity),
      'unit_cost_amt' => check_plain($row->unit_cost_amt),
      'subtotal_amt' => check_plain($row->subtotal_amt),
      'tax_percent' => check_plain($row->tax_percent),
      'tax_amt' => check_plain($row->tax_amt),
      'billed_amt' => check_plain($row->billed_amt),
    );
    $count++;
  }

  $row_header = array(
    'description' => t('Description'),
    'quantity' => t('Quantity'),
    'unit_cost_amt' => t('Unit Price'),
    'subtotal_amt' => t('Subtotal'),
    'tax_percent' => t('Tax Percent'),
    'tax_amt' => t('Tax Amount'),
    'billed_amt' => t('Billed Amount'),
  );

  return array('header' => $row_header, 'rows' => $rows);

}

/**
 * Gets items of types with description and billed amount.
 *
 * @param string $inv_id
 *   The invoice ID for the invoice to get items for.
 * @param string $type
 *   The item type to get (e.g. "Expense").
 * @param bool $quantity
 *   Whether to include quantity and unit cost in the results.
 *   Default is FALSE.
 *
 * @return array
 *   An associative array containing:
 *   - An associative array containing:
 *     - The headings for each data column.
 *   - An associative array containing:
 *     - Rows for each related item.
 */
function freebil_invgen_item_other_data($inv_id, $type, $quantity = FALSE) {
  $rows = array();
  $count = 0;

  $result = freebil_items_type_query($inv_id, $type, $quantity);
  foreach ($result as $row) {

    $rows[$count]['description'] = freebil_check_text($row->description);
    if ($quantity) {
      $rows[$count]['quantity'] = freebil_check_text($row->quantity);
      $rows[$count]['unit_cost_amt'] = freebil_check_text($row->unit_cost_amt);
    }
    $rows[$count]['billed_amt'] = freebil_check_text($row->billed_amt);
    $count++;
  }

  $row_header['description'] = t('Description');
  if ($quantity) {
    $row_header['quantity'] = t('Quantity');
    $row_header['unit_cost_amt'] = t('Unit Cost');
  }
  $row_header['billed_amt'] = t('Billed Amount');

  return array('header' => $row_header, 'rows' => $rows);

}

/**
 * Gets all payments received for this invoice.
 *
 * @param string $inv_id
 *   The invoice ID for the invoice to get related payments for.
 *
 * @return array
 *   An associative array containing:
 *   - An associative array containing:
 *     - The headings for each data column.
 *   - An associative array containing:
 *     - Rows for each related payment with payment data and ops links.
 */
function freebil_invgen_payments_list_data($inv_id) {
  $rows = array();
  $count = 0;

  $result = freebil_payments_for_invoice_query($inv_id);
  foreach ($result as $row) {
    $rows[$count] = array(
      'payment_amt' => check_plain($row->payment_amt),
      'currency_code' => check_plain($row->currency_code),
      'payment_date' => check_plain($row->payment_date),
    );
    $count++;
  }

  $row_header = array(
    'payment_amt' => t('Payment Amount'),
    'currency_code' => t('Payment Currency'),
    'payment_date' => t('Payment Date'),
  );

  return array('header' => $row_header, 'rows' => $rows);
}
