<?php

/**
 * @file
 * Manages invoice items.
 *
 * Invoices have invoice items, which represent charges for specific products,
 * hourly or fixed rate charges, expenses, taxes, and adjustments.
 *
 * Invoice items are accessed from the invoice page, which lists all
 * items for that invoice, and has a link for adding a new invoice item.
 * There are links for updating or deleting an item on each item row.
 */

/**
 * Page Callback: A form to create an invoice item.
 *
 * @param string $inv_id
 *   The invoice ID to create the item for.
 *
 * @return array
 *   A form array
 *
 * @see freebil_menu()
 * @see freebil_invoice_item_create_form()
 *
 * @ingroup forms
 */
function freebil_invoice_item_page_create($inv_id) {
  return drupal_get_form('freebil_invoice_item_create_form', $inv_id);
}

/**
 * Form constructor for the invoice create form.
 *
 * This is the wrapper function for a multi step form process. When creating
 * a new invoice item, we need to first determine which type of item is being
 * created in order to use the proper form.
 *
 * Uses code from
 * @link http://bryanjones.us/article/custom-multi-step-forms-drupal-7
 * Bryan Jones @endlink
 *
 * @param string $inv_id
 *   The invoice ID to create the item for.
 *
 * @return array
 *   A form array
 *
 * @see freebil_item_type_select_form()
 * @see freebil_invoice_item_create_form_submit()
 *
 * @ingroup forms
 */
function freebil_invoice_item_create_form($form, &$form_state, $inv_id) {
  // Check to see if anything has been stored.
  if ($form_state['rebuild']) {
    $form_state['values'] = array();
  }
  if (empty($form_state['storage'])) {
    // No step has been set so start with the first.
    $form_state['storage'] = array(
      'step' => 'freebil_item_type_select_form',
    );
  }

  // Add the invoice ID to form state storage.
  $form_state['storage']['inv_id'] = $inv_id;

  // Return the current form.
  $function = $form_state['storage']['step'];
  $form = $function($form, $form_state);
  return $form;

}

/**
 * Submit handler for our multi step wrapper form.
 *
 * Uses code from
 * @link http://bryanjones.us/article/custom-multi-step-forms-drupal-7
 * Bryan Jones @endlink.
 *
 * @see freebil_invoice_item_create_form()
 */
function freebil_invoice_item_create_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  if (isset($values['back']) && $values['op'] == $values['back']) {
    // Moving back in form.
    $step = $form_state['storage']['step'];
    // Call current step submit handler if it exists to unset step form data.
    if (function_exists($step . '_submit')) {
      $function = $step . '_submit';
      $function($form, $form_state);
    }
    // Remove the last saved step so we use it next.
    $last_step = array_pop($form_state['storage']['steps']);
    $form_state['storage']['step'] = $last_step;
  }
  else {
    // Record step.
    $step = $form_state['storage']['step'];
    $form_state['storage']['steps'][] = $step;
    // Call step submit handler if it exists.
    if (function_exists($step . '_submit')) {
      $function = $step . '_submit';
      $function($form, $form_state);
    }
  }
}

/**
 * Form constructor for the item type select form.
 *
 * @return array
 *   A form array
 *
 * @see freebil_invoice_item_create_form()
 *
 * @ingroup forms
 */
function freebil_item_type_select_form($form, &$form_state) {
  $inv_id = $form_state['storage']['inv_id'];
  $inv_data = freebil_get_inv_data($inv_id);

  // Set the page breadcrumb.
  $crumbs = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('freebil'), 'admin/freebil'),
    l(t('Customers'), 'admin/freebil/customers'),
    l($inv_data['cust_name'], 'admin/freebil/customers/' . $inv_data['cust_id'] . '/view'),
    l($inv_data['inv_name'], 'admin/freebil/invoices/' . $inv_data['inv_id'] . '/view'),
  );
  drupal_set_breadcrumb($crumbs);

  $values = '';
  if (!empty($form_state['storage'])) {
    $values = $form_state['storage'];
  }

  $form['item_type'] = array(
    '#type' => 'radios',
    '#title' => t('Item Type'),
    '#options' => drupal_map_assoc(
      array(
        'Hourly',
        'Fixed Rate',
        'Expense',
        'Adjustment',
        'Product',
        'Tax',
      )
    ),
    '#default_value' => isset($values['item_type']) ? $values['item_type'] : 'Hourly',
    '#description' => t('Select the type of invoice item you want to create.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create Item'),
  );
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/freebil/invoices/' . $inv_id . '/view'),
  );

  return $form;
}

/**
 * Submit handler for the item type select form.
 *
 * Uses code from
 * @link http://bryanjones.us/article/custom-multi-step-forms-drupal-7
 * Bryan Jones @endlink.
 *
 * @see freebil_item_type_select_form()
 */
function freebil_item_type_select_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $item_type = $values['item_type'];

  // Save values to the form storage, add the next step function callback.
  $form_state['rebuild'] = TRUE;
  $form_state['storage']['item_type'] = $item_type;

  if ($item_type == 'Hourly') {
    $form_state['storage']['step'] = 'freebil_item_create_hourly_form';
  }
  elseif ($item_type == 'Product') {
    $form_state['storage']['step'] = 'freebil_item_create_product_form';
  }
  else {
    $form_state['storage']['step'] = 'freebil_item_create_other_form';
  }

}

/**
 * Form constructor to create an invoice item with only one amount field.
 *
 * @return array
 *   A form array
 *
 * @see freebil_item_type_select_form_submit()
 * @see freebil_item_create_other_form_submit()
 *
 * @ingroup forms
 */
function freebil_item_create_other_form($form, &$form_state) {
  $inv_id = $form_state['storage']['inv_id'];
  $inv_data = freebil_get_inv_data($inv_id);

  // Set the page breadcrumb.
  $crumbs = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('freebil'), 'admin/freebil'),
    l(t('Customers'), 'admin/freebil/customers'),
    l($inv_data['cust_name'], 'admin/freebil/customers/' . $inv_data['cust_id'] . '/view'),
    l($inv_data['inv_name'], 'admin/freebil/invoices/' . $inv_data['inv_id'] . '/view'),
  );
  drupal_set_breadcrumb($crumbs);

  $item_data = array();
  $form = freebil_build_item_other_form("create", $form_state, $inv_id, $item_data);
  return $form;
}

/**
 * Submit handler for creating an invoice item with only one amount field.
 */
function freebil_item_create_other_form_submit($form, &$form_state) {

  $row = array(
    'inv_id' => $form_state['values']['inv_id'],
    'item_type' => $form_state['values']['item_type'],
    'hours_worked' => '0.00',
    'hourly_rate' => '0.00',
    'billed_amt' => $form_state['values']['billed_amt'],
    'quantity' => $form_state['values']['quantity'],
    'unit_cost_amt' => $form_state['values']['unit_cost_amt'],
    'subtotal_amt' => '0.00',
    'tax_percent' => '0',
    'tax_amt' => '0.00',
    'description' => $form_state['values']['description'],
  );
  // Insert the new invoice item.
  freebil_invoice_item_insert($row);

  // Recalculate invoice totals, and update the invoice row.
  freebil_recalc_invoice_totals_and_update($row['inv_id']);

  $form_state['redirect'] = 'admin/freebil/invoices/' . $form_state['values']['inv_id'] . '/view';
  drupal_set_message(t('Successfully saved Invoice Item.'));
}

/**
 * Form constructor to create a Product invoice item.
 *
 * @return array
 *   A form array
 *
 * @see freebil_item_type_select_form_submit()
 * @see freebil_item_create_product_form_validate()
 * @see freebil_item_create_product_form_submit()
 *
 * @ingroup forms
 */
function freebil_item_create_product_form($form, &$form_state) {
  $inv_id = $form_state['storage']['inv_id'];
  $inv_data = freebil_get_inv_data($inv_id);

  // Set the page breadcrumb.
  $crumbs = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('freebil'), 'admin/freebil'),
    l(t('Customers'), 'admin/freebil/customers'),
    l($inv_data['cust_name'], 'admin/freebil/customers/' . $inv_data['cust_id'] . '/view'),
    l($inv_data['inv_name'], 'admin/freebil/invoices/' . $inv_data['inv_id'] . '/view'),
  );
  drupal_set_breadcrumb($crumbs);

  $item_data = array();
  $form = freebil_build_item_product_form("create", $form_state, $inv_id, $item_data);
  return $form;
}

/**
 * Form validation handler for freebil_item_create_product_form().
 *
 * @see freebil_item_create_product_form_submit()
 */
function freebil_item_create_product_form_validate($form, &$form_state) {
  $in_value = check_plain($form_state['values']['tax_percent']);
  if (ctype_space($in_value) || $in_value == '') {
    form_set_error('tax_percent', t('Populate the tax percent.'));
  }
}
/**
 * Submit handler for creating a Product invoice item.
 */
function freebil_item_create_product_form_submit($form, &$form_state) {
  $tax_pct = check_plain($form_state['values']['tax_percent']);
  if (ctype_space($tax_pct) || $tax_pct == '') {
    $form_state['values']['tax_percent'] = '0';
  }
  $row = array(
    'inv_id' => $form_state['values']['inv_id'],
    'item_type' => $form_state['values']['item_type'],
    'hours_worked' => '0.00',
    'hourly_rate' => '0.00',
    'billed_amt' => '0.00',
    'quantity' => $form_state['values']['quantity'],
    'unit_cost_amt' => $form_state['values']['unit_cost_amt'],
    'subtotal_amt' => '0.00',
    'tax_percent' => $form_state['values']['tax_percent'],
    'tax_amt' => '0.00',
    'description' => $form_state['values']['description'],
  );
  // Insert the new invoice item.
  freebil_invoice_item_insert($row);

  // Recalculate invoice totals, and update the invoice row.
  freebil_recalc_invoice_totals_and_update($row['inv_id']);

  $form_state['redirect'] = 'admin/freebil/invoices/' . $form_state['values']['inv_id'] . '/view';
  drupal_set_message(t('Successfully saved Invoice Item.'));
}

/**
 * Form constructor to create an Hourly invoice item.
 *
 * @return array
 *   A form array
 *
 * @see freebil_item_type_select_form_submit()
 * @see freebil_item_create_hourly_form_validate()
 * @see freebil_item_create_hourly_form_submit()
 *
 * @ingroup forms
 */
function freebil_item_create_hourly_form($form, &$form_state) {
  $inv_id = $form_state['storage']['inv_id'];
  $inv_data = freebil_get_inv_data($inv_id);

  // Set the page breadcrumb.
  $crumbs = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('freebil'), 'admin/freebil'),
    l(t('Customers'), 'admin/freebil/customers'),
    l($inv_data['cust_name'], 'admin/freebil/customers/' . $inv_data['cust_id'] . '/view'),
    l($inv_data['inv_name'], 'admin/freebil/invoices/' . $inv_data['inv_id'] . '/view'),
  );
  drupal_set_breadcrumb($crumbs);

  $item_data = array();
  $form = freebil_build_item_hourly_form("create", $form_state, $inv_id, $item_data);
  return $form;
}

/**
 * Form validation handler for freebil_item_create_hourly_form().
 *
 * @see freebil_item_create_hourly_form_submit()
 */
function freebil_item_create_hourly_form_validate($form, &$form_state) {

  $hourly_rate = $form_state['values']['hourly_rate'];
  if (ctype_space($hourly_rate) || $hourly_rate == '') {
    form_set_error('hourly_rate', t('You must populate Hourly Rate.'));
  }
}

/**
 * Submit handler for creating an Hourly invoice item.
 */
function freebil_item_create_hourly_form_submit($form, &$form_state) {
  $hourly_rate = $form_state['values']['hourly_rate'];
  if (ctype_space($hourly_rate) || $hourly_rate == '') {
    $form_state['values']['hourly_rate'] = '0.00';
  }
  $row = array(
    'inv_id' => $form_state['values']['inv_id'],
    'item_type' => $form_state['values']['item_type'],
    'hours_worked' => $form_state['values']['hours_worked'],
    'hourly_rate' => $form_state['values']['hourly_rate'],
    'billed_amt' => '0.00',
    'quantity' => '0',
    'unit_cost_amt' => '0.00',
    'subtotal_amt' => '0.00',
    'tax_percent' => '0',
    'tax_amt' => '0.00',
    'description' => $form_state['values']['description'],
  );
  // Insert the new invoice item.
  freebil_invoice_item_insert($row);

  // Recalculate invoice totals, and update the invoice row.
  freebil_recalc_invoice_totals_and_update($row['inv_id']);

  $form_state['redirect'] = 'admin/freebil/invoices/' . $form_state['values']['inv_id'] . '/view';
  drupal_set_message(t('Successfully saved Invoice Item.'));
}

/**
 * Invoice Item insert API.
 *
 * @param array $row
 *   An associative array containing:
 *   - The column names as keys and the column values as values for
 *     the invoice row to be inserted.
 *
 * @return object
 *   - InsertQuery object
 */
function freebil_invoice_item_insert(array $row) {

  // Check invoice item amount fields and calculate amounts as needed.
  $row = freebil_item_calc_all($row);

  // Insert the invoice item row.
  $itm_id = db_insert('freebil_item')
    ->fields(array(
       'inv_id' => $row['inv_id'],
       'item_type' => $row['item_type'],
       'hours_worked' => $row['hours_worked'],
       'hourly_rate' => $row['hourly_rate'],
       'billed_amt' => $row['billed_amt'],
       'quantity' => $row['quantity'],
       'unit_cost_amt' => $row['unit_cost_amt'],
       'subtotal_amt' => $row['subtotal_amt'],
       'tax_percent' => $row['tax_percent'],
       'tax_amt' => $row['tax_amt'],
       'description' => $row['description'],
       'created_ts' => REQUEST_TIME,
       'last_update_ts' => REQUEST_TIME,
    ))->execute();

  return $itm_id;
}

/**
 * Page Callback: A form to edit an invoice item.
 *
 * @param string $itm_id
 *   The item ID for the item to be edited.
 *
 * @return array
 *   A form array
 *
 * @see freebil_menu()
 */
function freebil_invoice_item_page_edit($itm_id) {
  return drupal_get_form('freebil_invoice_item_edit_form', $itm_id);
}

/**
 * Form constructor for the invoice item edit form.
 *
 * @param string $itm_id
 *   The item ID for the item to edit.
 *
 * @return array
 *   A form array
 *
 * @see freebil_invoice_item_edit_form_submit()
 *
 * @ingroup forms
 */
function freebil_invoice_item_edit_form($form, &$form_state, $itm_id) {

  $item_data = freebil_get_item_data($itm_id);

  $inv_data = freebil_get_inv_data($item_data['inv_id']);

  // Set the page breadcrumb.
  $crumbs = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('freebil'), 'admin/freebil'),
    l(t('Customers'), 'admin/freebil/customers'),
    l($inv_data['cust_name'], 'admin/freebil/customers/' . $inv_data['cust_id'] . '/view'),
    l($inv_data['inv_name'], 'admin/freebil/invoices/' . $inv_data['inv_id'] . '/view'),
  );
  drupal_set_breadcrumb($crumbs);

  $item_type = $item_data['item_type'];
  if ($item_type == 'Hourly') {
    $form = freebil_build_item_hourly_form("edit", $form_state, $item_data['inv_id'], $item_data);
  }
  elseif ($item_type == 'Product') {
    $form = freebil_build_item_product_form("edit", $form_state, $item_data['inv_id'], $item_data);
  }
  else {
    $form = freebil_build_item_other_form("edit", $form_state, $item_data['inv_id'], $item_data);
  }

  return $form;
}

/**
 * Form submission handler for freebil_invoice_item_edit_form().
 */
function freebil_invoice_item_edit_form_submit($form, &$form_state) {
  $item_type = $form_state['values']['item_type'];
  if ($item_type == 'Hourly') {
    $row = array(
      'item_type' => $form_state['values']['item_type'],
      'hours_worked' => $form_state['values']['hours_worked'],
      'hourly_rate' => $form_state['values']['hourly_rate'],
      'billed_amt' => '0.00',
      'quantity' => '0',
      'unit_cost_amt' => '0.00',
      'subtotal_amt' => '0.00',
      'tax_percent' => '0',
      'tax_amt' => '0.00',
      'description' => $form_state['values']['description'],
    );
  }
  elseif ($item_type == 'Product') {
    $tax_pct = check_plain($form_state['values']['tax_percent']);
    if (ctype_space($tax_pct) || $tax_pct == '') {
      $form_state['values']['tax_percent'] = '0';
    }
    $row = array(
      'item_type' => $form_state['values']['item_type'],
      'hours_worked' => '0',
      'hourly_rate' => '0.00',
      'billed_amt' => '0.00',
      'quantity' => $form_state['values']['quantity'],
      'unit_cost_amt' => $form_state['values']['unit_cost_amt'],
      'subtotal_amt' => '0.00',
      'tax_percent' => $form_state['values']['tax_percent'],
      'tax_amt' => '0.00',
      'description' => $form_state['values']['description'],
    );
  }
  else {
    $row = array(
      'item_type' => $form_state['values']['item_type'],
      'hours_worked' => '0',
      'hourly_rate' => '0.00',
      'billed_amt' => $form_state['values']['billed_amt'],
      'quantity' => $form_state['values']['quantity'],
      'unit_cost_amt' => $form_state['values']['unit_cost_amt'],
      'subtotal_amt' => '0.00',
      'tax_percent' => '0',
      'tax_amt' => '0.00',
      'description' => $form_state['values']['description'],
    );
  }

  // Update this item row with data from the submitted form.
  freebil_invoice_item_update($row, $form_state['values']['itm_id']);

  // Recalculate invoice totals, and update the invoice row.
  freebil_recalc_invoice_totals_and_update($form_state['values']['inv_id']);

  $form_state['redirect'] = 'admin/freebil/invoices/' . $form_state['values']['inv_id'] . '/view';
  drupal_set_message(t('Successfully updated Invoice Item.'));
}

/**
 * Invoice Item update API.
 *
 * @param array $row
 *   An associative array containing:
 *   - The column names as keys and the column values as values for
 *     the item row to be updated.
 * @param string $itm_id
 *   The item ID for the item to be updated.
 *
 * @return object
 *   An UpdateQuery object
 */
function freebil_invoice_item_update(array $row, $itm_id) {

  // Check invoice item amount fields and calculate amounts as needed.
  $row = freebil_item_calc_all($row);

  $update_query_obj = db_update('freebil_item')
    ->fields(array(
       'item_type' => $row['item_type'],
       'hours_worked' => $row['hours_worked'],
       'hourly_rate' => $row['hourly_rate'],
       'billed_amt' => $row['billed_amt'],
       'quantity' => $row['quantity'],
       'unit_cost_amt' => $row['unit_cost_amt'],
       'subtotal_amt' => $row['subtotal_amt'],
       'tax_percent' => $row['tax_percent'],
       'tax_amt' => $row['tax_amt'],
       'description' => $row['description'],
       'last_update_ts' => REQUEST_TIME,
  ))
    ->condition('itm_id', $itm_id, '=')
    ->execute();

  return $update_query_obj;
}

/**
 * Sets default numeric form values to zero if they do not apply to the item.
 *
 * Also calculates all derived amount fields.
 *
 * @param array $row
 *   An associative array containing:
 *   - The values from the submitted form to be used in the update.
 *
 * @return array
 *   An associative array containing:
 *   - The modified values from the submitted form to be used in the update.
 *
 * @see freebil_invoice_item_update()
 */
function freebil_item_calc_all(array $row) {
  if ($row['item_type'] != 'Hourly') {
    $row['hourly_rate'] = '0.00';
  }
  if ($row['item_type'] != 'Product') {
    $row['tax_percent'] = '0';
  }

  // Calculate all derived fields.
  if ($row['item_type'] == 'Hourly') {
    $row['billed_amt'] = freebil_hourly_calc($row['hours_worked'], $row['hourly_rate']);
  }
  elseif ($row['item_type'] == 'Product') {
    $row['subtotal_amt'] = freebil_product_calc($row['quantity'], $row['unit_cost_amt']);
    $row['tax_amt'] = freebil_calc_tax($row['subtotal_amt'], $row['tax_percent']);
    $row['billed_amt'] = freebil_add_tax($row['subtotal_amt'], $row['tax_amt']);
  }
  else {
    // For "other" items there is the option of using quantity and unit cost.
    if ($row['unit_cost_amt'] != '0.00') {
      $row['billed_amt'] = freebil_product_calc($row['quantity'], $row['unit_cost_amt']);
    }
  }

  return $row;
}

/**
 * Page Callback: A form to delete an invoice item.
 *
 * @param string $itm_id
 *   The item ID for the item to be deleted.
 *
 * @return array
 *   A form array
 *
 * @see freebil_menu()
 */
function freebil_invoice_item_page_delete($itm_id) {
  return drupal_get_form('freebil_invoice_item_delete_form', $itm_id);
}

/**
 * Form constructor for the invoice item delete form.
 *
 * @param string $itm_id
 *   The item ID for the item to delete.
 *
 * @return array
 *   A form array
 *
 * @see freebil_invoice_item_delete_form_submit()
 *
 * @ingroup forms
 */
function freebil_invoice_item_delete_form($form, &$form_state, $itm_id) {

  $item_data = freebil_get_item_data($itm_id);

  $inv_data = freebil_get_inv_data($item_data['inv_id']);

  // Set the page breadcrumb.
  $crumbs = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('freebil'), 'admin/freebil'),
    l(t('Customers'), 'admin/freebil/customers'),
    l($inv_data['cust_name'], 'admin/freebil/customers/' . $inv_data['cust_id'] . '/view'),
    l($inv_data['inv_name'], 'admin/freebil/invoices/' . $inv_data['inv_id'] . '/view'),
  );
  drupal_set_breadcrumb($crumbs);

  $form = freebil_build_item_delete_form($form_state, $item_data);

  return $form;
}

/**
 * Form submission handler for freebil_invoice_item_delete_form().
 */
function freebil_invoice_item_delete_form_submit($form, &$form_state) {

  freebil_invoice_item_delete($form_state['values']['itm_id']);

  // Recalculate invoice totals, and update the invoice row.
  freebil_recalc_invoice_totals_and_update($form_state['values']['inv_id']);

  $form_state['redirect'] = 'admin/freebil/invoices/' . $form_state['values']['inv_id'] . '/view';
  drupal_set_message(t('Successfully deleted Item.'));
}

/**
 * Invoice Item delete API.
 *
 * @param string $itm_id
 *   The item ID for the item to be deleted.
 *
 * @return object
 *   A DeleteQuery object
 */
function freebil_invoice_item_delete($itm_id) {
  $delete_query_obj = db_delete('freebil_item')
    ->condition('itm_id', $itm_id)
    ->execute();
  return $delete_query_obj;
}

/**
 * Builds the header portion of invoice item forms.
 *
 * Called by functions that build the various kinds of invoice item forms,
 * because the header portion uses the same format on all of them.
 *
 * @param string $inv_id
 *   The invoice ID of the invoice this item belongs to.
 * @param array $item_data
 *   An associative array containing:
 *   - All data for the current item.
 *
 * @return array
 *   A form array
 */
function freebil_build_item_forms_header(&$form_state, $inv_id, $item_data = array()) {
  if (!empty($item_data['inv_id'])) {
    $inv_id = $item_data['inv_id'];
  }
  $form['inv_id'] = array(
    '#type' => 'hidden',
    '#value' => $inv_id,
  );

  if (!empty($item_data['item_type'])) {
    $item_type = $item_data['item_type'];
  }
  else {
    // On create the item type will only be in form state storage.
    $item_type = $form_state['storage']['item_type'];
  }
  $form['item_type'] = array(
    '#type' => 'hidden',
    '#value' => $item_type,
  );
  $cust_name = freebil_get_invoice_custname($inv_id);

  $form['customer_name'] = array(
    '#markup' => '<h4>' . t('Customer: @cust_name', array('@cust_name' => $cust_name)) . '</h4>',
  );
  $form['invoice_id'] = array(
    '#markup' => '<h4>' . t('Invoice ID: @inv_id', array('@inv_id' => $inv_id)) . '</h4>',
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Item Description'),
    '#maxlength' => 255,
    '#wysiwyg' => FALSE,
  );

  return $form;
}

/**
 * Builds a form for creating or editing items with one amount field.
 *
 * @param string $inv_id
 *   The invoice ID of the invoice this item belongs to.
 * @param array $item_data
 *   An associative array containing:
 *   - All data for the current item.
 *
 * @return array
 *   A form array
 */
function freebil_build_item_other_form($type, &$form_state, $inv_id, $item_data = array()) {

  $form = freebil_build_item_forms_header($form_state, $inv_id, $item_data);

  $form['adjustment_msg'] = array(
    '#markup' => '<p>' . t('For adjustments enter a negative number for a credit. Enter a positive number for a debit. Use the Billed Amount field.') . '</p>',
    '#prefix' => '<div style="display:none;">',
    '#suffix' => '</div>',
  );

  // Use states API to conditionaly display quantity and units fields.
  $form['entry_type'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display Quantity and Unit Cost fields.'),
    '#default_value' => FALSE,
  );
  $form['quantity'] = array(
    '#type' => 'textfield',
    '#title' => t('Quantity'),
    '#default_value' => '1',
    '#maxlength' => 9,
    '#element_validate' => array('element_validate_number'),
    '#description' => t('Enter the number of units sold.'),
    '#states' => array(
      'visible' => array(
        ':input[name="entry_type"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['unit_cost_amt'] = array(
    '#type' => 'textfield',
    '#title' => t('Unit Cost'),
    '#default_value' => '0.00',
    '#maxlength' => 12,
    '#element_validate' => array('element_validate_number'),
    '#description' => t('Enter the cost for one unit.'),
    '#states' => array(
      'visible' => array(
        ':input[name="entry_type"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['billed_amt'] = array(
    '#type' => 'textfield',
    '#title' => t('Billed Amount'),
    '#default_value' => '0.00',
    '#maxlength' => 12,
    '#element_validate' => array('element_validate_number'),
    '#description' => t('Enter the total amount billed for this item.'),
    '#states' => array(
      'visible' => array(
        ':input[name="entry_type"]' => array('checked' => FALSE),
      ),
    ),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/freebil/invoices/' . $inv_id . '/view'),
  );

  if ($type === "edit") {
    // Need the item ID to update the table row on form submit.
    $form['itm_id'] = array(
      '#type' => 'hidden',
      '#value' => $item_data['itm_id'],
    );
    // Using #default_value here. #value is not populated.
    $form['billed_amt']['#default_value'] = $item_data['billed_amt'];
    $form['quantity']['#default_value'] = $item_data['quantity'];
    $form['unit_cost_amt']['#default_value'] = $item_data['unit_cost_amt'];
    $form['description']['#default_value'] = $item_data['description'];
    if ($item_data['quantity'] > 1) {
      $form['entry_type']['#default_value'] = TRUE;
    }
  }

  if ($form['item_type']['#value'] == 'Adjustment') {
    $form['adjustment_msg']['#prefix'] = '';
    $form['adjustment_msg']['#suffix'] = '';
  }

  return $form;
}

/**
 * Builds a form for creating or editing product invoice items.
 *
 * @param string $inv_id
 *   The invoice ID of the invoice this item belongs to.
 * @param array $item_data
 *   An associative array containing:
 *   - All data for the current item.
 *
 * @return array
 *   A form array
 */
function freebil_build_item_product_form($type, &$form_state, $inv_id, $item_data = array()) {

  $form = freebil_build_item_forms_header($form_state, $inv_id, $item_data);

  $form['quantity'] = array(
    '#type' => 'textfield',
    '#title' => t('Quantity'),
    '#default_value' => '1',
    '#maxlength' => 9,
    '#element_validate' => array('element_validate_number'),
    '#description' => t('For product items enter the quantity sold.'),
  );
  $form['unit_cost_amt'] = array(
    '#type' => 'textfield',
    '#title' => t('Unit Cost'),
    '#default_value' => '0.00',
    '#maxlength' => 12,
    '#element_validate' => array('element_validate_number'),
    '#description' => t('Enter the cost for one unit.'),
  );

  $form['tax_section']['tax_percent'] = array(
    '#type' => 'textfield',
    '#title' => t('Tax Percent'),
    '#default_value' => variable_get('freebil_tax_percent'),
    '#maxlength' => 3,
    '#element_validate' => array('element_validate_number'),
    '#description' => t('If this item is taxed, enter the tax percentage rate as a number.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/freebil/invoices/' . $inv_id . '/view'),
  );

  if ($type === "edit") {
    // Need the item ID to update the table row on form submit.
    $form['itm_id'] = array(
      '#type' => 'hidden',
      '#value' => $item_data['itm_id'],
    );
    // Using #default_value here. #value is not populated.
    $form['quantity']['#default_value'] = $item_data['quantity'];
    $form['unit_cost_amt']['#default_value'] = $item_data['unit_cost_amt'];
    $form['subtotal_amt']['#default_value'] = $item_data['subtotal_amt'];
    $form['tax_section']['tax_percent']['#default_value'] = $item_data['tax_percent'];
    $form['tax_section']['tax_amt']['#default_value'] = $item_data['tax_amt'];
    $form['billed_amt']['#default_value'] = $item_data['billed_amt'];
    $form['description']['#default_value'] = $item_data['description'];
  }

  return $form;
}

/**
 * Builds a form for creating or editing hourly invoice items.
 *
 * @param string $inv_id
 *   The invoice ID of the invoice this item belongs to.
 * @param array $item_data
 *   An associative array containing:
 *   - All data for the current item.
 *
 * @return array
 *   A form array
 */
function freebil_build_item_hourly_form($type, &$form_state, $inv_id, $item_data = array()) {

  $form = freebil_build_item_forms_header($form_state, $inv_id, $item_data);

  $form['hours_worked'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of Hours Worked'),
    '#default_value' => '0.00',
    '#maxlength' => 9,
    '#element_validate' => array('element_validate_number'),
    '#description' => t('If this item is billed by the hour, enter the number of hours worked.'),
  );
  $form['hourly_rate'] = array(
    '#type' => 'textfield',
    '#title' => t('Hourly Rate'),
    '#default_value' => variable_get('freebil_hourly_rate'),
    '#maxlength' => 9,
    '#element_validate' => array('element_validate_number'),
    '#description' => t('If this item is billed by the hour, enter the amount per hour you are charging.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/freebil/invoices/' . $inv_id . '/view'),
  );

  if ($type === "edit") {
    // Need the item ID to update the table row on form submit.
    $form['itm_id'] = array(
      '#type' => 'hidden',
      '#value' => $item_data['itm_id'],
    );
    $form['hours_worked']['#default_value'] = $item_data['hours_worked'];
    $form['hourly_rate']['#default_value'] = $item_data['hourly_rate'];
    $form['description']['#default_value'] = $item_data['description'];
  }

  return $form;
}

/**
 * Builds a form for deleting invoice items.
 *
 * @param array $item_data
 *   An associative array containing:
 *   - All data for the current item.
 *
 * @return array
 *   A form array
 */
function freebil_build_item_delete_form(&$form_state, array $item_data) {

  // Need the invoice id for redirection after the delete.
  $form['inv_id'] = array(
    '#type' => 'hidden',
    '#value' => $item_data['inv_id'],
  );

  // Need the item ID to delete the row.
  $form['itm_id'] = array(
    '#type' => 'hidden',
    '#value' => $item_data['itm_id'],
  );
  $header = array(
    t('ID'),
    t('Type'),
    t('Description'),
    t('Billed Amount'),
    t('Created'),
    t('Last Updated'),
  );
  $rows[0] = array(
    'itm_id' => $item_data['itm_id'],
    'item_type' => $item_data['item_type'],
    'description' => $item_data['description'],
    'billed_amt' => $item_data['billed_amt'],
    'created_ts' => format_date($item_data['created_ts'], 'short'),
    'last_update_ts' => format_date($item_data['last_update_ts'], 'short'),
  );
  $form['identity'] = array(
    '#markup' => theme('table', array('header' => $header, 'rows' => $rows)),
  );
  $form['confirm'] = array(
    '#markup' => '<p>' . t('Are you sure you want to delete this item?') . '</p>',
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/freebil/invoices/' . $item_data['inv_id'] . '/view'),
  );
  return $form;

}

/**
 * Calculates billed amount for hourly items.
 *
 * @param string $num_hours
 *   The number of hours worked.
 * @param string $hourly_rate
 *   The cost per hour.
 *
 * @return string
 *   The billed amount as a decimal with two decimal places.
 */
function freebil_hourly_calc($num_hours, $hourly_rate) {
  $product = $hourly_rate * $num_hours;
  $billed_amt = number_format((float) $product, 2, '.', '');
  return $billed_amt;
}

/**
 * Calculates subtotal amount for product items.
 *
 * @param string $quantity
 *   The number of products purchased.
 * @param string $unit_cost
 *   The cost of one unit.
 *
 * @return string
 *   The subtotal amount as a decimal with two decimal places.
 */
function freebil_product_calc($quantity, $unit_cost) {
  $product = $unit_cost * $quantity;
  $subtotal_amt = number_format((float) $product, 2, '.', '');
  return $subtotal_amt;
}

/**
 * Calculates tax amount for product items.
 *
 * @param string $subtotal_amt
 *   The amount charged before tax.
 * @param string $tax_percent
 *   The tax percent to apply to the subtotal.
 *
 * @return string
 *   The tax amount as a decimal with two decimal places.
 */
function freebil_calc_tax($subtotal_amt, $tax_percent) {
  $tax_decimal = $tax_percent / 100;
  $product = $subtotal_amt * $tax_decimal;
  $tax_amount = number_format((float) $product, 2, '.', '');
  return $tax_amount;
}

/**
 * Calculates billed amount for product items.
 *
 * @param string $subtotal_amt
 *   The amount charged before tax.
 * @param string $tax_amount
 *   The tax amount charged.
 *
 * @return string
 *   The billed amount as a decimal with two decimal places.
 */
function freebil_add_tax($subtotal_amt, $tax_amount) {
  $sum = $subtotal_amt + $tax_amount;
  $billed_amt = number_format((float) $sum, 2, '.', '');
  return $billed_amt;
}

/**
 * Gets all editable item data for the input item ID.
 *
 * @param string $itm_id
 *   The item ID for the item to get data for.
 *
 * @return array
 *   An associative array containing:
 *   - All editable data values for the current item
 */
function freebil_get_item_data($itm_id) {
  $query = 'SELECT
   t.itm_id
  , t.inv_id
  , t.item_type
  , i.currency_code
  , t.hours_worked
  , t.hourly_rate
  , t.billed_amt
  , t.quantity
  , t.unit_cost_amt
  , t.subtotal_amt
  , t.tax_percent
  , t.tax_amt
  , t.description
  , t.created_ts
  , t.last_update_ts
   FROM
   {freebil_item} t
   INNER JOIN {freebil_invoice} i
   ON t.inv_id = i.inv_id
   WHERE
   itm_id = :itmid';
  $result = db_query($query, array(':itmid' => $itm_id));
  $record = $result->fetchObject();
  $output = array(
    'itm_id' => check_plain($record->itm_id),
    'inv_id' => check_plain($record->inv_id),
    'item_type' => check_plain($record->item_type),
    'currency_code' => check_plain($record->currency_code),
    'hours_worked' => check_plain($record->hours_worked),
    'hourly_rate' => check_plain($record->hourly_rate),
    'billed_amt' => check_plain($record->billed_amt),
    'quantity' => check_plain($record->quantity),
    'unit_cost_amt' => check_plain($record->unit_cost_amt),
    'subtotal_amt' => check_plain($record->subtotal_amt),
    'tax_percent' => check_plain($record->tax_percent),
    'tax_amt' => check_plain($record->tax_amt),
    'description' => freebil_check_text($record->description),
    'created_ts' => check_plain($record->created_ts),
    'last_update_ts' => check_plain($record->last_update_ts),
  );
  return $output;
}

/**
 * Gets all invoice items for the input invoice ID.
 *
 * @param string $inv_id
 *   The invoice ID for the invoice to get items for.
 *
 * @return object
 *   A DatabaseStatementInterface prepared statement object, already executed.
 */
function freebil_items_for_invoice_query($inv_id) {
  $query = 'SELECT
   t.itm_id
  , t.inv_id
  , t.item_type
  , i.currency_code
  , t.billed_amt
  , t.hours_worked
  , t.hourly_rate
  , t.unit_cost_amt
  , t.quantity
  , t.subtotal_amt
  , t.tax_amt
  , t.tax_percent
  , t.description
  , t.created_ts
  , t.last_update_ts
   FROM
   {freebil_item} t
   INNER JOIN {freebil_invoice} i
   ON t.inv_id = i.inv_id
   WHERE
   t.inv_id = :invid';
  $result = db_query($query, array(':invid' => $inv_id));

  return $result;
}

/**
 * Gets invoice items for the input invoice ID.
 *
 * Uses an input parameter determining which type of item to get.
 *
 * @param string $inv_id
 *   The invoice ID for the invoice to get items for.
 * @param string $type
 *   The type of item to get.
 * @param bool $quantity
 *   Whether to include items with quantity and unit cost in the results.
 *   Default is FALSE.
 *
 * @return object
 *   A DatabaseStatementInterface prepared statement object, already executed.
 */
function freebil_items_type_query($inv_id, $type, $quantity = FALSE) {
  $query = 'SELECT
   t.itm_id
  , t.inv_id
  , t.item_type
  , i.currency_code
  , t.billed_amt
  , t.hours_worked
  , t.hourly_rate
  , t.unit_cost_amt
  , t.quantity
  , t.subtotal_amt
  , t.tax_amt
  , t.tax_percent
  , t.description
  , t.created_ts
  , t.last_update_ts
   FROM
   {freebil_item} t
   INNER JOIN {freebil_invoice} i
   ON t.inv_id = i.inv_id
   WHERE
   t.inv_id = :invid
   AND t.item_type = :type';
  if (($type != 'Product') && ($type != 'Hourly')) {
    if ($quantity) {
      $query .= ' AND unit_cost_amt > 0.00';
    }
    else {
      $query .= ' AND unit_cost_amt = 0.00';
    }
  }
  $result = db_query($query, array(':invid' => $inv_id, ':type' => $type));
  return $result;
}

/**
 * Gets all item IDs related to an invoice.
 *
 * @param string $inv_id
 *   The invoice ID.
 *
 * @return array containing:
 *   - All item IDs related to the invoice ID
 */
function freebil_get_related_item_ids($inv_id) {
  $query = 'SELECT
   t.itm_id
   FROM
   {freebil_item} t
   WHERE
   t.inv_id = :invid';
  $result = db_query($query, array(':invid' => $inv_id));

  $itm_id_array = array();
  foreach ($result as $row) {
    $itm_id_array[] = $row->itm_id;
  }
  return $itm_id_array;
}
