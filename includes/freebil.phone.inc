<?php

/**
 * @file
 * Manages phone numbers.
 *
 * Customers and Contacts have phone numbers. The phone numbers for these
 * can be accessed from the Customer and Contact pages.
 */

/**
 * Page Callback: A form to create a Phone.
 *
 * @return array
 *   A form array
 *
 * @see freebil_menu()
 * @see freebil_phone_create_form()
 *
 * @ingroup forms
 */
function freebil_phone_page_create($cust, $sender) {
  return drupal_get_form('freebil_phone_create_form', $cust, $sender);
}

/**
 * Form constructor for the phone create form.
 *
 * @param string $cust_id
 *   The customer ID to create the phone for.
 * @param string $sender
 *   Indicates which page this form was called from.
 *   Values are: 'contacts' or 'customers'.
 *
 * @return array
 *   A form array
 *
 * @see freebil_phone_create_form_validate()
 * @see freebil_phone_create_form_submit()
 *
 * @ingroup forms
 */
function freebil_phone_create_form($form, &$form_state, $cust_id, $sender) {

  $cust_data = freebil_get_cust_data($cust_id);
  $parent_data = array();
  if ($sender == 'contacts') {
    $parent_data = freebil_get_cust_data($cust_data['parent_cust_id']);
  }
  $phone_data = array();

  // Set the page breadcrumb.
  $crumbs = array();
  $crumbs[] = l(t('Home'), '<front>');
  $crumbs[] = l(t('Administration'), 'admin');
  $crumbs[] = l(t('freebil'), 'admin/freebil');
  $crumbs[] = l(t('Customers'), 'admin/freebil/customers');
  if ($sender == 'contacts') {
    $crumbs[] = l($parent_data['cust_name'], 'admin/freebil/customers/' . $parent_data['cust_id'] . '/view');
    $crumbs[] = l($cust_data['cust_name'], 'admin/freebil/contacts/' . $cust_data['cust_id'] . '/view');
  }
  else {
    $crumbs[] = l($cust_data['cust_name'], 'admin/freebil/customers/' . $cust_data['cust_id'] . '/view');
  }
  drupal_set_breadcrumb($crumbs);

  return freebil_build_phone_form("create", $cust_data, $sender, $phone_data);
}

/**
 * Form validation handler for freebil_phone_create_form().
 *
 * @see freebil_phone_create_form_submit()
 */
function freebil_phone_create_form_validate($form, &$form_state) {
  $input_phone = check_plain($form_state['values']['phone_number']);
  if (ctype_space($input_phone) || $input_phone == '') {
    form_set_error('phone_number', t('Please input a phone number.'));
  }
}

/**
 * Form submission handler for freebil_phone_create_form().
 *
 * @see freebil_phone_create_form_validate()
 */
function freebil_phone_create_form_submit($form, &$form_state) {

  $row = array(
    'cust_id' => $form_state['values']['cust_id'],
    'phone_number' => $form_state['values']['phone_number'],
    'phone_type' => $form_state['values']['phone_type'],
    'description' => $form_state['values']['description'],
  );
  freebil_phone_insert($row);

  $form_state['redirect'] = 'admin/freebil/' . $form_state['values']['sender'] . '/' . $form_state['values']['cust_id'] . '/view';
  drupal_set_message(t('Successfully saved Phone.'));
}

/**
 * Phone insert API.
 *
 * @param array $row
 *   An associative array containing:
 *   - The column names as keys and the column values as values for
 *     the phone row to be inserted.
 *
 * @return object
 *   - InsertQuery object
 */
function freebil_phone_insert(array $row) {

  $phone_id = db_insert('freebil_phone')
    ->fields(array(
       'cust_id' => $row['cust_id'],
       'phone_number' => $row['phone_number'],
       'phone_type' => $row['phone_type'],
       'description' => $row['description'],
       'created_ts' => REQUEST_TIME,
       'last_update_ts' => REQUEST_TIME,
    ))->execute();

  return $phone_id;
}

/**
 * Page Callback: A form to edit a phone.
 *
 * @param string $phone
 *   The phone ID for the phone to be edited.
 * @param string $sender
 *   Indicates which page this form was called from.
 *   Values are: 'contacts' or 'customers'.
 *
 * @return array
 *   A form array
 *
 * @see freebil_menu()
 */
function freebil_phone_page_edit($phone, $sender) {
  return drupal_get_form('freebil_phone_edit_form', $phone, $sender);
}

/**
 * Form constructor for the phone edit form.
 *
 * @param string $phone
 *   The phone ID for the phone to edit.
 * @param string $sender
 *   Indicates which page this form was called from.
 *   Values are: 'contacts' or 'customers'.
 *
 * @return array
 *   A form array
 *
 * @see freebil_phone_edit_form_validate()
 * @see freebil_phone_edit_form_submit()
 *
 * @ingroup forms
 */
function freebil_phone_edit_form($form, &$form_state, $phone, $sender) {
  $phone_data = freebil_get_phone_data($phone);
  $cust_data = freebil_get_cust_data($phone_data['cust_id']);
  $parent_data = array();
  if ($sender == 'contacts') {
    $parent_data = freebil_get_cust_data($cust_data['parent_cust_id']);
  }

  // Set the page breadcrumb.
  $crumbs = array();
  $crumbs[] = l(t('Home'), '<front>');
  $crumbs[] = l(t('Administration'), 'admin');
  $crumbs[] = l(t('freebil'), 'admin/freebil');
  $crumbs[] = l(t('Customers'), 'admin/freebil/customers');
  if ($sender == 'contacts') {
    $crumbs[] = l($parent_data['cust_name'], 'admin/freebil/customers/' . $parent_data['cust_id'] . '/view');
    $crumbs[] = l($cust_data['cust_name'], 'admin/freebil/contacts/' . $cust_data['cust_id'] . '/view');
  }
  else {
    $crumbs[] = l($cust_data['cust_name'], 'admin/freebil/customers/' . $cust_data['cust_id'] . '/view');
  }
  drupal_set_breadcrumb($crumbs);

  $cust_data = array();
  $form = freebil_build_phone_form("edit", $cust_data, $sender, $phone_data);

  return $form;
}

/**
 * Form validation handler for freebil_phone_page_edit().
 *
 * @see freebil_phone_edit_form_submit()
 */
function freebil_phone_edit_form_validate($form, &$form_state) {
  $input_phone = check_plain($form_state['values']['phone_number']);
  if (ctype_space($input_phone) || $input_phone == '') {
    form_set_error('phone_number', t('Please input a phone number.'));
  }
}

/**
 * Form submission handler for freebil_phone_page_edit().
 *
 * @see freebil_phone_edit_form_validate()
 */
function freebil_phone_edit_form_submit($form, &$form_state) {

  $row = array(
    'cust_id' => $form_state['values']['cust_id'],
    'phone_number' => $form_state['values']['phone_number'],
    'phone_type' => $form_state['values']['phone_type'],
    'description' => $form_state['values']['description'],
  );

  // Update this phone row with data from the submitted form.
  freebil_phone_update($row, $form_state['values']['phone_id']);

  $form_state['redirect'] = 'admin/freebil/' . $form_state['values']['sender'] . '/' . $form_state['values']['cust_id'] . '/view';
  drupal_set_message(t('Successfully updated Phone.'));
}

/**
 * Phone update API.
 *
 * @param array $row
 *   An associative array containing:
 *   - The column names as keys and the column values as values for
 *     the phone row to be updated.
 * @param string $phone_id
 *   The phone ID for the phone to be updated.
 *
 * @return object
 *   An UpdateQuery object
 */
function freebil_phone_update(array $row, $phone_id) {
  $update_query_obj = db_update('freebil_phone')
    ->fields(array(
       'cust_id' => $row['cust_id'],
       'phone_number' => $row['phone_number'],
       'phone_type' => $row['phone_type'],
       'description' => $row['description'],
       'last_update_ts' => REQUEST_TIME,
  ))
    ->condition('phone_id', $phone_id, '=')
    ->execute();

  return $update_query_obj;
}

/**
 * Page Callback: A form to delete a phone.
 *
 * @param string $phone
 *   The phone ID for the phone to be deleted.
 * @param string $sender
 *   Indicates which page this form was called from.
 *   Values are: 'contacts' or 'customers'.
 *
 * @return array
 *   A form array
 *
 * @see freebil_menu()
 */
function freebil_phone_page_delete($phone, $sender) {
  return drupal_get_form('freebil_phone_delete_form', $phone, $sender);
}

/**
 * Form constructor for the phone delete form.
 *
 * @param string $phone
 *   The customer ID to delete.
 * @param string $sender
 *   Indicates which page this form was called from.
 *   Values are: 'contacts' or 'customers'.
 *
 * @return array
 *   A form array
 *
 * @see freebil_phone_delete_form_submit()
 *
 * @ingroup forms
 */
function freebil_phone_delete_form($form, &$form_state, $phone, $sender) {
  $phone_data = freebil_get_phone_data($phone);
  $cust_data = freebil_get_cust_data($phone_data['cust_id']);
  $parent_data = array();
  if ($sender == 'contacts') {
    $parent_data = freebil_get_cust_data($cust_data['parent_cust_id']);
  }

  // Set the page breadcrumb.
  $crumbs = array();
  $crumbs[] = l(t('Home'), '<front>');
  $crumbs[] = l(t('Administration'), 'admin');
  $crumbs[] = l(t('freebil'), 'admin/freebil');
  $crumbs[] = l(t('Customers'), 'admin/freebil/customers');
  if ($sender == 'contacts') {
    $crumbs[] = l($parent_data['cust_name'], 'admin/freebil/customers/' . $parent_data['cust_id'] . '/view');
    $crumbs[] = l($cust_data['cust_name'], 'admin/freebil/contacts/' . $cust_data['cust_id'] . '/view');
  }
  else {
    $crumbs[] = l($cust_data['cust_name'], 'admin/freebil/customers/' . $cust_data['cust_id'] . '/view');
  }
  drupal_set_breadcrumb($crumbs);

  $form = freebil_build_phone_delete_form($phone_data, $sender);

  return $form;
}

/**
 * Form submission handler for freebil_phone_delete_form().
 */
function freebil_phone_delete_form_submit($form, &$form_state) {
  freebil_phone_delete($form_state['values']['phone_id']);

  $form_state['redirect'] = 'admin/freebil/' . $form_state['values']['sender'] . '/' . $form_state['values']['cust_id'] . '/view';
  drupal_set_message(t('Successfully deleted Phone.'));
}

/**
 * Phone delete API.
 *
 * @param string $phone_id
 *   The phone ID for the phone to be deleted.
 *
 * @return object
 *   A DeleteQuery object.
 */
function freebil_phone_delete($phone_id) {
  $delete_query_obj = db_delete('freebil_phone')
    ->condition('phone_id', $phone_id)
    ->execute();
  return $delete_query_obj;
}

/**
 * Builds forms for creating or editing phones.
 *
 * @param string $type
 *   Indicates the type of form to build: "create" or "edit".
 * @param array $cust_data
 *   An associative array containing:
 *   - All data for the parent customer (or contact) with column names
 *     as keys and the column values as values.
 * @param string $sender
 *   Indicates which page this form was called from.
 *   Values are: 'contacts' or 'customers'.
 * @param array $phone_data
 *   Optional - only used if $type is "edit"
 *   An associative array containing:
 *   - All data for an existing phone with column names as keys
 *     and the column values as values.
 *
 * @return array
 *   A form array
 */
function freebil_build_phone_form($type, array $cust_data, $sender, $phone_data = array()) {

  $form['sender'] = array(
    '#type' => 'hidden',
    '#value' => $sender,
  );

  if ($type === "edit") {
    $form['phone_id_display'] = array(
      '#markup' => '<h5>Phone ID: ' . $phone_data['phone_id'] . '</h5>',
    );
  }

  $cust_name = '';
  if (array_key_exists("cust_name", $phone_data)) {
    $cust_name = $phone_data['cust_name'];
  }
  elseif (array_key_exists("cust_name", $cust_data)) {
    $cust_name = $cust_data['cust_name'];
  }

  if ($sender == 'customers') {
    $sender_display = 'Customer';
  }
  else {
    $sender_display = 'Contact';
  }
  $form['customer_name_display'] = array(
    '#markup' => '<h5>' . $sender_display . ': ' . $cust_name . '</h5>',
  );

  $cust_id = '';
  if (array_key_exists("cust_id", $phone_data)) {
    $cust_id = $phone_data['cust_id'];
  }
  elseif (array_key_exists("cust_id", $cust_data)) {
    $cust_id = $cust_data['cust_id'];
  }
  $form['cust_id'] = array(
    '#type' => 'hidden',
    '#value' => $cust_id,
  );

  $form['phone_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Phone Number'),
    '#maxlength' => 25,
    '#wysiwyg' => FALSE,
    '#description' => t('Add the full phone number here.'),
  );
  $form['phone_type'] = array(
    '#type' => 'select',
    '#title' => t('Phone Type'),
    '#options' => array(
      'Work' => t('Work'),
      'Cell' => t('Cell'),
      'Home' => t('Home'),
    ),
    '#default_value' => 'Work',
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Phone Description'),
    '#maxlength' => 255,
    '#wysiwyg' => FALSE,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  if ($type === "create") {
    $form['actions']['cancel'] = array(
      '#markup' => l(t('Cancel'), 'admin/freebil/' . $sender . '/' . $cust_id . '/view'),
    );
  }
  else {
    $form['actions']['cancel'] = array(
      '#markup' => l(t('Cancel'), 'admin/freebil/' . $sender . '/' . $cust_id . '/view'),
    );
  }

  // If this is the edit form, populate the values from the phone table.
  if ($type === "edit") {
    // Need the phone ID to update the table row on form submit.
    $form['phone_id'] = array(
      '#type' => 'hidden',
      '#value' => $phone_data['phone_id'],
    );
    $form['phone_number']['#default_value'] = $phone_data['phone_number'];
    $form['phone_type']['#default_value'] = $phone_data['phone_type'];
    $form['description']['#default_value'] = $phone_data['description'];
  }

  return $form;
}

/**
 * Builds a form for deleting a phone.
 *
 * @param array $phone_data
 *   An associative array containing:
 *   - All data for an existing phone with column names as
 *     keys and the column values as values.
 * @param string $sender
 *   Indicates which page this form was called from.
 *   Values are: 'contacts' or 'customers'.
 *
 * @return array
 *   A form array
 */
function freebil_build_phone_delete_form(array $phone_data, $sender) {
  $form['sender'] = array(
    '#type' => 'hidden',
    '#value' => $sender,
  );

  $header = array(
    t('ID'),
    t('Customer Name'),
    t('Phone'),
    t('Type'),
    t('Description'),
    t('Created'),
    t('Last Updated'),
  );
  $rows[0] = array(
    'phone_id' => $phone_data['phone_id'],
    'cust_name' => $phone_data['cust_name'],
    'phone_number' => $phone_data['phone_number'],
    'phone_type' => $phone_data['phone_type'],
    'description' => $phone_data['description'],
    'created_ts' => $phone_data['created_ts'],
    'last_update_ts' => $phone_data['last_update_ts'],
  );
  $form['identity'] = array(
    '#markup' => theme('table', array('header' => $header, 'rows' => $rows)),
  );
  $form['confirm'] = array(
    '#markup' => '<p>' . t('Are you sure you want to delete this phone number?') . '</p>',
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/freebil/' . $sender . '/' . $phone_data['cust_id'] . '/view'),
  );
  // Need the phone ID to delete the table row on form submit.
  $form['phone_id'] = array(
    '#type' => 'hidden',
    '#value' => $phone_data['phone_id'],
  );
  // Need cust ID to redirect back to customer view page after delete.
  $form['cust_id'] = array(
    '#type' => 'hidden',
    '#value' => $phone_data['cust_id'],
  );

  return $form;
}

/**
 * Gets all phone rows for the input customer ID.
 *
 * The returned phone rows include links to edit or delete the phone.
 *
 * @param string $cust_id
 *   The customer ID to get associated phones for.
 * @param string $sender
 *   Indicates which page this form was called from.
 *   Values are: 'contacts' or 'customers'.
 *
 * @return array
 *   An associative array containing:
 *   - An associative array containing:
 *     - The headings for each data column.
 *   - An associative array containing:
 *     - rows containing the phone data for each column for
 *       each associated phone.
 */
function freebil_phones_for_cust_links($cust_id, $sender) {
  $rows = array();
  $count = 0;
  $operations = '';

  $result = freebil_phones_for_cust_query($cust_id);
  foreach ($result as $row) {
    if (user_access('administer freebil')) {
      $op_links['edit'] = array(
        'href' => 'admin/freebil/phone/' . check_plain($row->phone_id) . '/' . $sender . '/edit',
        'title' => t('Edit'),
      );
      $op_links['delete'] = array(
        'href' => 'admin/freebil/phone/' . check_plain($row->phone_id) . '/' . $sender . '/delete',
        'title' => t('Delete'),
      );
      $operations = theme('links', array(
        'links' => $op_links,
        'attributes' => array('class' => array('links', 'inline')),
      ));
    }

    $rows[$count] = array(
      'phone_number' => check_plain($row->phone_number),
      'phone_type' => check_plain($row->phone_type),
      'description' => freebil_check_text($row->description),
      'created_ts' => check_plain(format_date($row->created_ts, 'short')),
      'last_update_ts' => check_plain(format_date($row->last_update_ts, 'short')),
      'operations' => $operations,
    );
    $count++;
  }

  $header = array(
    'phone_number' => t('Phone Number'),
    'phone_type' => t('Type'),
    'description' => t('Description'),
    'created_ts' => t('Created'),
    'last_update_ts' => t('Last Updated'),
    'operations' => t('Operations'),
  );

  return array('header' => $header, 'rows' => $rows);
}

/**
 * Builds and executes a query to get phones for the input customer ID.
 *
 * @param string $cust_id
 *   The customer ID to get phones for.
 *
 * @return object
 *   A DatabaseStatementInterface prepared statement object, already executed.
 */
function freebil_phones_for_cust_query($cust_id) {
  $query = 'SELECT
   phone_id
  , cust_id
  , phone_number
  , phone_type
  , description
  , created_ts
  , last_update_ts
   FROM
   {freebil_phone}
   WHERE
   cust_id = :custid';
  return db_query($query, array(':custid' => $cust_id));
}

/**
 * Gets all phone data for the input phone ID.
 *
 * @param string $phone_id
 *   The phone ID to get data for.
 *
 * @return array
 *   An associative array containing:
 *   - The column names as keys and the column values
 *     as values for the phone database row.
 */
function freebil_get_phone_data($phone_id) {
  $result = freebil_phone_query($phone_id);
  $record = $result->fetchObject();
  $row = array(
    'phone_id' => check_plain($record->phone_id),
    'cust_id' => check_plain($record->cust_id),
    'cust_name' => freebil_check_text($record->cust_name),
    'phone_number' => check_plain($record->phone_number),
    'phone_type' => check_plain($record->phone_type),
    'description' => freebil_check_text($record->description),
    'created_ts' => check_plain(format_date($record->created_ts, 'short')),
    'last_update_ts' => check_plain(format_date($record->last_update_ts, 'short')),
  );

  return $row;
}

/**
 * Builds and executes a query to get phone data for the input phone ID.
 *
 * @param string $phone_id
 *   The phone ID to get data for.
 *
 * @return object
 *   A DatabaseStatementInterface prepared statement object, already executed.
 */
function freebil_phone_query($phone_id) {
  $query = 'SELECT
   e.phone_id
  , e.cust_id
  , c.cust_name
  , e.phone_number
  , e.phone_type
  , e.description
  , e.created_ts
  , e.last_update_ts
   FROM
   {freebil_phone} e
   INNER JOIN {freebil_customer} c
   ON c.cust_id = e.cust_id
   WHERE
   phone_id = :phoneid';
  return db_query($query, array(':phoneid' => $phone_id));
}

/**
 * Gets the phones related to a customer.
 *
 * @param string $cust_id
 *   The customer ID.
 *
 * @return array containing:
 *   - All phone IDs related to the customer ID.
 */
function freebil_get_related_phone_ids($cust_id) {
  $query = 'SELECT
   phone_id
   FROM
   {freebil_phone}
   WHERE
   cust_id = :custid';
  $result = db_query($query, array(':custid' => $cust_id));

  $ids = array();
  foreach ($result as $row) {
    $ids[] = $row->phone_id;
  }
  return $ids;
}
