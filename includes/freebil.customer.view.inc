<?php
/**
 * @file
 * Renders the customer page.
 *
 * The customer page displays information for an individual customer
 * and includes options for the user to manage customer data and
 * customer contacts.
 *
 * The customer's invoices are accessed by clicking a tab which links to the
 * individual invoice page.
 */

/**
 * Page Callback: Displays the customer data with options to make changes.
 *
 * @param string $cust_id
 *   The customer ID for the customer to be displayed.
 *
 * @return string
 *   Markup for the page to display the customer info.
 *
 * @see freebil_menu()
 */
function freebil_customer_view_page($cust_id) {
  // Set the page breadcrumb.
  $crumbs = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('freebil'), 'admin/freebil'),
    l(t('Customers'), 'admin/freebil/customers'),
  );
  drupal_set_breadcrumb($crumbs);

  $cust_data = freebil_get_cust_data($cust_id);
  $cust_hdr_tbl = freebil_customer_view_header_data($cust_data);
  $cust_addr_tbl = freebil_customer_view_address_data($cust_data);
  $cust_contact_tbl = freebil_contact_list_for_cust($cust_id);
  $cust_email_tbl = freebil_emails_for_cust_links($cust_id, 'customers');
  $cust_phone_tbl = freebil_phones_for_cust_links($cust_id, 'customers');

  drupal_set_title($cust_data['cust_name']);

  return theme('freebil_customer_view_page',
        array(
          'cust_data' => $cust_data,
          'cust_hdr_tbl' => $cust_hdr_tbl,
          'cust_phone_tbl' => $cust_phone_tbl,
          'cust_email_tbl' => $cust_email_tbl,
          'cust_addr_tbl' => $cust_addr_tbl,
          'cust_contact_tbl' => $cust_contact_tbl,
        ));
}

/**
 * Page Callback: Displays a list of invoices belonging to the customer.
 *
 * Includes option to create a new invoice.
 *
 * @param string $cust_id
 *   The customer ID of the customer to list invoices for.
 *
 * @return string
 *   Markup for the page to display the invoice list.
 *
 * @see freebil_menu()
 */
function freebil_customer_invoices_page($cust_id) {
  $cust_data = freebil_get_cust_data($cust_id);
  drupal_set_title($cust_data['cust_name'] . ' ' . t('Invoices'));

  // Set the page breadcrumb.
  $crumbs = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('freebil'), 'admin/freebil'),
    l(t('Customers'), 'admin/freebil/customers'),
    l($cust_data['cust_name'], 'admin/freebil/customers/' . $cust_data['cust_id'] . '/view'),
  );
  drupal_set_breadcrumb($crumbs);

  $cust_invoice_tbl = freebil_invoice_list_for_cust($cust_id);

  return theme('freebil_customer_invoices_page',
        array(
          'cust_data' => $cust_data,
          'cust_invoice_tbl' => $cust_invoice_tbl,
        ));
}

/**
 * Builds markup for rendering the customer page.
 *
 * @param array $variables
 *   An associative array containing:
 *   - associative arrays for each data set needed to build the various
 *     sections of the customer page.
 *
 * @return string
 *   The markup for the customer page.
 *
 * @ingroup themeable
 */
function theme_freebil_customer_view_page(array $variables) {
  $output = '';
  $cust_name = $variables['cust_data']['cust_name'];

  // Customer data section.
  $output .= '<div id="cust-view-custdata" class="freebil-page-section">';
  $output .= '<h4>' . t('Customer Details') . '</h4>';
  $output .= theme('table', array('header' => $variables['cust_hdr_tbl']['header'], 'rows' => $variables['cust_hdr_tbl']['rows']));

  $output .= '<h4>' . t('Address') . '</h4>';
  $output .= '<p>';
  foreach ($variables['cust_addr_tbl']['rows'] as $addr_line) {
    $output .= $addr_line . '<br>';
  }
  $output .= '</p>';

  // Customer Operations Links.
  $cust_id = $variables['cust_data']['cust_id'];
  $output .= '<div class="freebil-operation">';
  $path = 'admin/freebil/customers/' . $cust_id . '/edit';
  $output .= l(t('Edit Customer Details'), $path, array('attributes' => array('class' => array('freebil-link'))));
  $output .= '</div>';
  $output .= '<div class="freebil-operation">';
  $path = 'admin/freebil/customers/' . $cust_id . '/delete';
  $output .= l(t('Delete this customer'), $path, array('attributes' => array('class' => array('freebil-link'))));
  $output .= '</div>';

  $output .= '</div>';

  // Customer Email Section.
  $output .= '<div id="cust-view-email" class="freebil-page-section">';
  $output .= '<strong>' . t('@cust_name Email Addresses', array('@cust_name' => $cust_name)) . '</strong>';

  // Add New Email Link.
  $email_links['addEmail'] = array(
    'href' => 'admin/freebil/email/' . $cust_id . '/customers/create',
    'title' => t('Add Email'),
  );
  $output .= theme('links', array(
     'links' => $email_links,
     'attributes' => array('class' => array('action-links')),
   ));

  if (!empty($variables['cust_email_tbl']['rows'])) {
    $output .= theme('table', array('header' => $variables['cust_email_tbl']['header'], 'rows' => $variables['cust_email_tbl']['rows']));
  }
  $output .= '</div>';

  // Customer Phone Section.
  $output .= '<div id="cust-view-phone" class="freebil-page-section">';
  $output .= '<strong>' . t('@cust_name Phone Numbers', array('@cust_name' => $cust_name)) . '</strong>';

  // Add New Phone Link.
  $phone_links['addPhone'] = array(
    'href' => 'admin/freebil/phone/' . $cust_id . '/customers/create',
    'title' => t('Add Phone'),
  );
  $output .= theme('links', array(
     'links' => $phone_links,
     'attributes' => array('class' => array('action-links')),
   ));

  if (!empty($variables['cust_phone_tbl']['rows'])) {
    $output .= theme('table', array('header' => $variables['cust_phone_tbl']['header'], 'rows' => $variables['cust_phone_tbl']['rows']));
  }
  $output .= '</div>';

  // Customer Contacts section.
  $output .= '<div id="cust-view-contacts" class="freebil-page-section">';
  $output .= '<strong>' . t('@cust_name Contacts', array('@cust_name' => $cust_name)) . '</strong>';

  // Add New Contact Link.
  $cont_links['addCont'] = array(
    'href' => 'admin/freebil/contacts/' . $cust_id . '/create',
    'title' => t('Add a Customer Contact'),
  );
  $output .= theme('links', array(
     'links' => $cont_links,
     'attributes' => array('class' => array('action-links')),
   ));

  if (!empty($variables['cust_contact_tbl']['rows'])) {
    $output .= theme('table', array('header' => $variables['cust_contact_tbl']['header'], 'rows' => $variables['cust_contact_tbl']['rows']));
  }

  $output .= '</div>';

  return $output;
}

/**
 * Builds markup for rendering the customer's invoice listing page.
 *
 * @param array $variables
 *   An associative array containing:
 *   - associative arrays for each data set needed to build the
 *     various sections of the invoice listing page.
 *
 * @return string
 *   The markup for the invoice listing page.
 *
 * @ingroup themeable
 */
function theme_freebil_customer_invoices_page(array $variables) {
  $output = '';
  $output .= t('Add a new invoice, or click the name of an existing invoice to access all additional options
                       related to that invoice.');

  // Invoice list section.
  $output .= '<div id="cust-view-invoices" class="freebil-page-section">';

  // Add New Invoice Link.
  $hdr_links['addInv'] = array(
    'href' => 'admin/freebil/invoices/' . $variables['cust_data']['cust_id'] . '/create',
    'title' => t('Add Invoice'),
  );
  $output .= theme('links', array(
     'links' => $hdr_links,
     'attributes' => array('class' => array('action-links')),
   ));

  if (!empty($variables['cust_invoice_tbl']['rows'])) {
    $output .= theme('table', array('header' => $variables['cust_invoice_tbl']['header'], 'rows' => $variables['cust_invoice_tbl']['rows']));
  }
  $output .= '</div>';

  return $output;
}

/**
 * Formats the high level (header) data for this customer.
 *
 * @param array $cust_data
 *   An associative array containing:
 *   - All data for this customer.
 *
 * @return array
 *   An associative array containing:
 *   - a row containing the headings for each data column
 *   - a (single) row containing the data for each column
 *
 * @see freebil_customer_view_page()
 */
function freebil_customer_view_header_data(array $cust_data) {
  $tbl_header = array(
    'cust_name' => t('Name'),
    'cust_type' => t('Type'),
    'description' => t('Description'),
    'created_ts' => t('Created'),
    'last_update_ts' => t('Last Updated'),
  );

  $tbl_rows[0] = array(
    'cust_name' => $cust_data['cust_name'],
    'cust_type' => $cust_data['cust_type'],
    'description' => $cust_data['description'],
    'created_ts' => format_date($cust_data['created_ts'], 'short'),
    'last_update_ts' => format_date($cust_data['last_update_ts'], 'short'),
  );

  return array('header' => $tbl_header, 'rows' => $tbl_rows);
}

/**
 * Formats the address lines for this customer.
 *
 * @param array $cust_data
 *   An associative array containing:
 *   - All data for this customer.
 *
 * @return array
 *   An associative array containing:
 *   - an empty header array
 *   - an array of address lines
 *
 * @see freebil_customer_view_page()
 */
function freebil_customer_view_address_data(array $cust_data) {
  $tbl_header = array();
  $tbl_rows = array();
  if (!empty($cust_data['addr_line1'])) {
    $tbl_rows[0] = $cust_data['addr_line1'];
  }
  if (!empty($cust_data['addr_line2'])) {
    $tbl_rows[1] = $cust_data['addr_line2'];
  }
  if (!empty($cust_data['addr_line3'])) {
    $tbl_rows[2] = $cust_data['addr_line3'];
  }
  if (!empty($cust_data['addr_line4'])) {
    $tbl_rows[3] = $cust_data['addr_line4'];
  }

  return array('header' => $tbl_header, 'rows' => $tbl_rows);
}

/**
 * Calls a function to get all invoices for this customer.
 *
 * @param string $cust_id
 *   The customer ID to get related invoices for.
 *
 * @return array
 *   An associative array containing:
 *   - an associative array containing:
 *     The header array, with names for each data column in our invoice
 *     list table.
 *   - an associative array containing:
 *     An array for each invoice related to the customer, along with the
 *     invoice data.
 *
 * @see freebil_customer_view_page()
 */
function freebil_invoice_list_for_cust($cust_id) {
  $table_data = freebil_get_invoices_for_cust($cust_id);

  return array('header' => $table_data['header'], 'rows' => $table_data['rows']);
}

/**
 * Calls a function to get all contacts for this customer.
 *
 * @param string $cust_id
 *   The customer ID to get related contacts for.
 *
 * @return array
 *   An associative array containing:
 *   - an associative array containing:
 *     The header array, with names for each data column in our contact
 *     list table.
 *   - an associative array containing:
 *     An array for each contact related to the customer with the contact data.
 *
 * @see freebil_customer_view_page()
 */
function freebil_contact_list_for_cust($cust_id) {
  $table_data = freebil_get_contacts_for_cust($cust_id);

  return array('header' => $table_data['header'], 'rows' => $table_data['rows']);
}
