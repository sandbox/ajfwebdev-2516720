<?php

/**
 * @file
 * Renders the contact page.
 *
 * Customers may have multiple points of contact.
 * The contact page displays information for an individual contact
 * and includes options for the user to add, change or remove contact data.
 *
 * The contact interface has many similarities with the customer interface,
 * but also enough differences to require a custom treatment.
 */

/**
 * Page Callback: Displays the contact data with options to make changes.
 *
 * @param string $cust
 *   The customer ID for the contact to be displayed.
 *
 * @return string
 *   Markup for the page to display the contact info.
 *
 * @see freebil_menu()
 */
function freebil_contact_view_page($cust) {

  $cont_data = freebil_get_cust_data($cust);
  $parent_cust_name = freebil_get_customer_name($cont_data['parent_cust_id']);

  drupal_set_title($parent_cust_name . ' ' . t('Contact'));

  $crumbs = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('freebil'), 'admin/freebil'),
    l(t('Customers'), 'admin/freebil/customers'),
    l($parent_cust_name, 'admin/freebil/customers/' . $cont_data['parent_cust_id'] . '/view'),
  );
  drupal_set_breadcrumb($crumbs);

  $cust_hdr_tbl = freebil_customer_view_header_data($cont_data);
  $cust_addr_tbl = freebil_customer_view_address_data($cont_data);
  $cust_email_tbl = freebil_emails_for_cust_links($cust, 'contacts');
  $cust_phone_tbl = freebil_phones_for_cust_links($cust, 'contacts');

  return theme('freebil_contact_view_page',
    array(
      'cust_data' => $cont_data,
      'cust_hdr_tbl' => $cust_hdr_tbl,
      'cust_phone_tbl' => $cust_phone_tbl,
      'cust_email_tbl' => $cust_email_tbl,
      'cust_addr_tbl' => $cust_addr_tbl,
    ));
}

/**
 * Builds markup for rendering the contact page.
 *
 * @param array $variables
 *   An associative array containing:
 *   - Associative arrays for each data set needed to
 *     build the various sections of the contact page.
 *
 * @return string
 *   The markup for the contact page.
 *
 * @ingroup themeable
 */
function theme_freebil_contact_view_page(array $variables) {
  $output = '';
  $cust_name = $variables['cust_data']['cust_name'];
  $parent_cust_id = $variables['cust_data']['parent_cust_id'];

  // Customer data section.
  $output .= '<div id="cust-view-custdata" class="freebil-page-section">';
  $output .= '<h4>' . t('Contact Details') . '</h4>';
  $output .= theme('table', array('header' => $variables['cust_hdr_tbl']['header'], 'rows' => $variables['cust_hdr_tbl']['rows']));

  $output .= '<h4>' . t('Address') . '</h4>';
  $output .= '<p>';
  foreach ($variables['cust_addr_tbl']['rows'] as $addr_line) {
    $output .= $addr_line . '<br>';
  }
  $output .= '</p>';

  // Customer Operations Links.
  $cust_id = $variables['cust_data']['cust_id'];
  $output .= '<div class="freebil-operation">';
  $path = 'admin/freebil/contacts/' . $cust_id . '/edit';
  $output .= l(t('Edit Contact Details'), $path, array('attributes' => array('class' => array('freebil-link'))));
  $output .= '</div>';
  $output .= '<div class="freebil-operation">';
  $path = 'admin/freebil/contacts/' . $cust_id . '/delete';
  $output .= l(t('Delete this Contact'), $path, array('attributes' => array('class' => array('freebil-link'))));
  $output .= '</div>';

  $output .= '</div>';

  // Customer Email Section.
  $output .= '<div id="cont-view-email" class="freebil-page-section">';
  $output .= '<h4>' . $cust_name . ' ' . t('Email Addresses') . '</h4>';

  // Add New Email Link.
  $email_links['add_email'] = array(
    'href' => 'admin/freebil/email/' . $cust_id . '/contacts/create',
    'title' => t('Add Email'),
  );
  $output .= theme('links', array(
     'links' => $email_links,
     'attributes' => array('class' => array('action-links')),
   ));

  if (!empty($variables['cust_email_tbl']['rows'])) {
    $output .= theme('table', array('header' => $variables['cust_email_tbl']['header'], 'rows' => $variables['cust_email_tbl']['rows']));
  }
  $output .= '</div>';

  // Customer Phone Section.
  $output .= '<div id="cont-view-phone" class="freebil-page-section">';
  $output .= '<h4>' . $cust_name . ' ' . t('Phone Numbers') . '</h4>';

  // Add New Phone Link.
  $phone_links['add_phone'] = array(
    'href' => 'admin/freebil/phone/' . $cust_id . '/contacts/create',
    'title' => t('Add Phone'),
  );
  $output .= theme('links', array(
     'links' => $phone_links,
     'attributes' => array('class' => array('action-links')),
   ));

  if (!empty($variables['cust_phone_tbl']['rows'])) {
    $output .= theme('table', array('header' => $variables['cust_phone_tbl']['header'], 'rows' => $variables['cust_phone_tbl']['rows']));
  }
  $output .= '</div>';

  // Link back to parent Customer.
  $page_links['to_cust'] = array(
    'href' => 'admin/freebil/customers/' . $parent_cust_id . '/view',
    'title' => t('Back to Customer'),
  );
  $output .= theme('links', array(
     'links' => $page_links,
     'attributes' => array('class' => array('')),
   ));

  return $output;
}
