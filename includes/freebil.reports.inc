<?php

/**
 * @file
 * Freebil reports processing.
 *
 * Generates reports on earnings.
 */

 /**
  * Page Callback: A form to display reports.
  *
  * @return array
  *   A form array
  *
  * @see freebil_menu()
  * @see freebil_reports_page_form()
  *
  * @ingroup forms
  */
function freebil_reports_page() {
  return drupal_get_form('freebil_reports_page_form');
}

/**
 * Form constructor for the invoice reports form.
 *
 * This is the wrapper function for a multi step form process.
 * First determine which type of report is being requested,
 * then display the report.
 *
 * The code for multi step form processing is from
 * @link http://bryanjones.us/article/custom-multi-step-forms-drupal-7 Bryan Jones @endlink.
 *
 * @return array
 *   A form array
 *
 * @see freebil_report_type_select_form()
 * @see freebil_reports_page_form_submit()
 *
 * @ingroup forms
 */
function freebil_reports_page_form($form, &$form_state) {
  // Check to see if anything has been stored.
  if ($form_state['rebuild']) {
    $form_state['values'] = array();
  }
  if (empty($form_state['storage'])) {
    // No step has been set so start with the first.
    $form_state['storage'] = array(
      'step' => 'freebil_report_type_select_form',
    );
  }

  // Return the current form.
  $function = $form_state['storage']['step'];
  $form = $function($form, $form_state);
  return $form;
}

/**
 * Submit handler for our multi step wrapper form.
 *
 * @see freebil_reports_page_form()
 */
function freebil_reports_page_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  if (isset($values['back']) && $values['op'] == $values['back']) {
    // Moving back in form.
    $step = $form_state['storage']['step'];
    // Call current step submit handler if it exists to unset step form data.
    if (function_exists($step . '_submit')) {
      $function = $step . '_submit';
      $function($form, $form_state);
    }
    // Remove the last saved step so we use it next.
    $last_step = array_pop($form_state['storage']['steps']);
    $form_state['storage']['step'] = $last_step;
  }
  else {
    // Record step.
    $step = $form_state['storage']['step'];
    $form_state['storage']['steps'][] = $step;
    // Call step submit handler if it exists.
    if (function_exists($step . '_submit')) {
      $function = $step . '_submit';
      $function($form, $form_state);
    }
  }
}

/**
 * Form constructor for the report type select form.
 *
 * @return array
 *   A form array
 *
 * @see freebil_reports_page_form()
 *
 * @ingroup forms
 */
function freebil_report_type_select_form($form, &$form_state) {
  // Set the page breadcrumb.
  $crumbs = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('freebil'), 'admin/freebil'),
    l(t('Customers'), 'admin/freebil/customers'),
  );
  drupal_set_breadcrumb($crumbs);

  // Get years with invoice and payment data.
  $years_with_data = freebil_get_years_with_data();

  if (count($years_with_data) == 0) {
    $form['message'] = array(
      '#markup' => '<p>' . t('There is no data to report on.') . '</p>',
    );
  }
  else {
    $form['report_year'] = array(
      '#type' => 'radios',
      '#title' => t('Report Year'),
      '#options' => drupal_map_assoc($years_with_data),
      '#default_value' => $years_with_data[0],
      '#description' => t('Select the year to report on. Years that you have data for are shown.'),
    );

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Display Report'),
    );
    $form['actions']['cancel'] = array(
      '#markup' => l(t('Cancel'), 'admin/freebil/customers'),
    );
  }

  return $form;
}

/**
 * Submit handler for the report type select form.
 *
 * @see freebil_report_type_select_form()
 */
function freebil_report_type_select_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $report_year = $values['report_year'];

  // Save values to the form storage, add the next step function callback.
  $form_state['rebuild'] = TRUE;
  $form_state['storage']['report_year'] = $report_year;
  $form_state['storage']['step'] = 'freebil_report_yearly_form';

}

/**
 * Form constructor for the yearly financial report form.
 *
 * @return array
 *   A form array.
 *
 * @see freebil_report_type_select_form_submit()
 *
 * @ingroup forms
 */
function freebil_report_yearly_form($form, &$form_state) {
  // Set the page breadcrumb.
  $crumbs = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('freebil'), 'admin/freebil'),
    l(t('Customers'), 'admin/freebil/customers'),
  );
  drupal_set_breadcrumb($crumbs);

  $form['title'] = array(
    '#markup' => t('<h4>@report_year Financial Summary</h4>', array('@report_year' => $form_state['storage']['report_year'])),
  );

  $report_year = $form_state['storage']['report_year'];
  $yearly_snapshot_data = freebil_get_yearly_snapshot_data($report_year);

  $form['inv_opened_closed'] = array(
    '#markup' => '<p>' . t('<p>Invoices Opened: @inv_opened<br>Invoices Sent: @inv_sent<br>Invoices Closed: @inv_closed',
      array(
        '@inv_opened' => $yearly_snapshot_data['inv_opened'],
        '@inv_sent' => $yearly_snapshot_data['inv_sent'],
        '@inv_closed' => $yearly_snapshot_data['inv_closed'],
      )) . '</p>',
  );

  $form['yearly_snapshot_table'] = array(
    '#markup' => theme('table', array('header' => $yearly_snapshot_data['snapshot_table']['header'], 'rows' => $yearly_snapshot_data['snapshot_table']['rows'])),
  );
  $form['billed_amt_definition'] = array(
    '#markup' => '<p>' . t('Billed Amount is only counted for invoices that were sent to the customer during the report year. This is determined by evaluating the invoice Sent Date.') . '</p>',
  );
  $form['title_details_billed'] = array(
    '#markup' => '<h4>' . t('Billed Amount Details') . '</h4>',
  );
  $form['billed_amt_details'] = array(
    '#markup' => theme('table', array('header' => $yearly_snapshot_data['billed_details']['header'], 'rows' => $yearly_snapshot_data['billed_details']['rows'])),
  );
  $form['title_details_balance'] = array(
    '#markup' => '<h4>' . t('Balance Due Details') . '</h4>',
  );
  $form['balance_due_details'] = array(
    '#markup' => theme('table', array('header' => $yearly_snapshot_data['balance_details']['header'], 'rows' => $yearly_snapshot_data['balance_details']['rows'])),
  );
  $form['title_detail_payment'] = array(
    '#markup' => '<h4>' . t('Payment Details') . '</h4>',
  );
  $form['payment_details'] = array(
    '#markup' => theme('table', array('header' => $yearly_snapshot_data['payment_details']['header'], 'rows' => $yearly_snapshot_data['payment_details']['rows'])),
  );

  return $form;
}

/**
 * Organizes the yearly financial data snapshot.
 *
 * @param string $report_year
 *   The year for this report.
 *
 * @return array
 *   An associative array containing:
 *   - Snapshot data as a header row and a data row for
 *     input to theme table, and other variables.
 */
function freebil_get_yearly_snapshot_data($report_year) {
  $inv_opened_count = freebil_get_inv_opened_count($report_year);
  $inv_sent_count = freebil_get_inv_sent_count($report_year);
  $inv_closed_count = freebil_get_inv_closed_count($report_year);

  $total_billed = freebil_total_billed_for_year($report_year);
  $total_billed_details = freebil_total_billed_details($report_year);
  $total_balance_due = freebil_total_balance_due($report_year);
  $total_balance_details = freebil_total_balance_details($report_year);
  $total_payment = freebil_total_payment_for_year($report_year);
  $total_payment_details = freebil_total_payment_details($report_year);

  $snapshot_rows = freebil_build_snapshot_table(array(
    count($total_billed),
    count($total_balance_due),
    count($total_payment),
  ));

  $snapshot_header = array(
    'currency_code' => t('Currency'),
    'total_billed_amt' => t('Total Billed'),
    'total_payment_amt' => t('Total Payments'),
    'total_balance_due' => t('Outstanding Balance Due'),
  );
  $wk_array = array();
  foreach ($total_billed as $row) {
    $wk_array[$row['currency_code']]['total_billed_amt'] = $row['total_billed_amt'];
  }
  foreach ($total_payment as $row) {
    $wk_array[$row['currency_code']]['total_payment_amt'] = $row['total_payment_amt'];
  }
  foreach ($total_balance_due as $row) {
    $wk_array[$row['currency_code']]['total_balance_due'] = $row['total_balance_due'];
  }

  $count = 0;
  foreach ($wk_array as $currency => $row) {
    // Add the currency code (key) as the first array element of each row.
    $snapshot_rows[$count]['currency_code'] = $currency;

    if (array_key_exists('total_billed_amt', $row)) {
      $snapshot_rows[$count]['total_billed_amt'] = $row['total_billed_amt'];
    }
    if (array_key_exists('total_payment_amt', $row)) {
      $snapshot_rows[$count]['total_payment_amt'] = $row['total_payment_amt'];
    }
    if (array_key_exists('total_balance_due', $row)) {
      $snapshot_rows[$count]['total_balance_due'] = $row['total_balance_due'];
    }
    $count++;
  }
  $snapshot_table = array('header' => $snapshot_header, 'rows' => $snapshot_rows);

  return array(
    'inv_opened' => $inv_opened_count,
    'inv_sent' => $inv_sent_count,
    'inv_closed' => $inv_closed_count,
    'snapshot_table' => $snapshot_table,
    'billed_details' => $total_billed_details,
    'balance_details' => $total_balance_details,
    'payment_details' => $total_payment_details,
  );
}

/**
 * Builds an empty array for the snapshot report.
 *
 * @param array $row_counts
 *   An array containing the row counts for each type
 *   of table data used in this report.
 *
 * @return array
 *   An associative array containing:
 *   - The number of rows needed for the report with
 *     default column values.
 */
function freebil_build_snapshot_table(array $row_counts) {
  // Sort row counts from high to low.
  rsort($row_counts);
  $max_row_count = $row_counts[0];
  $out_array = array();

  for ($i = 0; $i < $max_row_count; $i++) {
    $out_array[$i] = array(
      'currency_code' => ' ',
      'total_billed_amt' => '0.00',
      'total_payment_amt' => '0.00',
      'total_balance_due' => '0.00',
    );
  }
  return $out_array;
}

/**
 * Gets the total billed amount for the input year grouped by currency code.
 *
 * @param string $report_year
 *   The year to use in the query.
 *
 * @return array
 *   An associative array containing:
 *   - The query result rows.
 */
function freebil_total_billed_for_year($report_year) {
  $query = 'SELECT
   currency_code
  , sum(total_billed_amt) as total_billed_amt
   FROM {freebil_invoice}
   WHERE sent_year = :report_year
   GROUP BY currency_code';
  $result = db_query($query, array(':report_year' => $report_year));

  $rows = array();
  $count = 0;
  foreach ($result as $row) {
    $rows[$count] = array(
      'currency_code' => check_plain($row->currency_code),
      'total_billed_amt' => check_plain($row->total_billed_amt),
    );
    $count++;
  }
  return $rows;
}

/**
 * Gets the total billed details for the input year.
 *
 * @param string $report_year
 *   The year to use in the query.
 *
 * @return array
 *   An associative array containing:
 *   - The query result rows.
 */
function freebil_total_billed_details($report_year) {
  $query = 'SELECT
   c.cust_name
  , i.inv_name
  , i.currency_code
  , i.total_billed_amt
  , i.sent_date
   FROM {freebil_invoice} i
   INNER JOIN {freebil_customer} c
   ON c.cust_id = i.cust_id
   WHERE i.sent_year = :report_year
   ORDER BY i.currency_code, c.cust_name';
  $result = db_query($query, array(':report_year' => $report_year));

  $rows = array();
  $count = 0;
  foreach ($result as $row) {
    $rows[$count] = array(
      'cust_name' => freebil_check_text($row->cust_name),
      'inv_name' => freebil_check_text($row->inv_name),
      'currency_code' => check_plain($row->currency_code),
      'total_billed_amt' => check_plain($row->total_billed_amt),
      'sent_date' => check_plain($row->sent_date),
    );
    $count++;
  }

  $header = array(
    'cust_name' => t('Customer'),
    'inv_name' => t('Invoice'),
    'currency_code' => t('Currency'),
    'total_billed_amt' => t('Total Billed'),
    'sent_date' => t('Sent Date'),
  );
  return array('header' => $header, 'rows' => $rows);
}

/**
 * Gets total balance due for the input year.
 *
 * @param string $report_year
 *   The year to use in the query.
 *
 * @return array
 *   An associative array containing:
 *   - The query result rows.
 */
function freebil_total_balance_due($report_year) {
  $query = 'SELECT
   currency_code
  , sum(balance_due_amt) as total_balance_due
   FROM {freebil_invoice}
   WHERE open_closed_ind = \'O\'
   AND sent_year <= :report_year
   GROUP BY currency_code';
  $result = db_query($query, array(':report_year' => $report_year));

  $rows = array();
  $count = 0;
  foreach ($result as $row) {
    $rows[$count] = array(
      'currency_code' => check_plain($row->currency_code),
      'total_balance_due' => check_plain($row->total_balance_due),
    );
    $count++;
  }
  return $rows;
}

/**
 * Gets balance details for the input year.
 *
 * @param string $report_year
 *   The year to use in the query.
 *
 * @return array
 *   An associative array containing:
 *   - The query result rows.
 */
function freebil_total_balance_details($report_year) {
  $query = 'SELECT
   c.cust_name
  , i.inv_name
  , i.currency_code
  , i.balance_due_amt
  , i.sent_date
   FROM {freebil_invoice} i
   INNER JOIN {freebil_customer} c
   ON c.cust_id = i.cust_id
   WHERE i.open_closed_ind = \'O\'
   AND i.sent_year <= :report_year
   ORDER BY i.currency_code, c.cust_name';
  $result = db_query($query, array(':report_year' => $report_year));

  $rows = array();
  $count = 0;
  foreach ($result as $row) {
    $rows[$count] = array(
      'cust_name' => freebil_check_text($row->cust_name),
      'inv_name' => freebil_check_text($row->inv_name),
      'currency_code' => check_plain($row->currency_code),
      'balance_due_amt' => check_plain($row->balance_due_amt),
      'sent_date' => check_plain($row->sent_date),
    );
    $count++;
  }

  $header = array(
    'cust_name' => t('Customer'),
    'inv_name' => t('Invoice'),
    'currency_code' => t('Currency'),
    'balance_due_amt' => t('Balance Due'),
    'sent_date' => t('Sent Date'),
  );
  return array('header' => $header, 'rows' => $rows);
}

/**
 * Gets the total payment amount for the input year.
 *
 * @param string $report_year
 *   The year to use in the query.
 *
 * @return array
 *   An associative array containing:
 *   - The query result rows.
 */
function freebil_total_payment_for_year($report_year) {
  $query = 'SELECT
   currency_code
  , sum(payment_amt) as total_payment_amt
   FROM {freebil_payment}
   WHERE payment_year = :report_year
   GROUP BY currency_code';
  $result = db_query($query, array(':report_year' => $report_year));

  $rows = array();
  $count = 0;
  foreach ($result as $row) {
    $rows[$count] = array(
      'currency_code' => check_plain($row->currency_code),
      'total_payment_amt' => check_plain($row->total_payment_amt),
    );
    $count++;
  }
  return $rows;
}

/**
 * Gets the payment details for the input year.
 *
 * @param string $report_year
 *   The year to use in the query.
 *
 * @return array
 *   An associative array containing:
 *   - The query result rows.
 */
function freebil_total_payment_details($report_year) {
  $query = 'SELECT
   c.cust_name
  , i.inv_name
  , p.currency_code
  , p.payment_amt
  , p.payment_date
   FROM {freebil_payment} p
   INNER JOIN {freebil_invoice} i
   ON i.inv_id = p.inv_id
   INNER JOIN {freebil_customer} c
   ON c.cust_id = i.cust_id
   WHERE p.payment_year = :report_year
   ORDER BY p.currency_code, c.cust_name';
  $result = db_query($query, array(':report_year' => $report_year));

  $rows = array();
  $count = 0;
  foreach ($result as $row) {
    $rows[$count] = array(
      'cust_name' => freebil_check_text($row->cust_name),
      'inv_name' => freebil_check_text($row->inv_name),
      'currency_code' => check_plain($row->currency_code),
      'payment_amt' => check_plain($row->payment_amt),
      'payment_date' => check_plain($row->payment_date),
    );
    $count++;
  }

  $header = array(
    'cust_name' => t('Customer'),
    'inv_name' => t('Invoice'),
    'currency_code' => t('Currency'),
    'payment_amt' => t('Payment Amount'),
    'payment_date' => t('Payment Date'),
  );
  return array('header' => $header, 'rows' => $rows);
}

/**
 * Gets the count of invoices that were opened in the given year.
 *
 * @param string $report_year
 *   The year to use in the query.
 *
 * @return string
 *   The count
 */
function freebil_get_inv_opened_count($report_year) {
  $query = 'SELECT
   count(*) as count
   FROM {freebil_invoice}
   WHERE opened_year = :report_year';
  $result = db_query($query, array(':report_year' => $report_year));
  $record = $result->fetchObject();
  $count = $record->count;
  return $count;
}

/**
 * Gets the count of invoices that were sent in the given year.
 *
 * @param string $report_year
 *   The year to use in the query.
 *
 * @return string
 *   The count
 */
function freebil_get_inv_sent_count($report_year) {
  $query = 'SELECT
   count(*) as count
   FROM {freebil_invoice}
   WHERE sent_year = :report_year';
  $result = db_query($query, array(':report_year' => $report_year));
  $record = $result->fetchObject();
  $count = $record->count;
  return $count;
}

/**
 * Gets the count of invoices that were closed in the given year.
 *
 * @param string $report_year
 *   The year to use in the query.
 *
 * @return string
 *   The count
 */
function freebil_get_inv_closed_count($report_year) {
  $query = 'SELECT
   count(*) as count
   FROM {freebil_invoice}
   WHERE closed_year = :report_year';
  $result = db_query($query, array(':report_year' => $report_year));
  $record = $result->fetchObject();
  $count = $record->count;
  return $count;
}

/**
 * Determines which years the user has invoice data for.
 *
 * @return array containing:
 *   A list of distinct, 4 digit years. These are the years we have data for.
 */
function freebil_get_years_with_data() {
  $return_array = array();
  $distinct_years = array();
  $inv_opened_years = freebil_get_distinct_inv_opened_years();
  $inv_closed_years = freebil_get_distinct_inv_closed_years();
  $inv_sent_years = freebil_get_distinct_inv_sent_years();
  $pay_years = freebil_get_distinct_pay_years();

  foreach ($inv_opened_years as $year) {
    $distinct_years[$year] = '';
  }
  foreach ($inv_closed_years as $year) {
    $distinct_years[$year] = '';
  }
  foreach ($inv_sent_years as $year) {
    $distinct_years[$year] = '';
  }
  foreach ($pay_years as $year) {
    $distinct_years[$year] = '';
  }
  foreach ($distinct_years as $year => $null) {
    $return_array[] = $year;
  }
  // Sort years from high to low.
  rsort($return_array);
  return $return_array;
}

/**
 * Pulls distinct payment years.
 *
 * @return array
 *   List of years
 */
function freebil_get_distinct_pay_years() {
  $years = array();
  $query = 'SELECT
   DISTINCT(payment_year)
   FROM {freebil_payment}
   WHERE payment_year <> \' \'';
  $result = db_query($query);
  foreach ($result as $row) {
    $years[] = check_plain($row->payment_year);
  }
  return $years;
}

/**
 * Pulls distinct invoice opened years.
 *
 * @return array
 *   List of years
 */
function freebil_get_distinct_inv_opened_years() {
  $years = array();
  $query = 'SELECT
   DISTINCT(opened_year)
   FROM {freebil_invoice}
   WHERE opened_year <> \' \'';
  $result = db_query($query);
  foreach ($result as $row) {
    $years[] = check_plain($row->opened_year);
  }
  return $years;
}

/**
 * Pulls distinct invoice closed years.
 *
 * @return array
 *   List of years
 */
function freebil_get_distinct_inv_closed_years() {
  $years = array();
  $query = 'SELECT
   DISTINCT(closed_year)
   FROM {freebil_invoice}
   WHERE closed_year <> \' \'';
  $result = db_query($query);
  foreach ($result as $row) {
    $years[] = check_plain($row->closed_year);
  }
  return $years;
}

/**
 * Pulls distinct invoice sent years.
 *
 * @return array
 *   List of years
 */
function freebil_get_distinct_inv_sent_years() {
  $years = array();
  $query = 'SELECT
   DISTINCT(sent_year)
   FROM {freebil_invoice}
   WHERE sent_year <> \' \'';
  $result = db_query($query);
  foreach ($result as $row) {
    $years[] = check_plain($row->sent_year);
  }
  return $years;
}

/**
 * Calculates the financial quarter based on the input month.
 *
 * @param string $month
 *   A string representation of the 2 digit numeric month.
 *
 * @return string
 *   A string representation of the quarter, like "Q1".
 */
function freebil_get_financial_quarter($month) {
  $quarter = 'XX';
  switch ($month) {
    case "01":
    case "02":
    case "03":
      $quarter = 'Q1';
      break;

    case "04":
    case "05":
    case "06":
      $quarter = 'Q2';
      break;

    case "07":
    case "08":
    case "09":
      $quarter = 'Q3';
      break;

    case "10":
    case "11":
    case "12":
      $quarter = 'Q4';
      break;

  }
  return $quarter;
}

/**
 * Formats year and financial quarter from an input date.
 *
 * @param string $date
 *   A date in the format: yyyy-mm-dd.
 *
 * @return array
 *   An associative array containing:
 *   - A 4 digit year, and a financial quarter formatted like 'Q1'.
 */
function freebil_format_year_quarter($date) {
  $date_parts = explode('-', $date);
  $year  = $date_parts[0];
  $month = $date_parts[1];
  $quarter = freebil_get_financial_quarter($month);

  return array('year' => $year, 'quarter' => $quarter);
}
