<?php

/**
 * @file
 * Manages customer data.
 *
 * Customers are the parent node in our data model.
 * All other types of data, such as contacts and invoices,
 * must be associated with a customer.
 */

/**
 * Page Callback: Displays the customer list and a link to add a new customer.
 *
 * @return array
 *   A render array for the page to display the customer list page.
 *
 * @see freebil_menu()
 */
function freebil_customer_list() {

  // Set the page breadcrumb.
  $crumbs = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('freebil'), 'admin/freebil'),
  );
  drupal_set_breadcrumb($crumbs);

  // Get the header and customer query results arrays.
  $cust_data_tbl = freebil_get_all_customers();

  return theme('freebil_customer_page', array('header' => $cust_data_tbl['header'], 'rows' => $cust_data_tbl['rows']));
}

/**
 * Returns HTML of customer list page.
 *
 * @param array $variables
 *   An associative array containing:
 *   - the column headings and data rows for a table to list all customers.
 *
 * @ingroup themeable
 */
function theme_freebil_customer_page(array $variables) {
  $output = '';

  $hdr_links['addCust'] = array(
    'href' => 'admin/freebil/customers/create',
    'title' => t('Add Customer'),
  );
  $output .= theme('links', array(
     'links' => $hdr_links,
     'attributes' => array('class' => array('action-links')),
   ));

  $output .= theme('table', array('header' => $variables['header'], 'rows' => $variables['rows']));

  return $output;
}

/**
 * Gets a list of all customers.
 *
 * @return array
 *   An associative array containing:
 *   - the column headings and data rows for a list of all customers.
 */
function freebil_get_all_customers() {
  $query = 'SELECT cust_id, cust_name, cust_type, description, created_ts, last_update_ts' .
  ' FROM {freebil_customer} WHERE cust_type <> :type ORDER BY cust_name';
  $result = db_query($query, array(':type' => 'Contact'));

  $header = array(
    t('Customer Name'),
    t('Type'),
    t('Description'),
    t('Created'),
    t('Last Updated'),
  );

  $rows = array();
  $count = 0;

  foreach ($result as $row) {

    // Make the customer name link to the customer view page.
    $cust_link['name'] = array(
      'href' => 'admin/freebil/customers/' . check_plain($row->cust_id) . '/view',
      'title' => freebil_check_text($row->cust_name),
    );
    $cust_name = theme('links', array(
      'links' => $cust_link,
      'attributes' => array('class' => array('links', 'inline')),
    ));

    $rows[$count] = array(
      'cust_name' => $cust_name,
      'cust_type' => check_plain($row->cust_type),
      'description' => freebil_check_text($row->description),
      'created_ts' => check_plain(format_date($row->created_ts, 'short')),
      'last_update_ts' => check_plain(format_date($row->last_update_ts, 'short')),
    );
    $count++;
  }

  return array('header' => $header, 'rows' => $rows);
}

/**
 * Page Callback: A form to create a customer.
 *
 * @return array
 *   A form array.
 *
 * @see freebil_menu()
 * @see freebil_customer_create_form_validate()
 * @see freebil_customer_create_form_submit()
 *
 * @ingroup forms
 */
function freebil_customer_create_form($form, &$form_state) {
  // Set the page breadcrumb.
  $crumbs = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('freebil'), 'admin/freebil'),
    l(t('Customers'), 'admin/freebil/customers'),
  );
  drupal_set_breadcrumb($crumbs);

  return freebil_build_customer_form($form, $form_state, "create");
}

/**
 * Form validation handler for freebil_customer_create_form().
 *
 * @see freebil_customer_create_form_submit()
 */
function freebil_customer_create_form_validate($form, &$form_state) {
  $input_cust_name = freebil_check_text($form_state['values']['customer_name']);
  if (ctype_space($input_cust_name) || $input_cust_name == '') {
    form_set_error('customer_name', t('Enter a value for Customer Name.'));
  }
}

/**
 * Form submission handler for freebil_customer_create_form().
 *
 * @see freebil_customer_create_form_validate()
 */
function freebil_customer_create_form_submit($form, &$form_state) {

  $cust_row = array(
    'cust_name' => $form_state['values']['customer_name'],
    'cust_type' => $form_state['values']['customer_type'],
    'addr_line1' => $form_state['values']['addr_line1'],
    'addr_line2' => $form_state['values']['addr_line2'],
    'addr_line3' => $form_state['values']['addr_line3'],
    'addr_line4' => $form_state['values']['addr_line4'],
    'description' => $form_state['values']['description'],
  );

  $cust_id = freebil_customer_insert($cust_row);
  $form_state['redirect'] = 'admin/freebil/customers/' . $cust_id . '/view';
  drupal_set_message(t('Successfully saved Customer.'));
}

/**
 * Customer insert API.
 *
 * @param array $row
 *   An associative array containing:
 *   - The column names as keys and the column values as values for
 *     the customer row to be inserted.
 *
 * @return object
 *   - InsertQuery object
 */
function freebil_customer_insert(array $row) {
  $col_array = array();
  if ($row['cust_type'] == 'Contact') {
    $col_array['parent_cust_id'] = $row['parent_cust_id'];
  }
  $col_array['cust_name'] = $row['cust_name'];
  $col_array['cust_type'] = $row['cust_type'];
  $col_array['addr_line1'] = $row['addr_line1'];
  $col_array['addr_line2'] = $row['addr_line2'];
  $col_array['addr_line3'] = $row['addr_line3'];
  $col_array['addr_line4'] = $row['addr_line4'];
  $col_array['description'] = $row['description'];
  $col_array['created_ts'] = REQUEST_TIME;
  $col_array['last_update_ts'] = REQUEST_TIME;

  $obj = db_insert('freebil_customer')
    ->fields($col_array)->execute();

  return $obj;
}

/**
 * Page Callback: A form to edit a customer.
 *
 * @param string $cust
 *   The customer ID for the customer to be edited.
 *
 * @return array
 *   A form array
 *
 * @see freebil_menu()
 */
function freebil_customer_page_edit($cust) {
  return drupal_get_form('freebil_customer_edit_form', $cust);
}

/**
 * Form constructor for the customer edit form.
 *
 * @param string $cust
 *   The customer ID to edit.
 *
 * @return array
 *   A form array
 *
 * @see freebil_customer_edit_form_validate()
 * @see freebil_customer_edit_form_submit()
 *
 * @ingroup forms
 */
function freebil_customer_edit_form($form, &$form_state, $cust) {
  $cust_data = freebil_get_cust_data($cust);

  // Set the page breadcrumb.
  $crumbs = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('freebil'), 'admin/freebil'),
    l(t('Customers'), 'admin/freebil/customers'),
    l($cust_data['cust_name'], 'admin/freebil/customers/' . $cust_data['cust_id'] . '/view'),
  );
  drupal_set_breadcrumb($crumbs);

  $form = freebil_build_customer_form($form, $form_state, "edit", $cust_data);

  return $form;
}

/**
 * Form validation handler for freebil_customer_edit_form().
 *
 * @see freebil_customer_edit_form_submit()
 */
function freebil_customer_edit_form_validate($form, &$form_state) {
  $input_cust_name = freebil_check_text($form_state['values']['customer_name']);
  if (ctype_space($input_cust_name) || $input_cust_name == '') {
    form_set_error('customer_name', t('Enter a value for Customer Name.'));
  }
}

/**
 * Form submission handler for freebil_customer_edit_form().
 *
 * @see freebil_customer_edit_form_validate()
 */
function freebil_customer_edit_form_submit($form, &$form_state) {
  $row = array(
    'cust_name' => $form_state['values']['customer_name'],
    'cust_type' => $form_state['values']['customer_type'],
    'addr_line1' => $form_state['values']['addr_line1'],
    'addr_line2' => $form_state['values']['addr_line2'],
    'addr_line3' => $form_state['values']['addr_line3'],
    'addr_line4' => $form_state['values']['addr_line4'],
    'description' => $form_state['values']['description'],
  );

  // Update this customer row with data from the submitted form.
  $cust_id = $form_state['values']['cust_id'];
  freebil_customer_update($row, $cust_id);
  $form_state['redirect'] = 'admin/freebil/customers/' . $cust_id . '/view';
  drupal_set_message(t('Successfully updated Customer.'));
}

/**
 * Customer update API.
 *
 * @param array $row
 *   An associative array containing:
 *   - The column names as keys and the column values as values for
 *     the customer row to be updated.
 * @param string $cust_id
 *   The customer ID for the customer to be updated.
 *
 * @return object
 *   An UpdateQuery object
 */
function freebil_customer_update(array $row, $cust_id) {
  $update_query_obj = db_update('freebil_customer')
    ->fields(array(
      'cust_name' => $row['cust_name'],
      'cust_type' => $row['cust_type'],
      'addr_line1' => $row['addr_line1'],
      'addr_line2' => $row['addr_line2'],
      'addr_line3' => $row['addr_line3'],
      'addr_line4' => $row['addr_line4'],
      'description' => $row['description'],
      'last_update_ts' => REQUEST_TIME,
  ))
    ->condition('cust_id', $cust_id, '=')
    ->execute();

  return $update_query_obj;
}

/**
 * Page Callback: A form to delete a customer.
 *
 * @param string $cust
 *   The customer ID for the customer to be deleted.
 *
 * @return array
 *   A form array
 *
 * @see freebil_menu()
 */
function freebil_customer_page_delete($cust) {
  return drupal_get_form('freebil_customer_delete_form', $cust);
}

/**
 * Form constructor for the customer delete form.
 *
 * @param string $cust
 *   The customer ID to delete.
 *
 * @return array
 *   A form array
 *
 * @see freebil_customer_delete_form_validate()
 * @see freebil_customer_delete_form_submit()
 *
 * @ingroup forms
 */
function freebil_customer_delete_form($form, &$form_state, $cust) {
  $cust_data = freebil_get_cust_data($cust);

  // Set the page breadcrumb.
  $crumbs = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('freebil'), 'admin/freebil'),
    l(t('Customers'), 'admin/freebil/customers'),
    l($cust_data['cust_name'], 'admin/freebil/customers/' . $cust_data['cust_id'] . '/view'),
  );
  drupal_set_breadcrumb($crumbs);

  $form = freebil_build_customer_delete_form($cust_data);

  return $form;
}

/**
 * Form validation handler for freebil_customer_delete_form().
 *
 * @see freebil_customer_delete_form_submit()
 */
function freebil_customer_delete_form_validate($form, &$form_state) {
  if (freebil_customer_orphans_exist($form_state['values']['cust_id'])) {
    form_set_error('identity', t('Orphans detected! Please delete all related invoices before deleting this customer.'));
  }
}

/**
 * Form submission handler for freebil_customer_delete_form().
 *
 * @see freebil_customer_delete_form_validate()
 */
function freebil_customer_delete_form_submit($form, &$form_state) {

  freebil_customer_cascading_delete($form_state['values']['cust_id']);

  $form_state['redirect'] = 'admin/freebil/customers';
  drupal_set_message(t('Successfully deleted Customer.'));
}

/**
 * Customer cascading delete API.
 *
 * A transactional delete of customer, email and phone table rows,
 * with one commit performed at the end.
 * The transaction commits when the assigned $transaction
 * variable goes out of scope.
 *
 * @param string $cust_id
 *   The customer ID for the customer to be deleted.
 *
 * @return object
 *   A DeleteQuery object
 */
function freebil_customer_cascading_delete($cust_id) {
  $transaction = db_transaction();
  $delete_query_obj = '';

  // First store cust ID for customer and all related contacts in an array.
  $cust_id_array = freebil_get_related_cust_ids($cust_id);
  // Delete related email and phone rows then the customer row for each cust ID.
  // Contacts are also stored on the customer table.
  foreach ($cust_id_array as $cid) {
    // Get all related email row IDs.
    $email_ids = freebil_get_related_email_ids($cid);
    // Delete all related email rows.
    foreach ($email_ids as $email_id) {
      $delete_query_obj = freebil_email_delete($email_id);
    }
    // Get all related phone row IDs.
    $phone_ids = freebil_get_related_phone_ids($cid);
    // Delete all related email rows.
    foreach ($phone_ids as $phone_id) {
      $delete_query_obj = freebil_phone_delete($phone_id);
    }
    // Delete the customer row.
    $delete_query_obj = freebil_customer_delete($cid);
  }

  return $delete_query_obj;
}

/**
 * Customer delete API.
 *
 * @param string $cust_id
 *   The customer ID for the customer to be deleted.
 *
 * @return object
 *   A DeleteQuery object
 */
function freebil_customer_delete($cust_id) {
  $delete_query_obj = db_delete('freebil_customer')
    ->condition('cust_id', $cust_id)
    ->execute();
  return $delete_query_obj;
}

/**
 * Builds forms for creating or editing customers.
 *
 * @param string $type
 *   Indicates the type of form to build: "create" or "edit".
 * @param array $cust_data
 *   An associative array containing:
 *   - All data for an existing customer with column names as keys
 *     and the column values as values.
 *
 * @return array
 *   A form array
 */
function freebil_build_customer_form($form, &$form_state, $type, $cust_data = array()) {

  if ($type === "edit") {
    // Need the customer ID to update the table row on form submit.
    // Also need the cust ID to display associated email rows.
    $form['cust_id'] = array(
      '#type' => 'hidden',
      '#value' => $cust_data['cust_id'],
    );
  }

  $form['parent_cust_id'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );

  $form['customer_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Customer Name'),
    '#maxlength' => 255,
  );
  $form['customer_type'] = array(
    '#type' => 'select',
    '#title' => t('Customer Type'),
    '#options' => array('Company' => t('Company'), 'Individual' => t('Individual')),
    '#default_value' => 'Company',
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Customer Description'),
    '#maxlength' => 255,
    '#wysiwyg' => FALSE,
  );
  // Customer address lines.
  $form['addr_line1'] = array(
    '#type' => 'textfield',
    '#title' => t('Address Line 1'),
    '#maxlength' => 255,
  );
  $form['addr_line2'] = array(
    '#type' => 'textfield',
    '#title' => t('Address Line 2'),
    '#maxlength' => 255,
  );
  $form['addr_line3'] = array(
    '#type' => 'textfield',
    '#title' => t('Address Line 3'),
    '#maxlength' => 255,
  );
  $form['addr_line4'] = array(
    '#type' => 'textfield',
    '#title' => t('Address Line 4'),
    '#maxlength' => 255,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  if ($type === "edit") {
    $form['actions']['cancel'] = array(
      '#markup' => l(t('Cancel'), 'admin/freebil/customers/' . $cust_data['cust_id'] . '/view'),
    );
  }
  else {
    $form['actions']['cancel'] = array(
      '#markup' => l(t('Cancel'), 'admin/freebil/customers'),
    );
  }

  // If this is the edit form, populate the values from the customer table.
  if ($type === "edit") {
    $form['customer_name']['#default_value'] = $cust_data['cust_name'];
    $form['customer_type']['#default_value'] = $cust_data['cust_type'];
    $form['addr_line1']['#default_value'] = $cust_data['addr_line1'];
    $form['addr_line2']['#default_value'] = $cust_data['addr_line2'];
    $form['addr_line3']['#default_value'] = $cust_data['addr_line3'];
    $form['addr_line4']['#default_value'] = $cust_data['addr_line4'];
    $form['description']['#default_value'] = $cust_data['description'];
  }

  return $form;
}

/**
 * Builds a form for deleting a customer.
 *
 * @param array $cust_data
 *   An associative array containing:
 *   - All data for an existing customer with column names
 *     as keys and the column values as values.
 *
 * @return array
 *   A form array
 */
function freebil_build_customer_delete_form(array $cust_data) {

  $header = array(
    t('ID'),
    t('Name'),
    t('Type'),
    t('Created'),
    t('Last Updated'),
  );

  $rows[0] = array(
    'cust_id' => $cust_data['cust_id'],
    'cust_name' => $cust_data['cust_name'],
    'cust_type' => $cust_data['cust_type'],
    'created_ts' => format_date($cust_data['created_ts'], 'short'),
    'last_update_ts' => format_date($cust_data['last_update_ts'], 'short'),
  );
  $form['identity'] = array(
    '#markup' => theme('table', array('header' => $header, 'rows' => $rows)),
  );
  $form['confirm'] = array(
    '#markup' => t('<p>Are you sure you want to delete this customer? Deleting the customer will also delete all related contacts.</p>'),
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/freebil/customers/' . $cust_data['cust_id'] . '/view'),
  );
  // Need the customer ID to delete the table row on form submit.
  $form['cust_id'] = array(
    '#type' => 'hidden',
    '#value' => $cust_data['cust_id'],
  );

  return $form;
}

/**
 * Gets all customer data for the input customer ID.
 *
 * @param string $cust
 *   The customer ID to get data for.
 *
 * @return an associative array containing:
 *   - All data for the customer with column names as keys
 *     and the column values as values.
 */
function freebil_get_cust_data($cust) {
  $query = 'SELECT
   cust_id
  , parent_cust_id
  , cust_name
  , cust_type
  , addr_line1
  , addr_line2
  , addr_line3
  , addr_line4
  , description
  , created_ts
  , last_update_ts
   FROM
   {freebil_customer}
   WHERE
   cust_id = :custid';
  $result = db_query($query, array(':custid' => $cust));
  $record = $result->fetchObject();
  $output = array(
    'cust_id' => check_plain($record->cust_id),
    'parent_cust_id' => check_plain($record->parent_cust_id),
    'cust_name' => freebil_check_text($record->cust_name),
    'cust_type' => check_plain($record->cust_type),
    'addr_line1' => freebil_check_text($record->addr_line1),
    'addr_line2' => freebil_check_text($record->addr_line2),
    'addr_line3' => freebil_check_text($record->addr_line3),
    'addr_line4' => freebil_check_text($record->addr_line4),
    'description' => freebil_check_text($record->description),
    'created_ts' => check_plain($record->created_ts),
    'last_update_ts' => check_plain($record->last_update_ts),
  );
  return $output;
}

/**
 * Checks if a customer has any invoices.
 *
 * This check is used to avoid deleting customers
 * that have invoices in the system.
 *
 * @param string $cust_id
 *   The customer ID to check for related invoices.
 *
 * @return bool
 *   TRUE if there are related invoices, FALSE if not.
 */
function freebil_customer_orphans_exist($cust_id) {
  $query = 'SELECT
   inv_id
   FROM
   {freebil_invoice}
   WHERE
   cust_id = :custid';
  $result = db_query($query, array(':custid' => $cust_id));
  $row_count = $result->rowCount();
  if ($row_count > 0) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Gets all customer IDs along with cust names.
 *
 * @return array
 *   An associative array containing:
 *   - Customer IDs as keys and customer names as values.
 */
function freebil_get_customer_keys() {
  $result = db_query("SELECT cust_id, cust_name FROM {freebil_customer} ORDER BY cust_name");

  $out_array = array();
  foreach ($result as $record) {
    $cust_id = check_plain($record->cust_id);
    $cust_name = freebil_check_text($record->cust_name);
    $out_array[$cust_id] = $cust_name;
  }
  return $out_array;
}

/**
 * Gets the customer name for a given customer ID.
 *
 * @param string $cust_id
 *   The customer ID that you want to know the name for.
 *
 * @return string
 *   The customer name
 */
function freebil_get_customer_name($cust_id) {
  $record = db_query("SELECT cust_name FROM {freebil_customer} WHERE cust_id = :custid", array(':custid' => $cust_id))->fetchObject();

  return freebil_check_text($record->cust_name);
}
