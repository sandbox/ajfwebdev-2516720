<?php

/**
 * @file
 * Manages customer contacts.
 *
 * Customers may have multiple points of contact, and these contact details
 * are needed when it's time to choose an email address or physical address
 * to send an invoice to.
 *
 * Contacts are stored on the freebill_customer table along with the customer
 * rows. They are identified by having cust_type = 'Contact'. They also have
 * parent_cust_id populated with the ID of the customer they are associated
 * with.
 *
 * The contact interface has many similarities with the customer interface,
 * but also enough differences to require a custom treatment.
 */

/**
 * Page Callback: A form to create a contact.
 *
 * @param string $cust
 *   The customer ID that this contact is associated with.
 *
 * @return array
 *   A form array
 *
 * @see freebil_menu()
 */
function freebil_contact_page_create($cust) {
  return drupal_get_form('freebil_contact_create_form', $cust);
}

/**
 * Returns a form to create a customer contact.
 *
 * @return array
 *   A form array
 *
 * @see freebil_contact_page_create()
 * @see freebil_contact_create_form_validate()
 * @see freebil_contact_create_form_submit()
 *
 * @ingroup forms
 */
function freebil_contact_create_form($form, &$form_state, $cust_id) {

  $parent_cust_data = freebil_get_cust_data($cust_id);

  // Set the page breadcrumb.
  $crumbs = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('freebil'), 'admin/freebil'),
    l(t('Customers'), 'admin/freebil/customers'),
    l($parent_cust_data['cust_name'], 'admin/freebil/customers/' . $parent_cust_data['cust_id'] . '/view'),
  );
  drupal_set_breadcrumb($crumbs);

  $cont_data = array();
  return freebil_build_contact_form("create", $parent_cust_data['cust_id'], $cont_data);
}

/**
 * Form validation handler for freebil_contact_create_form().
 *
 * @see freebil_contact_create_form_submit()
 */
function freebil_contact_create_form_validate($form, &$form_state) {
  $input_cust_name = freebil_check_text($form_state['values']['customer_name']);
  if (ctype_space($input_cust_name) || $input_cust_name == '') {
    form_set_error('customer_name', t('Enter a value for Contact Name.'));
  }
}

/**
 * Form submission handler for freebil_contact_create_form().
 *
 * @see freebil_contact_create_form_validate()
 */
function freebil_contact_create_form_submit($form, &$form_state) {
  $row = array(
    'parent_cust_id' => $form_state['values']['parent_cust_id'],
    'cust_name' => $form_state['values']['customer_name'],
    'cust_type' => $form_state['values']['customer_type'],
    'addr_line1' => $form_state['values']['addr_line1'],
    'addr_line2' => $form_state['values']['addr_line2'],
    'addr_line3' => $form_state['values']['addr_line3'],
    'addr_line4' => $form_state['values']['addr_line4'],
    'description' => $form_state['values']['description'],
  );

  $cust_id = freebil_customer_insert($row);

  $form_state['redirect'] = 'admin/freebil/contacts/' . $cust_id . '/view';
  drupal_set_message(t('Successfully saved Contact.'));
}

/**
 * Page Callback: A form to edit a contact.
 *
 * @param string $cust
 *   The customer ID for the contact to be edited.
 *
 * @return array
 *   A form array
 *
 * @see freebil_menu()
 */
function freebil_contact_page_edit($cust) {
  return drupal_get_form('freebil_contact_edit_form', $cust);
}


/**
 * Returns a form to edit a customer contact.
 *
 * @return array
 *   A form array
 *
 * @see freebil_contact_page_edit()
 * @see freebil_contact_edit_form_validate()
 * @see freebil_contact_edit_form_submit()
 *
 * @ingroup forms
 */
function freebil_contact_edit_form($form, &$form_state, $cust) {
  $cont_data = freebil_get_cust_data($cust);
  $parent_cust_name = freebil_get_customer_name($cont_data['parent_cust_id']);

  // Set the page breadcrumb.
  $crumbs = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('freebil'), 'admin/freebil'),
    l(t('Customers'), 'admin/freebil/customers'),
    l($parent_cust_name, 'admin/freebil/customers/' . $cont_data['parent_cust_id'] . '/view'),
    l($cont_data['cust_name'], 'admin/freebil/contacts/' . $cont_data['cust_id'] . '/view'),
  );
  drupal_set_breadcrumb($crumbs);

  $form = freebil_build_contact_form("edit", $cont_data['parent_cust_id'], $cont_data);

  return $form;
}

/**
 * Form validation handler for freebil_contact_edit_form().
 *
 * @see freebil_contact_edit_form_submit()
 */
function freebil_contact_edit_form_validate($form, &$form_state) {
  $input_cust_name = freebil_check_text($form_state['values']['customer_name']);
  if (ctype_space($input_cust_name) || $input_cust_name == '') {
    form_set_error('customer_name', t('Enter a value for Contact Name.'));
  }
}

/**
 * Form submission handler for freebil_contact_edit_form().
 *
 * @see freebil_contact_edit_form_validate()
 */
function freebil_contact_edit_form_submit($form, &$form_state) {
  $row = array(
    'cust_name' => $form_state['values']['customer_name'],
    'cust_type' => $form_state['values']['customer_type'],
    'addr_line1' => $form_state['values']['addr_line1'],
    'addr_line2' => $form_state['values']['addr_line2'],
    'addr_line3' => $form_state['values']['addr_line3'],
    'addr_line4' => $form_state['values']['addr_line4'],
    'description' => $form_state['values']['description'],
  );

  // Update this customer row with data from the submitted form.
  $cust_id = $form_state['values']['cust_id'];
  freebil_customer_update($row, $cust_id);
  $form_state['redirect'] = 'admin/freebil/contacts/' . $cust_id . '/view';
  drupal_set_message(t('Successfully updated Contact.'));
}

/**
 * Page Callback: A form to delete a contact.
 *
 * @param string $cust
 *   The customer ID for the contact to be deleted.
 *
 * @return array
 *   A form array
 *
 * @see freebil_menu()
 */
function freebil_contact_page_delete($cust) {
  return drupal_get_form('freebil_contact_delete_form', $cust);
}

/**
 * Returns a form to delete a customer contact.
 *
 * @param string $cust
 *   The customer ID for the contact to be deleted.
 *
 * @return array
 *   A form array
 *
 * @see freebil_contact_page_delete()
 * @see freebil_contact_delete_form_submit()
 *
 * @ingroup forms
 */
function freebil_contact_delete_form($form, &$form_state, $cust) {
  $cust_data = array();
  $cust_data = freebil_get_cust_data($cust);
  $parent_cust_name = freebil_get_customer_name($cust_data['parent_cust_id']);

  // Set the page breadcrumb.
  $crumbs = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('freebil'), 'admin/freebil'),
    l(t('Customers'), 'admin/freebil/customers'),
    l($parent_cust_name, 'admin/freebil/customers/' . $cust_data['parent_cust_id'] . '/view'),
  );
  drupal_set_breadcrumb($crumbs);

  $form = freebil_build_contact_form("delete", $cust_data['parent_cust_id'], $cust_data);

  return $form;
}

/**
 * Form submission handler for freebil_contact_delete_form().
 */
function freebil_contact_delete_form_submit($form, &$form_state) {
  freebil_customer_cascading_delete($form_state['values']['cust_id']);

  $form_state['redirect'] = 'admin/freebil/customers/' . $form_state['values']['parent_cust_id'] . '/view';
  drupal_set_message(t('Successfully deleted Contact.'));
}


/**
 * Builds forms for creating, editing, or deleting contacts.
 *
 * @param string $type
 *   Indicates the type of form to build: "create", "edit", or "delete".
 * @param string $parent_cust_id
 *   The customer ID of the parent customer for this contact.
 * @param array $cont_data
 *   An associative array containing:
 *   - All data for an existing contact with column names as keys and
 *     the column values as values.
 *
 * @return array
 *   A form array
 */
function freebil_build_contact_form($type, $parent_cust_id, $cont_data = array()) {

  if (array_key_exists("parent_cust_id", $cont_data)) {
    $parent_cust_id = $cont_data['parent_cust_id'];
  }

  $parent_cust_name = '';
  if (!empty($parent_cust_id)) {
    $parent_cust_name = freebil_get_customer_name($parent_cust_id);
  }

  $form['parent_cust_id'] = array(
    '#type' => 'hidden',
    '#value' => $parent_cust_id,
  );
  $form['parent-custname'] = array(
    '#markup' => t('<h4>Customer Name: @parent_cust_name</h4>', array('@parent_cust_name' => $parent_cust_name)),
  );

  // Return a custom form for the delete option.
  if ($type === "delete") {

    $header = array(
      t('ID'),
      t('Name'),
      t('Type'),
      t('Created'),
      t('Last Updated'),
    );
    $rows[0] = array(
      'cust_id' => $cont_data['cust_id'],
      'cust_name' => $cont_data['cust_name'],
      'cust_type' => $cont_data['cust_type'],
      'created_ts' => format_date($cont_data['created_ts'], 'short'),
      'last_update_ts' => format_date($cont_data['last_update_ts'], 'short'),
    );
    $form['identity'] = array(
      '#markup' => theme('table', array('header' => $header, 'rows' => $rows)),
    );
    $form['confirm'] = array(
      '#markup' => '<p>' . t('Are you sure you want to delete this contact?') . '</p>',
    );
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
    );
    $form['actions']['cancel'] = array(
      '#markup' => l(t('Cancel'), 'admin/freebil/customers/' . $cont_data['parent_cust_id'] . '/view'),
    );
    // Need the contact's customer ID to delete the table row on form submit.
    $form['cust_id'] = array(
      '#type' => 'hidden',
      '#value' => $cont_data['cust_id'],
    );

    return $form;
  }

  $form['customer_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Contact Name'),
    '#maxlength' => 255,
  );
  $form['customer_type'] = array(
    '#type' => 'hidden',
    '#value' => 'Contact',
  );
  $form['addr_line1'] = array(
    '#type' => 'textfield',
    '#title' => t('Address Line 1'),
    '#maxlength' => 255,
  );
  $form['addr_line2'] = array(
    '#type' => 'textfield',
    '#title' => t('Address Line 2'),
    '#maxlength' => 255,
  );
  $form['addr_line3'] = array(
    '#type' => 'textfield',
    '#title' => t('Address Line 3'),
    '#maxlength' => 255,
  );
  $form['addr_line4'] = array(
    '#type' => 'textfield',
    '#title' => t('Address Line 4'),
    '#maxlength' => 255,
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Contact Description'),
    '#maxlength' => 255,
    '#wysiwyg' => FALSE,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  if ($type === "edit") {
    $form['actions']['cancel'] = array(
      '#markup' => l(t('Cancel'), 'admin/freebil/contacts/' . $cont_data['cust_id'] . '/view'),
    );
  }
  else {
    $form['actions']['cancel'] = array(
      '#markup' => l(t('Cancel'), 'admin/freebil/customers/' . $parent_cust_id . '/view'),
    );
  }

  // If this is the edit form, populate the values from the customer table.
  if ($type === "edit") {
    // Need the customer ID to update the table row on form submit.
    $form['cust_id'] = array(
      '#type' => 'hidden',
      '#value' => $cont_data['cust_id'],
    );
    $form['parent_cust_id']['#default_value'] = $cont_data['parent_cust_id'];
    $form['customer_name']['#default_value'] = $cont_data['cust_name'];
    $form['customer_type']['#default_value'] = $cont_data['cust_type'];
    $form['addr_line1']['#default_value'] = $cont_data['addr_line1'];
    $form['addr_line2']['#default_value'] = $cont_data['addr_line2'];
    $form['addr_line3']['#default_value'] = $cont_data['addr_line3'];
    $form['addr_line4']['#default_value'] = $cont_data['addr_line4'];
    $form['description']['#default_value'] = $cont_data['description'];
  }
  return $form;

}

/**
 * Gets data for all contacts associated with input customer ID.
 *
 * Formats this data as a table with a header row and data
 * rows for each contact.
 *
 * @param string $cust_id
 *   The customer ID to get contacts for.
 *
 * @return array
 *   An associative array containing:
 *   - an associative array containing:
 *     The header array, with names for each data column in our contact
 *     list table.
 *   - an associative array containing:
 *     An array for each contact related to the customer, along with
 *     the contact data.
 *
 * @see freebil_contact_list_for_cust()
 */
function freebil_get_contacts_for_cust($cust_id) {
  $query = 'SELECT
   cust_id
  , parent_cust_id
  , cust_name
  , cust_type
  , addr_line1
  , addr_line2
  , addr_line3
  , addr_line4
  , description
  , created_ts
  , last_update_ts
   FROM
   {freebil_customer}
   WHERE
   parent_cust_id = :custid
   AND cust_type = :custType';
  $result = db_query($query, array(':custid' => $cust_id, ':custType' => 'Contact'));

  $header = array(
    t('Contact Name'),
    t('Description'),
    t('Created'),
    t('Last Updated'),
  );

  $rows = array();
  $count = 0;

  foreach ($result as $row) {

    // Make the contact name link to the contact view page.
    $cont_link['name'] = array(
      'href' => 'admin/freebil/contacts/' . check_plain($row->cust_id) . '/view',
      'title' => freebil_check_text($row->cust_name),
    );
    $cont_name = theme('links', array(
      'links' => $cont_link,
      'attributes' => array('class' => array('links', 'inline')),
    ));

    $rows[$count] = array(
      'cont_name' => $cont_name,
      'description' => freebil_check_text($row->description),
      'created_ts' => check_plain(format_date($row->created_ts, 'short')),
      'last_update_ts' => check_plain(format_date($row->last_update_ts, 'short')),
    );
    $count++;
  }

  return array('header' => $header, 'rows' => $rows);
}

/**
 * Gets the contacts related to a customer.
 *
 * @param string $cust_id
 *   The parent customer ID.
 *
 * @return array containing:
 *   - The parent customer ID plus the child customer IDs
 *     of any related contacts.
 *
 * @see freebil_customer_cascading_delete()
 */
function freebil_get_related_cust_ids($cust_id) {
  $query = 'SELECT
   cust_id
   FROM
   {freebil_customer}
   WHERE
   cust_id = :custid
   OR parent_cust_id = :custid';
  $result = db_query($query, array(':custid' => $cust_id));

  $ids = array();
  foreach ($result as $row) {
    $ids[] = $row->cust_id;
  }
  return $ids;
}
