<?php

/**
 * @file
 * Invoice email processing.
 *
 * Generates an invoice email in plain text or HTML format,
 * and allows the user to review the email and send it to the customer.
 */

/**
 * Page Callback: A form to generate an email to send to the customer.
 *
 * @param string $inv
 *   The invoice ID for the current invoice, that will be sent in the email.
 * @param string $mail_fmt
 *   Indicates which format to use for the email: "text" or "html".
 *
 * @return array
 *   A form array
 *
 * @see freebil_menu()
 * @see freebil_sendmail_form()
 *
 * @ingroup forms
 */
function freebil_sendmail_generate($inv, $mail_fmt) {
  $inv_data = freebil_get_inv_data($inv);
  $cust_data = freebil_get_cust_data($inv_data['cust_id']);
  return drupal_get_form('freebil_sendmail_form', $inv_data, $cust_data, $mail_fmt);
}

/**
 * Form constructor for the form to generate an email to send to the customer.
 *
 * @param array $inv_data
 *   An associative array containing:
 *   - All data for the current invoice with column names as keys and the
 *     column values as values.
 * @param array $cust_data
 *   An associative array containing:
 *   - All data for the current customer with column names as keys and the
 *     column values as values.
 * @param string $mail_fmt
 *   Indicates which format to use for the email: "text" or "html".
 *
 * @return array
 *   A form array
 *
 * @see freebil_sendmail_form_submit()
 *
 * @ingroup forms
 */
function freebil_sendmail_form($form, &$form_state, array $inv_data, array $cust_data, $mail_fmt) {
  $crumbs = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('freebil'), 'admin/freebil'),
    l(t('Customers'), 'admin/freebil/customers'),
    l($inv_data['cust_name'], 'admin/freebil/customers/' . $inv_data['cust_id'] . '/view'),
    l($inv_data['inv_name'], 'admin/freebil/invoices/' . $inv_data['inv_id'] . '/view'),
    l($inv_data['inv_name'] . ' Gen', 'admin/freebil/invoices/' . $inv_data['inv_id'] . '/generate'),
  );
  drupal_set_breadcrumb($crumbs);

  return freebil_build_sendmail_form($inv_data, $cust_data, $mail_fmt);

}

/**
 * Submit handler for freebil_sendmail_form().
 *
 * Sends invoice email to the recipient(s)
 */
function freebil_sendmail_form_submit($form, &$form_state) {
  $inv_data = freebil_get_inv_data($form_state['values']['inv_id']);
  $cust_data = freebil_get_cust_data($form_state['values']['cust_id']);

  $to = '';
  $inv_recipient_tbl = freebil_recipients_for_invoice_links($inv_data['inv_id'], FALSE);
  foreach ($inv_recipient_tbl['rows'] as $row) {
    $to .= $row['email_addr'] . ', ';
  }
  if (empty($to)) {
    $result_msg = t('No recipients found! Email was not sent. Please add email recipients to this invoice then try again.');
  }
  else {
    $module = 'freebil';
    $key = 'sendmail';
    $from = freebil_check_text(variable_get('freebil_from_email', 'site_mail'));
    $language = language_default();
    if ($form_state['values']['mail_format'] == 'html') {
      $body = freebil_inv_email_html_format($inv_data, $cust_data);
    }
    else {
      $body = freebil_inv_email_plaintext_format($inv_data, $cust_data, "\r\n");
    }
    // The body must be an array within an array to work correctly.
    $params = array(
      'body' => array(
        'body2' => $body,
      ),
      'headers' => array(
        'Content-Type' => 'text/html; charset=UTF-8;',
        'From' => $from,
        'Sender' => $from,
        'Return-Path' => $from,
        'Bcc' => $from,
      ),
    );
    $send = TRUE;
    $result = drupal_mail($module, $key, $to, $language, $params, $from, $send);

    if ($result['result'] == TRUE) {
      $result_msg = 'Email was sent.';
      // Update the invoice with sent status and date.
      freebil_update_inv_sent($inv_data['inv_id']);
    }
    else {
      $result_msg = t('There was a problem sending your message and it was not sent.');
    }
  }

  $form_state['redirect'] = 'admin/freebil/invoices/' . $inv_data['inv_id'] . '/generate';
  drupal_set_message($result_msg);
}

/**
 * Implements hook_mail().
 */
function freebil_mail($key, &$message, $params) {
  $co_name = freebil_check_text(variable_get('freebil_company_name'));
  $message['headers'] = $params['headers'];
  $message['subject'] = t('Invoice from @co_name', array('@co_name' => $co_name));
  $message['body'] = $params['body'];
}

/**
 * Builds a form to generate an email to send to the customer.
 *
 * @param array $inv_data
 *   An associative array containing:
 *   - All data for an existing invoice with column names as keys and the
 *     column values as values.
 * @param array $cust_data
 *   An associative array containing:
 *   - All data for the current customer with column names as keys and the
 *     column values as values.
 * @param string $mail_fmt
 *   Indicates which format to use for the email: "text" or "html".
 *
 * @return array
 *   A form array
 */
function freebil_build_sendmail_form(array $inv_data, array $cust_data, $mail_fmt) {
  $form['cust_id'] = array(
    '#type' => 'hidden',
    '#value' => $cust_data['cust_id'],
  );
  $form['inv_id'] = array(
    '#type' => 'hidden',
    '#value' => $inv_data['inv_id'],
  );
  $form['mail_format'] = array(
    '#type' => 'hidden',
    '#value' => $mail_fmt,
  );

  $form['email_text_start'] = array(
    '#markup' => '<h4>' . t('Email Body Starts:') . '</h4>',
    '#prefix' => '<div class="freebil-sample-email-separator">',
    '#suffix' => '</div>',
  );

  if ($mail_fmt == 'text') {
    $markup = freebil_inv_email_plaintext_format($inv_data, $cust_data, '<br>');
  }
  else {
    $markup = freebil_inv_email_html_format($inv_data, $cust_data, FALSE);
  }

  $form['email_text'] = array(
    '#markup' => '<p>' . $markup . '</p>',
    '#prefix' => '<div id="freebil-sample-email">',
    '#suffix' => '</div>',
  );

  $form['email_text_end'] = array(
    '#markup' => '<h4>' . t('Email Body Ends.') . '</h4>',
    '#prefix' => '<div class="freebil-sample-email-separator">',
    '#suffix' => '</div>',
  );

  $inv_recipient_tbl = freebil_recipients_for_invoice_links($inv_data['inv_id'], FALSE);

  if (!empty($inv_recipient_tbl['rows'])) {
    $markup = theme('table', array('header' => $inv_recipient_tbl['header'], 'rows' => $inv_recipient_tbl['rows']));
  }
  else {
    $markup = '<p><strong>' . t('No invoice email recipients chosen!') . '</strong> ' . t('Go back to the <a href="@actions">Invoice Actions page</a> to select email recipients.',
      array('@actions' => url('admin/freebil/invoices/' . $inv_data['inv_id'] . '/view'))) . '</p>';
  }
  $form['email_recipients_tbl'] = array(
    '#markup' => $markup,
    '#prefix' => '<div id="freebil-sendmail-recipients"><p>' . t('The email will be sent to the following recipients:') . '</p>',
    '#suffix' => '</div>',
  );
  $form['email_from_addr'] = array(
    '#markup' => '<div id="freebil-sendmail-from"><p>' . t('The email will use the following "from" address:') . ' <strong>' .
    check_plain(variable_get('freebil_from_email')) . '</strong><br>' .
    t('If the "from" email address is not correct, please go to <a href="@config">Freebil configuration</a> and change it.',
    array('@config' => url('admin/config/system/freebil'))) . '</p></div>',
  );

  $fmt_csv = ($mail_fmt == "html" ? 'CSV' : 'text');

  $form['email_download'] = array(
    '#markup' => l(t('Download this email as a @fmt_csv file', array('@fmt_csv' => $fmt_csv)), 'admin/freebil/invoices/' . $inv_data['inv_id'] . '/' . $mail_fmt . '/sendmail/download'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send Email'),
  );
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/freebil/invoices/' . $inv_data['inv_id'] . '/generate'),
  );

  return $form;
}

/**
 * Generates the email body in Plain Text format.
 *
 * @param array $inv_data
 *   An associative array containing:
 *   - All data for the current invoice with column names as keys and
 *     the column values as values.
 * @param array $cust_data
 *   An associative array containing:
 *   - All data for the current customer with column names as keys and
 *     the column values as values.
 * @param string $new_line
 *   If set to '<br>' it returns HTML markup to simulate the plain text
 *   email body for display to the user. If set to "\r\n" it returns
 *   the plain text email body for sending to the customer.
 *
 * @return string
 *   The email body text.
 */
function freebil_inv_email_plaintext_format(array $inv_data, array $cust_data, $new_line = '<br>') {
  $output = '';

  // Gather all the data for this invoice.
  $variables = freebil_get_all_invgen_data($inv_data);

  $my_co_name = freebil_check_text(variable_get('freebil_company_name'));
  $cust_name = $inv_data['cust_name'];

  // Store the configured company address lines in a table.
  $co_addr_tbl = freebil_get_mycompany_addr_lines();

  // Store the customer's address lines in a table.
  $cust_addr_tbl = freebil_get_customer_addr_lines($cust_data);

  // Header Section, My company name and address.
  $output .= $my_co_name . $new_line;
  foreach ($co_addr_tbl as $addr_line) {
    if (!empty($addr_line)) {
      $output .= $addr_line . $new_line;
    }
  }
  $output .= $new_line;

  // Header Section, Invoice number, date, due date.
  $output .= t('Invoice #@inv_id', array('@inv_id' => $inv_data['inv_id'])) . $new_line;
  $output .= t('Invoice Date: @today_date', array('@today_date' => $variables['pay_due_date']['today_date'])) . $new_line;
  $output .= t('Invoice Currency: @currency_code', array('@currency_code' => $inv_data['currency_code'])) . $new_line;
  $output .= t('Total Due: @balance_due', array('@balance_due' => $variables['inv_balance_tbl']['rows'][0]['balance_due'])) . $new_line;
  $output .= t('Due Date: @due_date', array('@due_date' => $variables['pay_due_date']['due_date'])) . $new_line;
  $output .= $new_line;

  // Header Section, Customer name and address.
  $output .= t('INVOICE TO') . $new_line;
  $output .= $cust_name . $new_line;
  foreach ($cust_addr_tbl as $addr_line) {
    if (!empty($addr_line)) {
      $output .= $addr_line . $new_line;
    }
  }
  $output .= $new_line;

  // Invoice Items Section.
  if (!empty($variables['item_hourly_tbl']['rows'])) {
    $output .= t('HOURLY CHARGES') . $new_line;
    foreach ($variables['item_hourly_tbl']['rows'] as $row) {
      $output .= $row['description'] . ' ... ' . $row['billed_amt'] . $new_line;
    }
    $output .= $new_line;
  }
  if (!empty($variables['item_fixed_tbl']['rows'])) {
    $output .= t('FIXED RATE CHARGES') . $new_line;
    foreach ($variables['item_fixed_tbl']['rows'] as $row) {
      $output .= $row['description'] . ' ... ' . $row['billed_amt'] . $new_line;
    }
    if (empty($variables['item_fixed_qty_tbl']['rows'])) {
      $output .= $new_line;
    }
  }
  if (!empty($variables['item_fixed_qty_tbl']['rows'])) {
    foreach ($variables['item_fixed_qty_tbl']['rows'] as $row) {
      $output .= $row['description'] . ' ... ' . $row['billed_amt'] . $new_line;
    }
    $output .= $new_line;
  }
  if (!empty($variables['item_product_tbl']['rows'])) {
    $output .= t('PRODUCT CHARGES') . $new_line;
    foreach ($variables['item_product_tbl']['rows'] as $row) {
      $output .= $row['description'] . ' ... ' . $row['billed_amt'] . $new_line;
    }
    $output .= $new_line;
  }
  if (!empty($variables['item_expense_tbl']['rows'])) {
    $output .= t('EXPENSES') . $new_line;
    foreach ($variables['item_expense_tbl']['rows'] as $row) {
      $output .= $row['description'] . ' ... ' . $row['billed_amt'] . $new_line;
    }
    if (empty($variables['item_expense_qty_tbl']['rows'])) {
      $output .= $new_line;
    }
  }
  if (!empty($variables['item_expense_qty_tbl']['rows'])) {
    foreach ($variables['item_expense_qty_tbl']['rows'] as $row) {
      $output .= $row['description'] . ' ... ' . $row['billed_amt'] . $new_line;
    }
    $output .= $new_line;
  }
  if (!empty($variables['item_adjust_tbl']['rows'])) {
    $output .= t('ADJUSTMENTS') . $new_line;
    foreach ($variables['item_adjust_tbl']['rows'] as $row) {
      $output .= $row['description'] . ' ... ' . $row['billed_amt'] . $new_line;
    }
    if (empty($variables['item_adjust_qty_tbl']['rows'])) {
      $output .= $new_line;
    }
  }
  if (!empty($variables['item_adjust_qty_tbl']['rows'])) {
    foreach ($variables['item_adjust_qty_tbl']['rows'] as $row) {
      $output .= $row['description'] . ' ... ' . $row['billed_amt'] . $new_line;
    }
    $output .= $new_line;
  }
  if (!empty($variables['item_tax_tbl']['rows'])) {
    $output .= t('TAXES') . $new_line;
    foreach ($variables['item_tax_tbl']['rows'] as $row) {
      $output .= $row['description'] . ' ... ' . $row['billed_amt'] . $new_line;
    }
    if (empty($variables['item_tax_qty_tbl']['rows'])) {
      $output .= $new_line;
    }
  }
  if (!empty($variables['item_tax_qty_tbl']['rows'])) {
    foreach ($variables['item_tax_qty_tbl']['rows'] as $row) {
      $output .= $row['description'] . ' ... ' . $row['billed_amt'] . $new_line;
    }
    $output .= $new_line;
  }

  // Totals Section.
  $output .= t('INVOICE TOTALS') . $new_line;
  foreach ($variables['inv_totals_tbl']['rows'][0] as $key => $value) {
    $output .= $variables['inv_totals_tbl']['header'][$key] . ' ... ' . $value . $new_line;
  }
  $output .= $new_line;

  // Payments Section.
  if (!empty($variables['inv_payments_tbl']['rows'])) {
    $output .= t('PAYMENTS RECEIVED') . $new_line;
    $payed_on = t('Paid on');
    $thank_you = t('Thank You!');
    foreach ($variables['inv_payments_tbl']['rows'] as $row) {
      $output .= $row['payment_amt'] . ' ' . $row['currency_code'] . ', ' . $payed_on . ' ' . $row['payment_date'] . ', ' . $thank_you . $new_line;
    }
    $output .= $new_line;
  }

  // Balance Section.
  $output .= t('INVOICE BALANCE') . $new_line;
  foreach ($variables['inv_balance_tbl']['rows'][0] as $key => $value) {
    $output .= $variables['inv_balance_tbl']['header'][$key] . ' ... ' . $value . $new_line;
  }
  $output .= $new_line;

  // Payment options and terms of payment.
  $output .= t('PAYMENT OPTIONS') . $new_line;
  $output .= freebil_check_text(variable_get('freebil_payment_instructions')) . $new_line;
  $output .= $new_line;

  $output .= t('TERMS OF PAYMENT') . $new_line;
  $output .= freebil_check_text(variable_get('freebil_payment_terms')) . $new_line;
  $output .= $new_line;

  return $output;
}

/**
 * Generates the email body in HTML format.
 *
 * Uses HTML tables for structuring the content to be more compatible with
 * the limited HTML rendering capabilities of many email clients.
 *
 * @param array $inv_data
 *   An associative array containing:
 *   - All data for the current invoice with column names as keys and
 *     the column values as values.
 * @param array $cust_data
 *   An associative array containing:
 *   - All data for the current customer with column names as keys and
 *     the column values as values.
 * @param bool $for_sending
 *   TRUE if the email body should be formatted for sending.
 *   FALSE if the email body should be formatted for display to the user.
 *
 * @return string
 *   Markup for an HTML email for this invoice.
 */
function freebil_inv_email_html_format(array $inv_data, array $cust_data, $for_sending = TRUE) {
  $output = '';

  // Gather all the data for this invoice.
  $variables = freebil_get_all_invgen_data($inv_data);

  $my_co_name = freebil_check_text(variable_get('freebil_company_name'));
  $cust_name = $inv_data['cust_name'];

  // Store the configured company address lines in a table.
  $co_addr_tbl = freebil_get_mycompany_addr_lines();

  // Store the customer's address lines in a table.
  $cust_addr_tbl = freebil_get_customer_addr_lines($cust_data);

  // HTML headers.
  if ($for_sending) {
    $output .= '<html><head><title>' . t('Invoice from @co_name', array('@co_name' => $my_co_name)) . '</title></head><body>';
  }

  // Reusable tag/CSS combinations.
  $td_style1 = '<td style="padding: 0 5px; border: none;">';
  $td_outer = '<td style="padding: 0 0; border: none;">';
  $tr_style1 = '<tr style="border: none;">';
  $table_style1 = '<table style="width: 100%; border: none; border-collapse: collapse; font-size: 1em; color: #000; margin-bottom: 1em;">';

  // Outer table to wrap two header sections.
  $output .= $table_style1 . $tr_style1 . $td_outer;
  // Header Section, Invoice number, date, due date.
  $output .= $table_style1;
  $output .= $tr_style1 . $td_style1 . '<strong>' . t('Invoice') . '</strong> #' . $inv_data['inv_id'] . '</td></tr>';
  $output .= $tr_style1 . $td_style1 . '<strong>' . t('Invoice Date') . '</strong>: ' . $variables['pay_due_date']['today_date'] . '</td></tr>';
  $output .= $tr_style1 . $td_style1 . '<strong>' . t('Invoice Currency') . '</strong>: ' . $inv_data['currency_code'] . '</td></tr>';
  $output .= $tr_style1 . $td_style1 . '<strong>' . t('Total Due') . '</strong>: ' . $variables['inv_balance_tbl']['rows'][0]['balance_due'] . '</td></tr>';
  $output .= $tr_style1 . $td_style1 . '<strong>' . t('Due Date') . '</strong>: ' . $variables['pay_due_date']['due_date'] . '</td></tr>';
  $output .= '</table>';
  $output .= '</td>';

  $output .= $td_outer;
  // Header Section, My company name and address.
  $output .= $table_style1 . $tr_style1 . $td_style1 . '<strong>' . $my_co_name . '</strong></td></tr>';
  foreach ($co_addr_tbl as $addr_line) {
    if (!empty($addr_line)) {
      $output .= $tr_style1 . $td_style1 . $addr_line . '</td></tr>';
    }
  }
  $output .= '</table>';
  $output .= '</td></tr></table>';

  // Header Section, Customer name and address.
  $output .= $table_style1;
  $output .= $tr_style1 . $td_style1 . t('Invoice To') . '</td></tr>';
  $output .= $tr_style1 . $td_style1 . '<strong>' . $cust_name . '</strong></td></tr>';
  foreach ($cust_addr_tbl as $addr_line) {
    if (!empty($addr_line)) {
      $output .= $tr_style1 . $td_style1 . $addr_line . '</td></tr>';
    }
  }
  $output .= '</table>';

  // Invoice Items Section.
  $output .= $table_style1;
  $output .= $tr_style1 . $td_style1 . '<strong>' . t('ITEMIZED CHARGES') . '</strong></td></tr>';
  $output .= '</table>';

  if (!empty($variables['item_hourly_tbl']['rows'])) {
    $output .= freebil_build_email_table(t('Hourly Items'), array('header' => $variables['item_hourly_tbl']['header'], 'rows' => $variables['item_hourly_tbl']['rows']), TRUE);
  }
  if (!empty($variables['item_fixed_tbl']['rows'])) {
    $output .= freebil_build_email_table(t('Fixed Rate Items'), array('header' => $variables['item_fixed_tbl']['header'], 'rows' => $variables['item_fixed_tbl']['rows']), TRUE);
  }
  if (!empty($variables['item_fixed_qty_tbl']['rows'])) {
    $output .= freebil_build_email_table(t('Fixed Rate Items'), array('header' => $variables['item_fixed_qty_tbl']['header'], 'rows' => $variables['item_fixed_qty_tbl']['rows']), TRUE);
  }
  if (!empty($variables['item_product_tbl']['rows'])) {
    $output .= freebil_build_email_table(t('Products'), array('header' => $variables['item_product_tbl']['header'], 'rows' => $variables['item_product_tbl']['rows']), TRUE);
  }
  if (!empty($variables['item_expense_tbl']['rows'])) {
    $output .= freebil_build_email_table(t('Expenses'), array('header' => $variables['item_expense_tbl']['header'], 'rows' => $variables['item_expense_tbl']['rows']), TRUE);
  }
  if (!empty($variables['item_expense_qty_tbl']['rows'])) {
    $output .= freebil_build_email_table(t('Expenses'), array('header' => $variables['item_expense_qty_tbl']['header'], 'rows' => $variables['item_expense_qty_tbl']['rows']), TRUE);
  }
  if (!empty($variables['item_adjust_tbl']['rows'])) {
    $output .= freebil_build_email_table(t('Adjustments'), array('header' => $variables['item_adjust_tbl']['header'], 'rows' => $variables['item_adjust_tbl']['rows']), TRUE);
  }
  if (!empty($variables['item_adjust_qty_tbl']['rows'])) {
    $output .= freebil_build_email_table(t('Adjustments'), array('header' => $variables['item_adjust_qty_tbl']['header'], 'rows' => $variables['item_adjust_qty_tbl']['rows']), TRUE);
  }
  if (!empty($variables['item_tax_tbl']['rows'])) {
    $output .= freebil_build_email_table(t('Taxes'), array('header' => $variables['item_tax_tbl']['header'], 'rows' => $variables['item_tax_tbl']['rows']), TRUE);
  }
  if (!empty($variables['item_tax_qty_tbl']['rows'])) {
    $output .= freebil_build_email_table(t('Taxes'), array('header' => $variables['item_tax_qty_tbl']['header'], 'rows' => $variables['item_tax_qty_tbl']['rows']), TRUE);
  }

  // Totals Section.
  $output .= freebil_build_email_table(t('INVOICE TOTALS'), array('header' => $variables['inv_totals_tbl']['header'], 'rows' => $variables['inv_totals_tbl']['rows']));

  // Payments Section.
  if (!empty($variables['inv_payments_tbl']['rows'])) {
    $output .= freebil_build_email_table(t('PAYMENTS RECEIVED - Thank You!'), array('header' => $variables['inv_payments_tbl']['header'], 'rows' => $variables['inv_payments_tbl']['rows']));
  }

  // Balance Section.
  $output .= freebil_build_email_table(t('INVOICE BALANCE'), array('header' => $variables['inv_balance_tbl']['header'], 'rows' => $variables['inv_balance_tbl']['rows']));

  // Payment options and terms of payment.
  $output .= $table_style1;
  $output .= $tr_style1 . $td_style1 . '<strong>' . t('PAYMENT OPTIONS') . '</strong></td></tr>';
  $output .= $tr_style1 . $td_style1 . freebil_check_text(variable_get('freebil_payment_instructions')) . '</td></tr>';
  $output .= '</table>';

  $output .= $table_style1;
  $output .= $tr_style1 . $td_style1 . '<strong>' . t('TERMS OF PAYMENT') . '</strong></td></tr>';
  $output .= $tr_style1 . $td_style1 . freebil_check_text(variable_get('freebil_payment_terms')) . '</td></tr>';
  $output .= '</table>';

  // HTML headers, closing tags.
  if ($for_sending) {
    $output .= '</body></html>';
  }

  return $output;
}

/**
 * Generates markup for an HTML table based on input parameters.
 *
 * For use in building an HTML email body.
 *
 * @param string $title
 *   The title for this table.
 * @param array $tbl_array
 *   An associative array containing:
 *   - An associative array containing:
 *     One row of column headings.
 *   - An associative array containing:
 *     All data rows for this table.
 * @param bool $replace_desc
 *   FALSE (default) if the input title should appear above the table.
 *   TRUE if the input title should appear with the table column headings,
 *   replacing the column heading named "description".
 *
 * @return string
 *   Markup for an HTML table.
 */
function freebil_build_email_table($title, array $tbl_array, $replace_desc = FALSE) {
  // Reusable tag/CSS combinations.
  $td_style1 = '<td style="padding: 0 5px; border: none;">';
  $td_style2 = '<td style="padding: 5px 8px; border: 1px solid #bebfb9;">';
  $tr_style1 = '<tr style="border: none;">';
  $tr_style_hd = '<tr style="border: none; background: #e1e2dc;">';
  $table_style1 = '<table style="width: 100%; border: none; border-collapse: collapse; font-size: 1em; color: #000; margin-bottom: 1em;">';
  $table_style2 = '<table style="width: 100%; border: 1px solid #bebfb9; border-collapse: collapse; font-size: 1em; color: #000; margin-bottom: 1.5em;">';

  $output = '';

  if (!$replace_desc) {
    // The input title will appear above the table.
    $output .= $table_style1;
    $output .= $tr_style1 . $td_style1 . '<strong>' . $title . '</strong></td></tr>';
    $output .= '</table>';
  }
  $output .= $table_style2;
  $output .= $tr_style_hd;
  foreach ($tbl_array['header'] as $key => $heading) {
    if ($replace_desc &&  ($key == 'description')) {
      // The input title replaces the word "Description" in the header row.
      $output .= $td_style2 . $title . '</td>';
    }
    else {
      $output .= $td_style2 . $heading . '</td>';
    }
  }
  $output .= '</tr>';
  foreach ($tbl_array['rows'] as $row) {
    $output .= $tr_style1;
    foreach ($row as $col) {
      $output .= $td_style2 . $col . '</td>';
    }
    $output .= '</tr>';
  }
  $output .= '</table>';
  return $output;
}

/**
 * Gets all data for an invoice and formats it for rendering.
 *
 * @param array $inv_data
 *   An associative array containing:
 *   - All data for the current invoice with column names as keys and
 *     the column values as values.
 *
 * @return array
 *   An associative array containing:
 *   - multiple arrays and variables containing all data for the completed
 *     invoice, ready to be rendered for display to the user or sending in
 *     an email to the customer.
 */
function freebil_get_all_invgen_data(array $inv_data) {
  $inv_id = $inv_data['inv_id'];
  $inv_hdr_tbl = freebil_invoice_view_header_data($inv_data);
  $invgen_item_hourly_tbl = freebil_invgen_item_hourly_data($inv_id);
  // Calling freebil_invoice_item_other_data() with TRUE makes it return items
  // with quantity and unit cost populated. FALSE excludes these items.
  $invgen_item_fixedrate_tbl = freebil_invgen_item_other_data($inv_id, 'Fixed Rate', FALSE);
  $invgen_item_fixedrate_qty_tbl = freebil_invgen_item_other_data($inv_id, 'Fixed Rate', TRUE);
  $invgen_item_product_tbl = freebil_invgen_item_product_data($inv_id);
  $invgen_item_expense_tbl = freebil_invgen_item_other_data($inv_id, 'Expense', FALSE);
  $invgen_item_expense_qty_tbl = freebil_invgen_item_other_data($inv_id, 'Expense', TRUE);
  $invgen_item_adjust_tbl = freebil_invgen_item_other_data($inv_id, 'Adjustment', FALSE);
  $invgen_item_adjust_qty_tbl = freebil_invgen_item_other_data($inv_id, 'Adjustment', TRUE);
  $invgen_item_tax_tbl = freebil_invgen_item_other_data($inv_id, 'Tax', FALSE);
  $invgen_item_tax_qty_tbl = freebil_invgen_item_other_data($inv_id, 'Tax', TRUE);
  $inv_totals_tbl = freebil_invoice_view_totals_data($inv_data);
  $invgen_payments_tbl = freebil_invgen_payments_list_data($inv_id);
  $inv_balance_tbl = freebil_invoice_view_balance_data($inv_data);
  $pay_due_date = freebil_calc_payment_due_date();
  $inv_recipient_tbl = freebil_recipients_for_invoice_links($inv_id, FALSE);

  return array(
    'pay_due_date' => $pay_due_date,
    'inv_header_tbl' => $inv_hdr_tbl,
    'item_hourly_tbl' => $invgen_item_hourly_tbl,
    'item_fixed_tbl' => $invgen_item_fixedrate_tbl,
    'item_fixed_qty_tbl' => $invgen_item_fixedrate_qty_tbl,
    'item_product_tbl' => $invgen_item_product_tbl,
    'item_expense_tbl' => $invgen_item_expense_tbl,
    'item_expense_qty_tbl' => $invgen_item_expense_qty_tbl,
    'item_adjust_tbl' => $invgen_item_adjust_tbl,
    'item_adjust_qty_tbl' => $invgen_item_adjust_qty_tbl,
    'item_tax_tbl' => $invgen_item_tax_tbl,
    'item_tax_qty_tbl' => $invgen_item_tax_qty_tbl,
    'inv_totals_tbl' => $inv_totals_tbl,
    'inv_payments_tbl' => $invgen_payments_tbl,
    'inv_balance_tbl' => $inv_balance_tbl,
    'inv_recipients_tbl' => $inv_recipient_tbl,
  );
}

/**
 * Menu Callback: Downloads email contents in text format.
 *
 * @param string $inv
 *   The invoice ID for this invoice email.
 * @param string $mail_fmt
 *   Indicates the format of the email: "text" or "html".
 */
function freebil_sendmail_download($inv, $mail_fmt) {
  $inv_data = freebil_get_inv_data($inv);
  $cust_data = freebil_get_cust_data($inv_data['cust_id']);
  if ($mail_fmt == "text") {
    $email_text = freebil_inv_email_plaintext_format($inv_data, $cust_data, "\r\n");
  }
  elseif ($mail_fmt == "html") {
    $email_text = freebil_inv_email_csv_format($inv_data, $cust_data);
  }
  $extension = ($mail_fmt == "html" ? 'csv' : 'txt');
  $filename = 'invoice_' . $inv . '.' . $extension;
  header("Content-type: text/plain");
  header("Content-Disposition: attachment; filename=" . $filename);
  print $email_text;
  // Any output after this point will be added to the downloaded file!
  // The exit function acts like a redirect back to the prior page.
  drupal_exit();
}

/**
 * Generates a CSV version of the HTML email.
 *
 * @param array $inv_data
 *   An associative array containing:
 *   - All data for the current invoice with column names as keys and
 *     the column values as values.
 * @param array $cust_data
 *   An associative array containing:
 *   - All data for the current customer with column names as keys and
 *     the column values as values.
 *
 * @return string
 *   The email body in CSV format.
 */
function freebil_inv_email_csv_format(array $inv_data, array $cust_data) {
  $new_line = "\r\n";
  $output = '';

  // Gather all the data for this invoice.
  $variables = freebil_get_all_invgen_data($inv_data);

  $my_co_name = freebil_csv_quote(freebil_check_text(variable_get('freebil_company_name')));
  $cust_name = freebil_csv_quote($inv_data['cust_name']);

  // Store the configured company address lines in a table.
  $co_addr_tbl = freebil_get_mycompany_addr_lines();
  $co_addr_tbl = freebil_quote_lines($co_addr_tbl);

  // Store the customer's address lines in a table.
  $cust_addr_tbl = freebil_get_customer_addr_lines($cust_data);
  $cust_addr_tbl = freebil_quote_lines($cust_addr_tbl);

  // Header Section, My company name and address.
  $output .= $my_co_name . $new_line;
  foreach ($co_addr_tbl as $addr_line) {
    if (!empty($addr_line)) {
      $output .= $addr_line . $new_line;
    }
  }
  $output .= $new_line;

  // Header Section, Invoice number, date, due date.
  $output .= t('Invoice #@inv_id', array('@inv_id' => $inv_data['inv_id'])) . $new_line;
  $output .= t('Invoice Date: @today_date', array('@today_date' => $variables['pay_due_date']['today_date'])) . $new_line;
  $output .= t('Invoice Currency: @currency_code', array('@currency_code' => $inv_data['currency_code'])) . $new_line;
  $output .= t('Total Due: @balance_due', array('@balance_due' => $variables['inv_balance_tbl']['rows'][0]['balance_due'])) . $new_line;
  $output .= t('Due Date: @due_date', array('@due_date' => $variables['pay_due_date']['due_date'])) . $new_line;
  $output .= $new_line;

  // Header Section, Customer name and address.
  $output .= t('INVOICE TO') . $new_line;
  $output .= $cust_name . $new_line;
  foreach ($cust_addr_tbl as $addr_line) {
    if (!empty($addr_line)) {
      $output .= $addr_line . $new_line;
    }
  }
  $output .= $new_line;

  // Itemized charges section.
  $output .= t('ITEMIZED CHARGES') . $new_line;

  if (!empty($variables['item_hourly_tbl']['rows'])) {
    $output .= freebil_table_to_csv(t('Hourly Items'), $new_line, array('header' => $variables['item_hourly_tbl']['header'], 'rows' => $variables['item_hourly_tbl']['rows']), TRUE);
  }
  if (!empty($variables['item_fixed_tbl']['rows'])) {
    $output .= freebil_table_to_csv(t('Fixed Rate Items'), $new_line, array('header' => $variables['item_fixed_tbl']['header'], 'rows' => $variables['item_fixed_tbl']['rows']), TRUE);
  }
  if (!empty($variables['item_fixed_qty_tbl']['rows'])) {
    $output .= freebil_table_to_csv(t('Fixed Rate Items'), $new_line, array('header' => $variables['item_fixed_qty_tbl']['header'], 'rows' => $variables['item_fixed_qty_tbl']['rows']), TRUE);
  }
  if (!empty($variables['item_product_tbl']['rows'])) {
    $output .= freebil_table_to_csv(t('Products'), $new_line, array('header' => $variables['item_product_tbl']['header'], 'rows' => $variables['item_product_tbl']['rows']), TRUE);
  }
  if (!empty($variables['item_expense_tbl']['rows'])) {
    $output .= freebil_table_to_csv(t('Expenses'), $new_line, array('header' => $variables['item_expense_tbl']['header'], 'rows' => $variables['item_expense_tbl']['rows']), TRUE);
  }
  if (!empty($variables['item_expense_qty_tbl']['rows'])) {
    $output .= freebil_table_to_csv(t('Expenses'), $new_line, array('header' => $variables['item_expense_qty_tbl']['header'], 'rows' => $variables['item_expense_qty_tbl']['rows']), TRUE);
  }
  if (!empty($variables['item_adjust_tbl']['rows'])) {
    $output .= freebil_table_to_csv(t('Adjustments'), $new_line, array('header' => $variables['item_adjust_tbl']['header'], 'rows' => $variables['item_adjust_tbl']['rows']), TRUE);
  }
  if (!empty($variables['item_adjust_qty_tbl']['rows'])) {
    $output .= freebil_table_to_csv(t('Adjustments'), $new_line, array('header' => $variables['item_adjust_qty_tbl']['header'], 'rows' => $variables['item_adjust_qty_tbl']['rows']), TRUE);
  }
  if (!empty($variables['item_tax_tbl']['rows'])) {
    $output .= freebil_table_to_csv(t('Taxes'), $new_line, array('header' => $variables['item_tax_tbl']['header'], 'rows' => $variables['item_tax_tbl']['rows']), TRUE);
  }
  if (!empty($variables['item_tax_qty_tbl']['rows'])) {
    $output .= freebil_table_to_csv(t('Taxes'), $new_line, array('header' => $variables['item_tax_qty_tbl']['header'], 'rows' => $variables['item_tax_qty_tbl']['rows']), TRUE);
  }

  // Totals Section.
  $output .= freebil_table_to_csv(t('INVOICE TOTALS'), $new_line, array('header' => $variables['inv_totals_tbl']['header'], 'rows' => $variables['inv_totals_tbl']['rows']));

  // Payments Section.
  if (!empty($variables['inv_payments_tbl']['rows'])) {
    $output .= freebil_table_to_csv(t('PAYMENTS RECEIVED - Thank You!'), $new_line, array('header' => $variables['inv_payments_tbl']['header'], 'rows' => $variables['inv_payments_tbl']['rows']));
  }

  // Balance Section.
  $output .= freebil_table_to_csv(t('INVOICE BALANCE'), $new_line, array('header' => $variables['inv_balance_tbl']['header'], 'rows' => $variables['inv_balance_tbl']['rows']));

  // Payment options and terms of payment.
  $output .= t('PAYMENT OPTIONS') . $new_line;
  $pay_options = freebil_csv_quote(freebil_check_text(variable_get('freebil_payment_instructions')));
  $output .= $pay_options . $new_line . $new_line;

  $output .= t('TERMS OF PAYMENT') . $new_line;
  $pay_terms = freebil_csv_quote(freebil_check_text(variable_get('freebil_payment_terms')));
  $output .= $pay_terms . $new_line;

  return $output;
}

/**
 * Converts a table-formatted array to csv lines.
 *
 * @param string $title
 *   The title of the table.
 * @param string $new_line
 *   The new line character(s) to use at the end of each line.
 * @param array $tbl_array
 *   An associative array containing:
 *   - An array for the table header.
 *   - An array for the table rows.
 * @param bool $replace_desc
 *   FALSE (default) if the input title should appear above the table.
 *   TRUE if the input title should appear with the table column headings,
 *   replacing the column heading named "description".
 *
 * @return string
 *   The table data as multiple lines of comma separated values.
 */
function freebil_table_to_csv($title, $new_line, array $tbl_array, $replace_desc = FALSE) {
  $output = '';

  if (!$replace_desc && (!empty($title))) {
    $output .= $title . $new_line;
  }

  $len = count($tbl_array['header']);
  $count = 0;
  foreach ($tbl_array['header'] as $key => $heading) {
    $count++;
    if ($replace_desc && $key == 'description') {
      // The input title replaces the word "Description" in the header row.
      $output .= $title;
    }
    else {
      $output .= $heading;
    }
    if ($count < $len) {
      $output .= ', ';
    }
  }
  $output .= $new_line;

  foreach ($tbl_array['rows'] as $key2 => $row) {
    $len = count($tbl_array['rows'][$key2]);
    $count = 0;
    foreach ($row as $key2 => $col) {
      $count++;
      if ($key2 == 'description' || $key2 == 'notes') {
        $col = freebil_csv_quote($col);
      }
      $output .= $col;
      if ($count < $len) {
        $output .= ', ';
      }
    }
    $output .= $new_line;
  }

  $output .= $new_line;
  return $output;
}

/**
 * Puts double quotes around each element of an array of strings.
 *
 * @param array $in_array
 *   An array of strings.
 *
 * @return array
 *   The input array with quotes added.
 */
function freebil_quote_lines(array $in_array) {
  $out_array = array();
  foreach ($in_array as $in_string) {
    $out_string = freebil_csv_quote($in_string);
    $out_array[] = $out_string;
  }
  return $out_array;
}

/**
 * Puts double quotes around each element of a string.
 *
 * Also removes any double quote characters originally presesnt in the string.
 *
 * @param string $in_string
 *   A string.
 *
 * @return string
 *   The input string with internal quotes removed and enclosing quotes added.
 */
function freebil_csv_quote($in_string) {
  // Remove all double quote characters from the string.
  $in_string = str_replace('"', '', $in_string);
  // Enclose the string with double quotes.
  $out_string = '"' . $in_string . '"';
  return $out_string;
}
