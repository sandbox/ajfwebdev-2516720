
INTRODUCTION
------------

The Freebil module is a mini billing system intended to help freelancers,
consultants or small business owners manage information about their
customers, charges, invoices, and payments. Working as a freelancer,
I found it too time consuming and confusing to create invoices manually
and keep track of all the invoicing and payment data for multiple customers.
While there are many cloud-based services for billing and invoicing,
they generally represent a recurring cost to your business. So if
you are already paying for a Drupal site to be hosted securely, using
the Freebil module could save you money.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/ajfwebdev/2516720

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2516720

SECURITY
--------

It's important to keep your customer's information and your own financial
data secure. Although I've coded the module up to the Drupal security
standards that I'm aware of, I make no guarantee as to how secure this
module is. Use it at your own risk. It runs on a Drupal website, and
websites can be hacked. I recommend that users run freebil on a site
with SSL encryption activated. At least that way, all your data entered
and rendered (network traffic between your browser and the site) will
be encrypted in transit. It would also seem prudent to have your site
scanned for viruses and malware regularly.

REQUIREMENTS
------------
Freebil uses the following Drupal modules:
  * date (https://drupal.org/project/date)
    - For formatting dates.
  * date_popup (https://drupal.org/project/date_popup)
    - Used as a date picker in forms.
  * mimemail (https://drupal.org/project/mimemail)
    - To enable HTML email sending, for the invoice emails
      sent to customers.
  * mailsystem  (https://drupal.org/project/mailsystem)
    - Required by mimemail to enable HTML email sending.

INSTALLATION
------------

Until the project is approved, use Git Clone to retrieve it from the
Freebil sandbox URL:
http://git.drupal.org/sandbox/ajfwebdev/2516720.git
Put it in your contributed modules directory and then enable it as you
would any other Drupal module.

CONFIGURATION
-------------

  * Configure Freebil by going to the Freebil configuration page at:
    Administration >> Configuration >> System >> Freebil
    Here you will find a form to enter all the information you wish to
    include on most invoices you create. Your (or your company's) name,
    your address, email address, default currency, and other default
    values. Failure to complete this form will result in missing data
    on your generated invoices.  Having defaults set such as currency code
    and your default hourly rate, will also make it easier to create
    invoices and add invoice items. So it is best to complete the configuration
    before doing anything else with Freebil.
